/obj/item/weapon/gun/projectile/pistol/lugergun
	name = "l58 luger pistol"
	desc = "The TE Arms L58 Luger. A medium size, classical gun. Uses 9mm rounds."
	icon_state = "lugerclassic"
	item_state = "lugerclassic"
	w_class = ITEM_SIZE_NORMAL
	caliber = "9mm"
	silenced = 0
	fire_delay = 1.5
	origin_tech = list(TECH_COMBAT = 2, TECH_MATERIAL = 2, TECH_ILLEGAL = 2)
	load_method = MAGAZINE
	magazine_type = /obj/item/ammo_magazine/mc9mm
	allowed_magazines = /obj/item/ammo_magazine/mc9mm

/obj/item/weapon/gun/projectile/pistol/lugergun/attack_hand(mob/user as mob)
	if(user.get_inactive_hand() == src)
		if(silenced)
			if(user.l_hand != src && user.r_hand != src)
				..()
				return
			to_chat(user, "<span class='notice'>You unscrew [silenced] from [src].</span>")
			user.put_in_hands(silenced)
			silenced = initial(silenced)
			w_class = initial(w_class)
			update_icon()
			return
	..()

/obj/item/weapon/gun/projectile/pistol/lugergun/attackby(obj/item/I as obj, mob/user as mob)
	if(istype(I, /obj/item/weapon/silencer))
		if(user.l_hand != src && user.r_hand != src)	//if we're not in his hands
			to_chat(user, "<span class='notice'>You'll need [src] in your hands to do that.</span>")
			return
		user.drop_item()
		to_chat(user, "<span class='notice'>You screw [I] onto [src].</span>")
		silenced = I	//dodgy?
		w_class = ITEM_SIZE_NORMAL
		I.forceMove(src)		//put the silencer into the gun
		update_icon()
		return
	..()

/obj/item/weapon/gun/projectile/pistol/lugergun/update_icon()
	..()
	if(silenced)
		icon_state = "lugerclassics"
	else
		icon_state = "lugerclassic"
	if(!(ammo_magazine && ammo_magazine.stored_ammo.len))
		icon_state = "[icon_state]-e"

/obj/item/weapon/gun/projectile/automatic/wolfnewassault
	name = "StG 108 assault rifle"
	desc = "The Neoclassic StG 108 is an new remanufactured model of older assault rifle, made by the Teutonic's Empire Fabrics. Uses armor piercing 7.62mm rounds. Makes you feel like a Sturmtruppler when you hold it."
	icon_state = "assaultnew"
	item_state = "assaultnew"
	w_class = ITEM_SIZE_HUGE
	force = 15
	caliber = "a556"
	origin_tech = list(TECH_COMBAT = 8, TECH_MATERIAL = 3)
	ammo_type = /obj/item/ammo_casing/a556
	slot_flags = SLOT_BACK
	load_method = MAGAZINE
	magazine_type = /obj/item/ammo_magazine/c556
	allowed_magazines = /obj/item/ammo_magazine/c556
	one_hand_penalty = 3
	burst_delay = 2
	wielded_item_state = "z8carbine-wielded"
	//would have one_hand_penalty=4,5 but the added weight of a grenade launcher makes one-handing even harder
	firemodes = list(
		list(mode_name="semiauto",       burst=1,    fire_delay=0,    move_delay=null, use_launcher=null, one_hand_penalty=2, burst_accuracy=null, dispersion=null),
		list(mode_name="3-round bursts", burst=3,    fire_delay=null, move_delay=3,    use_launcher=null, one_hand_penalty=3, burst_accuracy=list(0,-1,-1), dispersion=list(0.0, 0.6, 1.0)),
		)

/obj/item/weapon/gun/projectile/automatic/wolfnewassault/update_icon()
	icon_state = (ammo_magazine)? "assaultnew" : "assaultnew-e"
	wielded_item_state = (ammo_magazine)? "arifle-wielded" : "arifle-wielded-e"
	..()

/obj/item/weapon/gun/projectile/automatic/wolfnewassaultgren
	name = "StG 108-GL assault rifle "
	desc = "The Neoclassic StG 108 with underslung greande launcher."
	icon_state = "assaultnewgren"
	item_state = "assaultnewgren"
	w_class = ITEM_SIZE_HUGE
	force = 15
	caliber = "a556"
	origin_tech = list(TECH_COMBAT = 8, TECH_MATERIAL = 3)
	ammo_type = /obj/item/ammo_casing/a556
	slot_flags = SLOT_BACK
	load_method = MAGAZINE
	magazine_type = /obj/item/ammo_magazine/c556
	allowed_magazines = /obj/item/ammo_magazine/c556
	one_hand_penalty = 3
	burst_delay = 2
	wielded_item_state = "z8carbine-wielded"
	//would have one_hand_penalty=4,5 but the added weight of a grenade launcher makes one-handing even harder
	firemodes = list(
		list(mode_name="semiauto",       burst=1,    fire_delay=0,    move_delay=null, use_launcher=null, one_hand_penalty=2, burst_accuracy=null, dispersion=null),
		list(mode_name="3-round bursts", burst=3,    fire_delay=null, move_delay=3,    use_launcher=null, one_hand_penalty=3, burst_accuracy=list(0,-1,-1), dispersion=list(0.0, 0.6, 1.0)),
		list(mode_name="fire grenades",  burst=null, fire_delay=null, move_delay=null, use_launcher=1,    one_hand_penalty=2, burst_accuracy=null, dispersion=null)
		)

	var/use_launcher = 0
	var/obj/item/weapon/gun/launcher/grenade/underslung/launcher

/obj/item/weapon/gun/projectile/automatic/wolfnewassaultgren/New()
	..()
	launcher = new(src)

/obj/item/weapon/gun/projectile/automatic/wolfnewassaultgren/attackby(obj/item/I, mob/user)
	if((istype(I, /obj/item/weapon/grenade)))
		launcher.load(I, user)
	else
		..()

/obj/item/weapon/gun/projectile/automatic/wolfnewassaultgren/attack_hand(mob/user)
	if(user.get_inactive_hand() == src && use_launcher)
		launcher.unload(user)
	else
		..()

/obj/item/weapon/gun/projectile/automatic/wolfnewassaultgren/Fire(atom/target, mob/living/user, params, pointblank=0, reflex=0)
	if(use_launcher)
		launcher.Fire(target, user, params, pointblank, reflex)
		if(!launcher.chambered)
			switch_firemodes() //switch back automatically
	else
		..()

/obj/item/weapon/gun/projectile/automatic/wolfnewassaultgren/update_icon()
	icon_state = (ammo_magazine)? "assaultnewgren" : "assaultnewgren-e"
	wielded_item_state = (ammo_magazine)? "arifle-wielded" : "arifle-wielded-empty"
	..()

/obj/item/weapon/gun/projectile/automatic/wolfnewassaultgren/examine(mob/user)
	. = ..()
	if(launcher.chambered)
		to_chat(user, "\The [launcher] has \a [launcher.chambered] loaded.")
	else
		to_chat(user, "\The [launcher] is empty.")

/obj/item/weapon/gun/projectile/pistol/neoluger
	name = "LS 781 neo-luger pistol"
	desc = "The TE Arms LS 781 Neo-Luger. A badass neoclassical gun, only for executives. Uses 9mm rounds."
	icon_state = "neoluger"
	item_state = "neoluger"
	w_class = ITEM_SIZE_NORMAL
	caliber = "9mm"
	silenced = 0
	fire_delay = 0.5
	origin_tech = list(TECH_COMBAT = 2, TECH_MATERIAL = 2, TECH_ILLEGAL = 2)
	load_method = MAGAZINE
	magazine_type = /obj/item/ammo_magazine/mc9mmt
	allowed_magazines = /obj/item/ammo_magazine/mc9mmt

/obj/item/weapon/gun/projectile/pistol/neoluger/attack_hand(mob/user as mob)
	if(user.get_inactive_hand() == src)
		if(silenced)
			if(user.l_hand != src && user.r_hand != src)
				..()
				return
			to_chat(user, "<span class='notice'>You unscrew [silenced] from [src].</span>")
			user.put_in_hands(silenced)
			silenced = initial(silenced)
			w_class = initial(w_class)
			update_icon()
			return
	..()

/obj/item/weapon/gun/projectile/pistol/neoluger/attackby(obj/item/I as obj, mob/user as mob)
	if(istype(I, /obj/item/weapon/silencer))
		if(user.l_hand != src && user.r_hand != src)	//if we're not in his hands
			to_chat(user, "<span class='notice'>You'll need [src] in your hands to do that.</span>")
			return
		user.drop_item()
		to_chat(user, "<span class='notice'>You screw [I] onto [src].</span>")
		silenced = I	//dodgy?
		w_class = ITEM_SIZE_NORMAL
		I.forceMove(src)		//put the silencer into the gun
		update_icon()
		return
	..()

/obj/item/weapon/gun/projectile/pistol/neoluger/update_icon()
	..()
	if(silenced)
		icon_state = "neolugers"
	else
		icon_state = "neoluger"
	if(!(ammo_magazine && ammo_magazine.stored_ammo.len))
		icon_state = "[icon_state]-e"

/obj/item/weapon/gun/energy/wolflaserrifle
	name = "Lasergewehr L-200"
	desc = "TE Arms Lasergewehr L-200, redesigned to kill with concentrated energy blasts.This model designed to mass production, so it's very cheap, but have some troubles with accuracy"
	icon_state = "laserrifle"
	item_state = "laserrifle"
	slot_flags = SLOT_BELT|SLOT_BACK
	w_class = ITEM_SIZE_LARGE
	force = 15
	one_hand_penalty = 4
	accuracy = 1
	origin_tech = list(TECH_COMBAT = 3, TECH_MAGNET = 2)
	matter = list(DEFAULT_WALL_MATERIAL = 2000)
	projectile_type = /obj/item/projectile/beam/midlaser
	wielded_item_state = "laser-wielded"

/obj/item/weapon/gun/projectile/automatic/wolfoldassault
	name = "StG-89 assault rifle"
	desc = "The rugged StG-89 is a durable automatic weapon of a make popular on the different borderworlds. The serial number has been scratched off. Uses 7.62mm rounds."
	icon_state = "assaultold"
	item_state = null
	w_class = ITEM_SIZE_HUGE
	force = 15
	caliber = "a762"
	origin_tech = list(TECH_COMBAT = 6, TECH_MATERIAL = 1, TECH_ILLEGAL = 5)
	slot_flags = SLOT_BACK
	load_method = MAGAZINE
	magazine_type = /obj/item/ammo_magazine/a762
	allowed_magazines = /obj/item/ammo_magazine/a762
	one_hand_penalty = 9
	wielded_item_state = "arifle-wielded"

	//Assault rifle, burst fire degrades quicker than SMG, worse one-handing penalty, slightly increased move delay
	firemodes = list(
		list(mode_name="semiauto",       burst=1, fire_delay=0,    move_delay=null, one_hand_penalty=2, burst_accuracy=null, dispersion=null),
		list(mode_name="2-round bursts", burst=2, fire_delay=null, move_delay=2,    one_hand_penalty=2, burst_accuracy=null,       dispersion=null),
		list(mode_name="short bursts",   burst=5, fire_delay=null, move_delay=6,    one_hand_penalty=9, burst_accuracy=list(0,-1,-2,-3,-3), dispersion=list(0.6, 1.0, 1.2, 1.2, 1.5)),
		)

/obj/item/weapon/gun/projectile/automatic/wolfoldassault/update_icon()
	icon_state = (ammo_magazine)? "assaultold" : "assaultold-e"
	wielded_item_state = (ammo_magazine)? "arifle-wielded" : "arifle-wielded-empty"