/var/datum/rk_law/primary/command = new(do_log = 1, do_newscast = 1)


/datum/rk_law
	var/title = "Law"
	var/lawmaker = ""
	var/log = 1
	var/newscast = 1
	var/channel_name = "Law Book of the Reichskomissariat"
	var/law_type = "Primary"

/datum/rk_law/New(var/do_log = 0, var/new_sound = null, var/do_newscast = 0)
	log = do_log
	newscast = do_newscast

/datum/rk_law/primary
	title = "Law"
	law_type = "Primary"

/datum/rk_law/primary/command/New(var/do_log = 1, var/new_sound = 'sound/misc/notice2.ogg', var/do_newscast = 0)
	..(do_log, new_sound, do_newscast)
	title = "Another Law"
	law_type = "[command_name()] Law"

/datum/rk_law/proc/Announce(var/message as text, var/new_title = "", var/new_sound = null, var/do_newscast = newscast, var/msg_sanitized = 0, var/zlevels = GLOB.using_map.contact_levels)
	if(!message)
		return
	var/message_title = new_title ? new_title : title

	if(!msg_sanitized)
		message = sanitize(message, extra = 0)
	message_title = sanitizeSafe(message_title)

	var/msg = FormMessage(message, message_title)
	for(var/mob/M in GLOB.player_list)
		if((M.z in (zlevels | GLOB.using_map.admin_levels)) && !istype(M,/mob/new_player) && !isdeaf(M))
			to_chat(M, msg)

	if(do_newscast)
		NewsCast(message, message_title)

	if(log)
		log_say("[key_name(usr)] has adopted a law\a : [message_title] - [message]")
		message_admins("[key_name_admin(usr)] has adopted a law: [message].", 1)

/datum/rk_law/proc/FormMessage(message as text, message_title as text)
	. = "<h1 class='alert'>A New Law Adopted!</h1>"
	. += "<br><span class='alert'>[message]</span><br>"
	. += "<br>"


/datum/rk_law/proc/NewsCast(message as text, message_title as text)
	if(!newscast)
		return

	var/datum/news_announcement/news = new
	news.channel_name = channel_name
	news.author = lawmaker
	news.message = message
	news.message_type = law_type
	news.can_be_redacted = 0
	announce_newscaster_news(news)
