#define TAG_CULTURE   "culture"
#define TAG_EDUCATION "education"
#define TAG_FACTION   "faction"
#define TAG_RELIGION  "religion"

#define ALL_CULTURAL_TAGS list( \
	TAG_CULTURE =   "Culture", \
	TAG_EDUCATION = "Education", \
	TAG_FACTION =   "Faction", \
	TAG_RELIGION =  "Beliefs" \
	)

// Cultural IDs.
#define EDUCATION_NONE         "None"
#define EDUCATION_DROPOUT      "Dropout"
#define EDUCATION_HIGH_SCHOOL  "High School"
#define EDUCATION_TRADE_SCHOOL "Trade School"
#define EDUCATION_UNDERGRAD    "Bachelor's Degree"
#define EDUCATION_MASTERS      "Master's Degree"
#define EDUCATION_DOCTORATE    "Doctorate"
#define EDUCATION_MEDSCHOOL    "Medical Degree"

#define FACTION_TEUTONIA                 "Teutonian Empire"
#define FACTION_ALBION                   "Albion Kingdom"
#define FACTION_FRANKONIA                "Frankonia Republic"
#define FACTION_ITALOROMANIA             "Italo-Romanian Confederation"
#define FACTION_SLAV                     "Soviet Federative Republic of Slavia"
#define FACTION_IBERIA                   "Nova-Iberian State"
#define FACTION_UNITEDCOLONIES           "Federation of United Colonies"
#define FACTION_ARABIA                   "Arabian Sultanate"
#define FACTION_NIPPONIA                 "Nipponia Shogunate"
#define FACTION_NOVACHINESE_DYNASTIES    "United Novachinese Dynasties"
#define FACTION_WHITESLAV                "Slavian Empire in Exile"
#define FACTION_OTHER                    "Other Faction"
#define FACTION_TEST_SUBJECTS            "Test Subjects"

#define CULTURE_OTHER          "Other Culture"
#define CULTURE_HUMAN          "Humankind"
#define CULTURE_HUMAN_VATGROWN "Nonborn"
#define CULTURE_HUMAN_TEUTONIRICH "Teutonian, Upper Class"
#define CULTURE_HUMAN_TEUTONPOOR "Teutonian, Lower Class"
#define CULTURE_HUMAN_ALBION "Albionian"
#define CULTURE_HUMAN_FRANKON "Frakonian"
#define CULTURE_HUMAN_ITALIAN "Italo-Romanian"
#define CULTURE_HUMAN_DANUBIAN "Commonwealth resident"
#define CULTURE_HUMAN_SLAVIAN   "Slavian"
#define CULTURE_HUMAN_IBEROLAT    "Iberian"
#define CULTURE_HUMAN_UNITEDCOLONIES   "Unicolonial"
#define CULTURE_HUMAN_ARABIAN    "Arabian"
#define CULTURE_HUMAN_HINDUSTAN   "Hindustanian"
#define CULTURE_HUMAN_NIPPONIAN   "Nipponian"
#define CULTURE_HUMAN_NOVACHINESE   "Neo-Chinese"
#define CULTURE_HUMAN_OTHER    "Other, Humanity"
#define CULTURE_STARLIGHT      "Starlit Realms"
#define CULTURE_CULTIST        "Blood Cult"
#define CULTURE_MONKEY         "Monkey Business"
#define CULTURE_FARWA          "Farwa Business"
#define CULTURE_NEARA          "Neara Business"
#define CULTURE_STOK           "Stok Business"
#define CULTURE_ALIUM          "Mysterious Aliens"

#define RELIGION_OTHER         "Other Religion"
#define RELIGION_JUDAISM       "Judaism"
#define RELIGION_HINDUISM      "Hinduism"
#define RELIGION_BUDDHISM      "Buddhism"
#define RELIGION_ISLAM         "Islam"
#define RELIGION_CHRISTIANITY  "Christianity"
#define RELIGION_AGNOSTICISM   "Agnosticism"
#define RELIGION_DEISM         "Deism"
#define RELIGION_ATHEISM       "Atheism"
#define RELIGION_THELEMA       "Thelema"
#define RELIGION_SPIRITUALISM  "Spiritualism"
#define RELIGION_NEOPAGAN      "Neopaganism"


// Other culture values.
#define EDUCATION_TIER_NONE      0
#define EDUCATION_TIER_DROPOUT   1
#define EDUCATION_TIER_BASIC     2
#define EDUCATION_TIER_TRADE     3
#define EDUCATION_TIER_BACHELOR  4
#define EDUCATION_TIER_MASTERS   5
#define EDUCATION_TIER_DOCTORATE 6
#define EDUCATION_TIER_MEDSCHOOL 7

// Nabber grades.
#define EDUCATION_NABBER_CMINUS "Grade C-"
#define EDUCATION_NABBER_C      "Grade C"
#define EDUCATION_NABBER_CPLUS  "Grade C+"
#define EDUCATION_NABBER_BMINUS "Grade B-"
#define EDUCATION_NABBER_B      "Grade B"
#define EDUCATION_NABBER_BPLUS  "Grade B+"
#define EDUCATION_NABBER_AMINUS "Grade A-"
#define EDUCATION_NABBER_A      "Grade A"
#define EDUCATION_NABBER_APLUS  "Grade A+"