/datum/ideology
	var/name = "default"
	var/p_name = ""
	var/description = ""
	var/global_leader = ""
	var/mob/living/carbon/human/station_leader = null
	var/ideology_flag = null

	var/has_leader = 0
	var/is_legal = 1
	var/is_election_started = 0

	var/list/mob/living/carbon/human/followers = list()
	var/list/mob/living/carbon/human/election_candidates = list()

	var/area/elections_area

/datum/ideology/New()
	..()
	name = ideology_flag

/datum/ideology/proc/choose_leader()
	if(station_leader && has_leader)
		station_leader = null
		has_leader = 0
	var/list/candidates = list()
	for(var/mob/living/carbon/human/H in followers)
		if(H.stat == DEAD || H.stat == UNCONSCIOUS || H.mind.special_role || !H.client)
			continue
		candidates.Add(H)
	if(!candidates.len)
		return
	station_leader = pick(candidates)
	to_chat(station_leader, "<b>������ �� - ����� ��������� [src.ideology_flag]!</b>")
	has_leader = 1

/datum/ideology/proc/initiate_elections(var/mob/initiator, var/area/A)
	is_election_started = 1

	var/leader_votes = 0
	var/leader = null

	elections_area = A

	for(var/mob/living/M in elections_area)
		if(M.mob_ideology == src)
			to_chat(M, "<h2><b>[initiator.name] ����������� ����������� �� ������ ���������!</b></h2><br>")
			to_chat(M, "<span class='alert'>� ����� ���� 180 ������ �� ��, ����� ������� ������ ���������.</span>")
			M.verbs += /mob/living/carbon/human/proc/choose_candidate

	spawn(1800)
		if(!election_candidates.len)
			for(var/mob/M in followers)
				to_chat(M, "<h2><b>����� �� ������������ �� ������ ������.</b></h2>")
				is_election_started = 0
				return

		for(var/candidate in election_candidates)
			if(!leader_votes && !leader)
				leader_votes = election_candidates[candidate]
				leader = candidate
			if(election_candidates[candidate] > leader_votes)
				leader_votes = election_candidates[candidate]
				leader = candidate
			if(election_candidates[candidate] < leader_votes)
				continue
			if(election_candidates[candidate] == leader_votes)
				if(rand(50))
					leader = candidate
					leader_votes = election_candidates[candidate]

		var/mob/winner = leader
		for(var/mob/M in followers)
			to_chat(M, "<h2><B>[winner.name] ������ �������!</B><h2>")

		station_leader = winner
		has_leader = 1
		is_election_started = 0
		elections_area = null

/mob/living/carbon/human/verb/ideology_info()
	set name = "Ideology"
	set category = "Politics"

	if(src.mob_ideology)
		var/message = "<span class='notice'>��������&#255;: <b>[src.mob_ideology.name]</b>, "
		if(src.mob_ideology.has_leader)
			message += "����� ��������� <b>[src.mob_ideology.station_leader.real_name]</b>."
		else
			message += "��� ������������ ������."
		message += "<br>[src.mob_ideology.description]</span>"
		to_chat(usr, message)
	else
		to_chat(usr, "�� �� ��������������� ���������")


/mob/living/carbon/human/proc/start_election()
	set category = "Politics"
	set name = "Elect a Leader"

	var/area/A = get_area(src)
	var/area_followers = 0
	var/active_followers = 0

	if(!mob_ideology)
		src.verbs -= /mob/living/carbon/human/proc/start_election
		return

	if(istype(A, /area/space))
		return

	if(!mob_ideology.is_election_started)
		for(var/mob/living/carbon/human/H in A)
			if(H.mob_ideology == src.mob_ideology && H.client && H.stat == CONSCIOUS && !H.restrained())
				area_followers++
		for(var/mob/living/carbon/human/H in mob_ideology.followers)
			if(H.client && H.stat == CONSCIOUS)
				active_followers++
		if(area_followers >= round(active_followers * 0.75))
			mob_ideology.initiate_elections(src, A)
		else
			to_chat(src, "<span class='alert'>���� ����, ����� ������� ������ ������, ���������� ���������� � ����� ���� ��� ������� 75% �������� ������ ����� ���������.</span>")
	else
		to_chat(src, "<span class='alert'>������ ��� ��������!</span>")

/mob/living/carbon/human/proc/choose_candidate()
	set name = "Vote Politican"
	set category = "Politics"

	if(!mob_ideology.is_election_started)
		src.verbs -= /mob/living/carbon/human/proc/choose_candidate
		return

	var/list/mob/candidates = list()
	for(var/mob/M in mob_ideology.elections_area)
		if(M.client && M.stat == CONSCIOUS)
			candidates += M

	var/mob/choice = input("������ ������ ���������.", "������!") as mob in candidates
	if(!choice || !mob_ideology.is_election_started)
		return
	if(mob_ideology.election_candidates[choice])
		mob_ideology.election_candidates[choice]++
	else
		mob_ideology.election_candidates[choice] = 1

	src.verbs -= /mob/living/carbon/human/proc/choose_candidate


/mob/living/carbon/human/proc/propaganda(mob/M as mob in able_mobs_in_oview(src))
	set name = "Propaganda"
	set category = "Politics"
	if(src == mob_ideology.station_leader)
		if(!M.mind || !M.client)
			return
		convert_ideology(M.mind, M.mob_ideology)
	else
		to_chat(src,"<span class='danger'>�� �� ����������� ������� ���������.</span>")
		return

/mob/living/proc/convert_ideology(var/datum/mind/player, var/ideology)
	for(var/mob/living/carbon/human/H)
		if(H.mob_ideology == src.mob_ideology && H.client && H.stat == CONSCIOUS && !H.restrained())
			to_chat(src,"<span class='danger'>\ [player.current] � ���� ����� � ��� �� ���������!</span>")
			return
		else
			var/choice = alert(H,"���������� [src]: ������ �� �� ����� [src.mob_ideology.name]?","������� ���� [src.mob_ideology.name]?","���!","��!")
			if(choice == "��!" && H.mob_ideology == src.mob_ideology)
				to_chat(src, "<span class='notice'>\ [H] ������� ���� [src.mob_ideology.name]!</span>")
				message_admins("<span class='danger'>[src]([src.ckey]) ������ ��������� [H] �� [src.mob_ideology.name].</span>")
				return
			else
				to_chat(player, "<span class='danger'>�� ������������� �� ������������ �����������!</span>")
		to_chat(src, "<span class='danger'>\ [H] �� ������������  ���� [src.mob_ideology.name]!</span>")