/var/global/list/ideologies = list( \
	"Paternal Militaristic" = new/datum/ideology/patmil(), \
	"Social Conservative" = new/datum/ideology/soccon(), \
	"Social Liberal" = new/datum/ideology/soclib(), \
	"Social Democratic" = new/datum/ideology/socdem(), \
	"Market Liberal" = new/datum/ideology/marklib(), \
	"National Populism" = new/datum/ideology/natpop(), \
	"Neo Bolshevism" = new/datum/ideology/neobolshevism(), \
	"Radical Socialism" = new/datum/ideology/radsoc() \

	)

/datum/ideology/patmil
	p_name = "Teutonic National Party"
	description = "Paternalism-militarism is more likely not an ideology, but a method of governing the state, which is built on the principle of state control over people. Similar to father control of children in a patriarchal family."
	global_leader = "Richard Sterke"
	ideology_flag = IDEOLOGY_PATMIL

/datum/ideology/soccon
	p_name = "Teutonic Conservative Party"
	description = "Social conservatism is the belief that society is built upon a fragile network of relationships which need to be upheld through duty, traditional values and established institutions."
	global_leader = "Willhelm von Kuchel"
	ideology_flag = IDEOLOGY_SOCCON

/datum/ideology/soclib
	p_name = "Progress Party"
	description = "Social liberalism is a political ideology and a variety of liberalism that endorses a market economy and the expansion of civil and political rights while also believing that the legitimate role of the government includes addressing economic and social issues such as poverty, health care and education."
	global_leader = "Paul Kerner"
	ideology_flag = IDEOLOGY_SOCLIB

/datum/ideology/socdem
	p_name = "Social Democratic Labor Party of Teutonia"
	description = "Social democracy is a political, social and economic ideology that supports economic and social interventions to promote social justice within the framework of a liberal democratic polity and capitalist economy."
	global_leader = "Thomas Brinkerhoff"
	ideology_flag = IDEOLOGY_SOCDEM

/datum/ideology/marklib
	p_name = "Liberal Party of Teutonia"
	description = "Market liberalism depicts a political ideology, combining free market economy with personal liberty and human rights, in contrast to social liberalism, which combines personal liberty and human rights along with a mixed economy and welfare state.."
	global_leader = "Valdemar Rosenkranc"
	ideology_flag = IDEOLOGY_MARKLIB

/datum/ideology/natpop
	p_name = "Teutonic National Democratic Party"
	description = "National Populism is a term used to describe a variety of ultra-nationalist, radical religious, and militaristic movements. National populism typically venerates devotion to the state, uniting the people under a strong leader and a corporatist economy."
	global_leader = "Lorenz August Henlein"
	ideology_flag = IDEOLOGY_NATPOP

	is_legal = 0

/datum/ideology/neobolshevism
	p_name = "United Socialist Party of Teutonia"
	description = "Neobolshevism is a political ideology based upon state controlled economics, nationalism, the total involvement of the state in internal affairs and the importance of the state in preserving socialism."
	global_leader = "Joachim Bachmeier"
	ideology_flag = IDEOLOGY_NEOBOLSHEVISM

	is_legal = 0

/datum/ideology/radsoc
	p_name = "Rote Armee Fraktion - Teutonia"
	description = "Radical socialism is used to describe a socialist system with no tolerance for capitalism. It might still be democratic, but the �means of production� can no longer be owned, and are considered the collective property of the people who use them."
	global_leader = "Katarina Rothbauer"
	ideology_flag = IDEOLOGY_RADSOC

	is_legal = 0

