/decl/cultural_info/faction/teutonia
	name = FACTION_TEUTONIA
	description = "�������� ��� ���������� ������� - ������������� ��������\
	���������� �� ��������� ���������� ������������� �����.\
	������������� ���������� �������, � ������ ��������, �������������� �� ������� ���-������.,\
	������� ������ � ������� ����������� �� �� ������� � ���������� �� ��������.\
	�� ������ ������ �������� ���������� � �������� ����� � ����������� �����������. \
	������ ����������� �������� ���������� ������� �������� (������), ��������� ��������-������� � �������������, \
	������� �������� ����� � �������. diverse. ������ ��������, ��� ����������� �������� ��� ������ ���������������� \
	�������� ������ ������������� �����, �� ��� ����� �� ��������� �� ����� ����."
	ruling_body = "��� ������������� ���������� ������ ������� IV � ������� ������ ������"
	capital = "���-������"
	language = LANGUAGE_TEUTONIA
	ideology = IDEOLOGY_PATMIL

/decl/cultural_info/faction/albion
	name = FACTION_ALBION
	description = "����������� ����������� - �����������, ������������� � ���������� ��������. \
	����������� ����������� ����������� � ��������� �� �������-���������. �������� ����� ������� ��������� � ����������� �������������� �����.\
	���������� ������� � ������� ����������� � ���������� ������� � � � ����������� ����������. \
	���� ��������� ����� �������� ����� ��������������� � �������� ���������������. ������ ������� � ��� ������� ������� �����.\
	���� ��� ����������� ����� ����� ������������� ��������� � ��������� ����������, ��������, ����� ��������� � �� NanoTrasen ������ ������������ \
	� ��������������."
	economic_power = 1.2
	subversive_potential = 50
	ruling_body = "�������-������� ������ ���������� (��� ����������� ���������� ������ VII)"
	capital = "���������-����"
	language = LANGUAGE_ALBION
	ideology = IDEOLOGY_MARKLIB

/decl/cultural_info/faction/frankonia
	name = FACTION_FRANKONIA
	description = "����������� ���������� - ������ ����� ���������� �������. \
	����������� ������������ �������� ����������� ������������ �����������. �� �� � ����������� ����������������� ����� ���������.\
	��������� ����� ��� �� ���������, ��� ��� ��� ���������� �� ������ ������� �� ���������� �����-������� �, ��������, ���������� ��������� ���\
	����� �������������. �� ��������� ����� ���� ��������� �����. ��������� �������� ����� ���������, ������� ��� ������ ������ �� ���������� �����.\
	�� ���� ����� ������ ��, ��� ��� ������ � ����� � ������ ����� ������ �����. �� �� �� ����������� ������ �������� ����������� ������� � ���������\
	���� � ���������."
	economic_power = 1.2
	subversive_potential = 35
	ruling_body = "��������� ������ �����"
	capital = "����-�����"
	language = LANGUAGE_FRANKON
	ideology = IDEOLOGY_SOCDEM

/decl/cultural_info/faction/italoromania
	name = FACTION_ITALOROMANIA
	description = "�����-���������� ������������� - ���� ������ ����� � ������. ����� �����������, ����� � ���� ����� ������.\
	�� ������� ������� ���������� ����� � ������������� ��������� �������� ������������\
	��� ����������� ������� ���������������, �� ������� ������� ������ � ����������� �����������.\
	�����-������� �������� ������ ��������, ��������, �������������� ��������� ����� ������ ��������� ������� ��������. \
	������ ������������, ��� ����� ���������� ������ �����-������� �� ������� � ���������� �����������.\
	������ ����� ����� � �� ���������, ���� � ����������� �����, ��� ������� ����� � ��� ��� � ��� �����."
	economic_power = 0.9
	subversive_potential = 50
	ruling_body = "��������� ������ ����� ���������"
	capital = "�����"
	language = LANGUAGE_ITALIAN
	ideology = IDEOLOGY_SOCCON

/decl/cultural_info/faction/slav
	name = FACTION_SLAV
	description = "����������������� ������������� ���������� ������� - ���-�������������� ����������� �� ���� ���������� �������.\
	����������� ��������� � ���� �������� ����������� ����� ����� ���-������������ � ����� �������, ������������ ��-�� �������� ����������\
	� ���������� �������. ����������� ��������������� ���������� ������������� ���������, ������ ����� ����� ���� � �������� ���������.\
	�� �� ���������� �������� �������� ���������� �������� ��������, ��� ��������, ������, ��� � ������������ ���������. \
	������� ������ ������ ������ �� ����� �� ����, ��������� �������� ����������� ������������ �� ���� ����������������� ����������, \
	������� ����� �� ������� �������� �������������� ����������������� �����������.\
	����� ������ ���� �������� ������ ������������� � ������ ������������, � ������ ������� ���� ������ ������� ������,\
	������� ����� � ���� ������ �� ����� ����������������� ����."
	ruling_body = "����������� ��������� �� ���� ������� ������"
	capital = "����������"
	language = LANGUAGE_SLAV
	ideology = IDEOLOGY_NEOBOLSHEVISM

/decl/cultural_info/faction/iberia
	name = FACTION_IBERIA
	description = "TBA."
	economic_power = 1.2
	subversive_potential = 15
	ruling_body = "�������� ���� ���������"
	capital = "�����"
	language = LANGUAGE_IBEROLAT
	ideology = IDEOLOGY_NATPOP

/decl/cultural_info/faction/uncolonies
	name = FACTION_UNITEDCOLONIES
	description = "TBA."
	economic_power = 1.1
	subversive_potential = 15
	language = LANGUAGE_GALCOM
	ruling_body = "��������� ��� ���� ��������"
	capital = "�������"
	ideology = IDEOLOGY_PATMIL

/decl/cultural_info/faction/arabia
	name = FACTION_ARABIA
	description = "TBA."
	economic_power = 1.2
	subversive_potential = 15
	language = LANGUAGE_ARABIAN
	ruling_body = "������ ������ VI ���-������"
	capital = "�������-��-�������"
	ideology = IDEOLOGY_PATMIL

/decl/cultural_info/faction/nipponia
	name = FACTION_NIPPONIA
	description = "TBA."
	economic_power = 1.3
	subversive_potential = 15
	language = LANGUAGE_NIPPONIAN
	ruling_body = "Ѹ��� ��������-�� �������"
	capital = "������� �����"
	ideology = IDEOLOGY_PATMIL

/decl/cultural_info/faction/novachinese_dynasties
	name = FACTION_NOVACHINESE_DYNASTIES
	description = "TBA."
	subversive_potential = 15
	language = LANGUAGE_NOVACHINESE
	ruling_body = "������������� �����"
	capital = "��������� �������"
	ideology = IDEOLOGY_PATMIL

/decl/cultural_info/faction/slav/white
	name = FACTION_WHITESLAV
	description = "TBA."
	economic_power = 1.1
	subversive_potential = 15
	language = LANGUAGE_SLAV
	secondary_langs = LANGUAGE_FRANKON
	ruling_body = "��� ������������� ���������� ��������� I"
	capital = "������� ������-13"
	ideology = IDEOLOGY_PATMIL


/decl/cultural_info/faction/other
	name = FACTION_OTHER
	description = "You belong to one of the many other factions that operate in the galaxy. Numerous, too numerous to list, these factions represent a variety of interests, purposes, intents and goals."
	subversive_potential = 25
	language = LANGUAGE_GALCOM
