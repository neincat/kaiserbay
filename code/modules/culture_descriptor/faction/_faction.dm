/decl/cultural_info/faction
	desc_type = "Faction"
	additional_langs = null
	secondary_langs = null
	category = TAG_FACTION
	var/mob_faction = MOB_FACTION_NEUTRAL
	var/ruling_body = FACTION_TEUTONIA
	var/capital
	var/ideology = IDEOLOGY_PATMIL

/decl/cultural_info/faction/get_text_details()
	. = list()
	if(!isnull(capital))
		. += "<b>Capital:</b> [capital]."
	if(!isnull(ruling_body))
		. += "<b>Ruler:</b> [ruling_body]."
	if(!isnull(ideology))
		. += "<b>Ideology</b> [ideology]."
	. += ..()
