/decl/cultural_info/culture/generic
	name = CULTURE_OTHER
	description = "You are from one of the many small, relatively unknown cultures scattered across the galaxy."

/decl/cultural_info/culture/human
	name = CULTURE_HUMAN
	description = "You are from one of various planetary cultures of humankind."
	language = LANGUAGE_TEUTONIA
	secondary_langs = list(LANGUAGE_GALCOM)

/decl/cultural_info/culture/human/vatgrown
	name = CULTURE_HUMAN_VATGROWN
	description = "You were grown in a vat, either as clone or as a gene-adapt, and your outlook diverges from baseline humanity accordingly."
	language = LANGUAGE_GALCOM
	secondary_langs = list(LANGUAGE_TEUTONIA)

/decl/cultural_info/culture/human/vatgrown/sanitize_name(name)
	return sanitizeName(name, allow_numbers=TRUE)

/decl/cultural_info/culture/human/vatgrown/get_random_name(gender)
	// #defines so it's easier to read what's actually being generated
	#define LTR ascii2text(rand(65,90)) // A-Z
	#define NUM ascii2text(rand(48,57)) // 0-9
	#define NAME capitalize(pick(gender == FEMALE ? GLOB.first_names_female : GLOB.first_names_male))
	switch(rand(1,4))
		if(1) return NAME
		if(2) return "[LTR][LTR]-[NAME]"
		if(3) return "[NAME]-[NUM][NUM][NUM]"
		if(4) return "[LTR][LTR]-[NUM][NUM][NUM]"
	. = 1 // Never executed, works around http://www.byond.com/forum/?post=2072419
	#undef LTR
	#undef NUM
	#undef NAME

/decl/cultural_info/culture/human/teutonic_rich
	name = CULTURE_HUMAN_TEUTONIRICH
	description = "�� �������� � ���������� �������. �������� �� ������-�� �� ���������� ������� ����������� �������� �������. \
	������������� ���������� ������������ �������� �������� ��&#255;�&#255;�� � ��������������� �������� ������� \
	������� ����� � ���������� ������� ���������� �������. ������ ������ ����� ��������� ���� ����������� �� ���������, ���� � �������� � �������� � �������������.\
	� ��� �������� ���������� ������������� ������ �������� ���������� �������. ��������, ����������, � �� ������ ����� �� ������������� ���������."
	secondary_langs = list(LANGUAGE_TEUTONIA)
	economic_power = 1

/decl/cultural_info/culture/human/teutonic_poor
	name = CULTURE_HUMAN_TEUTONPOOR
	description = "�� ������ ������� ������� ���������� �������. ��� ��� - �������, ����������, ��������� ����� �� ������������ �������, ��� ������� �����-����������� ����&#255;� ����.\
	� ������� ������ �� ������� ������������, ���������� � ����� ����-�� ������� ����� �����.\
	����� ������� ������� ������� ���� ����������� ���������� �������������� ������� ���� ���������, ������������� ��������.\
	��� ������� �� �� ����� ������� ����������� � ������ ������� ���&#255;���������� ������ ����� �����, �� �� ������ ��������� ��� ����-�� ���."
	secondary_langs = list(LANGUAGE_TEUTONIA, LANGUAGE_SIGN)
	economic_power = 0.8

/decl/cultural_info/culture/human/albion
	name = CULTURE_HUMAN_ALBION
	description = "�� ��������� - ���� ����� ���� ��� ������������ � ������� ���������� �������. �� ������ ���� ��������&#255;������ �������������, ������� �������� �������, �� ��� �� ��� �����, \
	��� ��� ��������. �� ������������� �������, ������������, ��������� ������������� ������. �� ������� ���� �� ��� ��������\
	� ������� �� ������� ���� ����������� ����� ���� � �������� ������������� ����� � ������� ������� ���-���������, ���� ���� �� ��� �� ����."
	secondary_langs = list(LANGUAGE_TEUTONIA, LANGUAGE_ALBION)
	economic_power = 0.5

/decl/cultural_info/culture/human/frankon
	name = CULTURE_HUMAN_FRANKON
	description = "�������, ���������! �� ��������� - ������������� ������-����� ���������. ���� �������&#255; ����� - ������������.\
	�� ������� ����� �����, ����� ������ � ������ ������� �� ����� �� ���������. �� �� ������� �� ����� �������� ���&#255; ���� � �������������� �� ���&#255;� ���������. \
	�� � ������ ������ ������� � ���&#255; ��������: �������. ���������. ��������."
	secondary_langs = list(LANGUAGE_TEUTONIA, LANGUAGE_FRANKON)
	economic_power = 0.5

/decl/cultural_info/culture/human/italiy
	name = CULTURE_HUMAN_ITALIAN
	description = "�� �����-�������. ���� ����&#255; - ���������� ����� �����&#255;�����, ������� � ��������� �������. �� �������� � ������� ������������, ��������� �������� ���������� �������\
	�� ������� � ������ ������, ��� ���������, ������� ��� � ����������. �� ������ ����� � ����� �� ������ ������� ����� � ����"
	secondary_langs = list(LANGUAGE_TEUTONIA, LANGUAGE_ITALIAN)
	economic_power = 0.6

/decl/cultural_info/culture/human/slav
	name = CULTURE_HUMAN_SLAVIAN
	description = "�� �������. ������� ����, ������������������ � ���������� ��������� ������ ������ - ��� ��� ���, \
	�� ����� ���� ��������� � ������&#255;� ���������� � ����, � ����� ���� ��������� � ������&#255;� ����������-������� �������������� ����� ����� ������ \
	��� �� �� ������ ������ ��� �������� � ������ � ����� ����."
	secondary_langs = list(LANGUAGE_TEUTONIA, LANGUAGE_SLAV)
	economic_power = 0.5

/decl/cultural_info/culture/human/iberolat
	name = CULTURE_HUMAN_IBEROLAT
	description = "������������ - ������� ����� ������������ ����. ������ - ������ ������� ����� ��������, ��� � �������� ����� \
	���� � ����� ����� ������ ������ ����, ��� � ������. �� ������ � ������, � ���������, � ����."
	secondary_langs = list(LANGUAGE_TEUTONIA, LANGUAGE_IBEROLAT)
	economic_power = 0.4

/decl/cultural_info/culture/human/unitedcolonies
	name = CULTURE_HUMAN_UNITEDCOLONIES
	description = "�� �� ������������ �������. ���� �������� - ����� ������ ������� ������������ ��������, � ��� ����� � ���������� \
	�� ������ ������ � ���������������� ������ �������. ��� �� ����� ����� ��������, �����������. ���������� � ����� ����� �����."
	secondary_langs = list(LANGUAGE_TEUTONIA, LANGUAGE_SIGN)
	economic_power = 0.7

/decl/cultural_info/culture/human/arabian
	name = CULTURE_HUMAN_ARABIAN
	description = "You are from Ceti Epsilon, the technical hub of the SCG. As a Cetite you are no stranger to the cutting edge of technology present in Sol space. \
	Putting education and the latest tech at the forefront of their priorities the people of Ceti are some of the brightest or tech savvy around. \
	This has afforded those from the system or planet a reputation as being a cut above the rest in technical matters, with those who attended the Ceti Institute of Technology \
	being considered some of the best qualified technical specialists in humanity. Recently there has been a rising transhumanist element in Ceti society resulting in a large \
	cybernetics culture; it is not uncommon to see many Cetites sporting some chrome."
	secondary_langs = list(LANGUAGE_TEUTONIA, LANGUAGE_ARABIAN)
	economic_power = 0.3


/decl/cultural_info/culture/human/nipponia
	name = CULTURE_HUMAN_NIPPONIAN
	description =  "You are from the void between worlds, though you are in the distant, vast frontier of SCG space and beyond. Out here things like national identity and culture mean less; \
	those who live so far from anything only look to their close family and friends rather than any larger group. Raised on one of the long haul freighters that move between frontier worlds delivering \
	vital goods, a lonely outpost on the edge of a dreary backwater, such people are raised in small, confined environments with few others, and tend to be most familiar with older, reliable but outdated \
	technology. An independent sort, people on the frontier are more likely to be isolationist and self-driven."
	secondary_langs = list(LANGUAGE_TEUTONIA, LANGUAGE_NIPPONIAN, LANGUAGE_SIGN)
	economic_power = 0.4

/decl/cultural_info/culture/human/novachinese
	name = CULTURE_HUMAN_NOVACHINESE
	description = "You are from Terra (not Earth), in the Gilgamesh system. The capital world of the Terran Colonial Confederation, your people embody what it means to be a part of the TCC. \
	Unfortunately, the years since the war have not been easy on Terra and the long period of economic recovery has not made life easy. The people of Terra are typically employed \
	in the military, industrial, government or service sectors, with an emphasis being placed on military service. Terrans today are generally poor, bitter and a somewhat broken people angry and \
	resentful about their loss in the Gaia Conflict. An upbringing on Terra emphasises an odd mix of service to the state, liberalism and militarism."
	secondary_langs = list(LANGUAGE_TEUTONIA, LANGUAGE_NOVACHINESE)
	economic_power = 0.2

/decl/cultural_info/culture/human/other
	name = CULTURE_HUMAN_OTHER
	description = "�� �� ����������� ����������, ����������, ���������� ������������. "
	secondary_langs = list(LANGUAGE_TEUTONIA, LANGUAGE_SIGN)
	economic_power = 1
