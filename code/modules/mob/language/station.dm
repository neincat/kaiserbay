
datum/language/human
	name = LANGUAGE_TEUTONIA
	desc = "A mixture of German, Swedish, Norwegian, Danish and Dutch. The official language of the Teutonic Empire."
	speech_verb = "says"
	whisper_verb = "whispers"
	colour = "solcom"
	key = "1"
	flags = WHITELISTED
	shorthand = "Teu"

	//syllables are at the bottom of the file

/datum/language/human/get_spoken_verb(var/msg_end)
	switch(msg_end)
		if("!")
			return pick("exclaims","shouts","yells") //TODO: make the basic proc handle lists of verbs.
		if("?")
			return ask_verb
	return speech_verb

/datum/language/human/get_random_name(var/gender)
	if (prob(80))
		if(gender==FEMALE)
			return capitalize(pick(GLOB.first_names_female)) + " " + capitalize(pick(GLOB.last_names))
		else
			return capitalize(pick(GLOB.first_names_male)) + " " + capitalize(pick(GLOB.last_names))
	else
		return ..()

/datum/language/machine
	name = LANGUAGE_EAL
	desc = "An efficient language of encoded tones developed by synthetics and cyborgs."
	speech_verb = "whistles"
	ask_verb = "chirps"
	exclaim_verb = "whistles loudly"
	colour = "changeling"
	key = "6"
	flags = NO_STUTTER
	syllables = list("beep","beep","beep","beep","beep","boop","boop","boop","bop","bop","dee","dee","doo","doo","hiss","hss","buzz","buzz","bzz","ksssh","keey","wurr","wahh","tzzz")
	space_chance = 10
	shorthand = "EAL"

/datum/language/machine/can_speak_special(var/mob/living/speaker)
	return speaker.isSynthetic()

/datum/language/machine/get_random_name()
	if(prob(70))
		return "[pick(list("PBU","HIU","SINA","ARMA","OSI"))]-[rand(100, 999)]"
	return pick(GLOB.ai_names)

/datum/language/human/syllables = list(
"an", "au", "abe", "ach", "and", "auf", "be", "ben", "ber", "bah", "bur", "bor", "bir", "byr", "borg", "barh",
"ch", "che", "cha", "chu", "chw", "che", "cra", "cro", "cru", "cri", "da", "de", "di", "du",
"der", "dar", "dor", "die", "ein", "ei", "el", "er", "en", "es", "ere", "ers", "ese", "ge",
"gen", "he", "ht", "hen", "ich", "ige", "ine", "ist", "ic", "ie", "in", "is", "it", "le", "li",
"lic", "lle", "men", "mit", "nd", "ne", "no", "ng", "nde", "nen", "nge", "nic", "nte",
"ren", "re", "ra", "ri", "rit", "rot", "rat", "ryt", "ry", "ro", "sc", "sch", "sei", "sen", "ste",
"sie", "sic", "ten", "ter", "tar", "tyr", "tir", "tyl", "tul", "tal", "te", "un", "und", "ung", "uns",
"unr", "ver", "ve", "vyr", "var", "vor")

/datum/language/albionish
	name = LANGUAGE_ALBION
	desc = "A language that is a heavily modified version of English used in the Albion Empire."
	colour = "tajaran"
	speech_verb = "speaks"
	key = "al"
	syllables = list("al","an","ar","as","at","ea","ed","en","er","es","ha",
	"he","hi","in","is","it","le","me","nd","ne","ng","nt","on",
	"or","ou","re","se","st","te","th","ti","to","ve","wa","his","her","was","did","why")
	shorthand = "ALB"

/datum/language/frankon
	name = LANGUAGE_FRANKON
	desc = "The official language of the Franonian Republic. Mixture of French and Belgian."
	speech_verb = "speaks"
	colour = "terran"
	key = "fr"
	syllables = list("rus","zem","ave","groz","ski","ska","ven","konst","pol","lin","svy",
	"danya","da","mied","zan","das","krem","myka","to","st","no","na","ni",
	"ko","ne","en","po","ra","li","on","byl","cto","eni","ost","ol","ego","ver","stv","pro")
	shorthand = "FR"

/datum/language/italian
	name = LANGUAGE_ITALIAN
	desc = "The official language of the Italo-Roman Confederation. A mixture of Latin and Italian"
	colour = "spacer"
	key = "it"
	syllables = list ("die", "en", "skei", "van", "son", "der", "aar", "ch", "op", "ruk", "aa", "be", "ne", "het",
 	"ek", "ras", "ver", "zan", "das", "waa", "geb", "vol", "lu", "min", "breh", "rus", "stv", "ee", "goe", "sk",
 	"la", "ver", "we", "ge", "luk", "an", "ar", "at", "es", "et", "bel", "du", "jaa", "ch", "kk", "gh", "ll", "uu", "wat")
	shorthand = "IT"

/datum/language/slav
	name = LANGUAGE_SLAV
	desc = "Slavic language - a mixture of languages of the Slavic peoples. The official language of the Slavic Empire in exile and the Soviet Federal Republic of Slavia"
	speech_verb = "speaks"
	colour = "terran"
	key = "sl"
	syllables = list("rus","zem","ave","groz","ski","ska","ven","konst","pol","lin","svy",
	"danya","da","mied","zan","das","krem","myka","to","st","no","na","ni",
	"ko","ne","en","po","ra","li","on","byl","cto","eni","ost","ol","ego","ver","stv","pro")
	shorthand = "SL"

/datum/language/iberian
	name = LANGUAGE_IBEROLAT
	desc = "Iberian is a mixture of Spanish, Portuguese, Catalan and Basque. The official language of the Iberian Commune Federation"
	speech_verb = "speaks"
	colour = "terran"
	key = "ib"
	syllables = list("rus","zem","ave","groz","ski","ska","ven","konst","pol","lin","svy",
	"danya","da","mied","zan","das","krem","myka","to","st","no","na","ni",
	"ko","ne","en","po","ra","li","on","byl","cto","eni","ost","ol","ego","ver","stv","pro")
	shorthand = "IB"

/datum/language/arabian
	name = LANGUAGE_ARABIAN
	desc = "Arabic has not changed in the last century. The official language of the Sultanate of New Arabia."
	speech_verb = "speaks"
	colour = "terran"
	key = "ar"
	syllables = list("rus","zem","ave","groz","ski","ska","ven","konst","pol","lin","svy",
	"danya","da","mied","zan","das","krem","myka","to","st","no","na","ni",
	"ko","ne","en","po","ra","li","on","byl","cto","eni","ost","ol","ego","ver","stv","pro")
	shorthand = "AR"


/datum/language/nippon
	name = LANGUAGE_NIPPONIAN
	desc = "Nippon is a mixture of Japanese, Russian, Korean. Used in the Nippon Shogunate"
	speech_verb = "speaks"
	colour = "terran"
	key = "r"
	syllables = list("rus","zem","ave","groz","ski","ska","ven","konst","pol","lin","svy",
	"danya","da","mied","zan","das","krem","myka","to","st","no","na","ni",
	"ko","ne","en","po","ra","li","on","byl","cto","eni","ost","ol","ego","ver","stv","pro")
	shorthand = "FR"


/datum/language/novachinese
	name = LANGUAGE_NOVACHINESE
	desc = "Neo-Chinese is an evolution of simplified Chinese. Used in the Shan, Tzun and Qian dynasties"
	speech_verb = "speaks"
	colour = "terran"
	key = "r"
	syllables = list("rus","zem","ave","groz","ski","ska","ven","konst","pol","lin","svy",
	"danya","da","mied","zan","das","krem","myka","to","st","no","na","ni",
	"ko","ne","en","po","ra","li","on","byl","cto","eni","ost","ol","ego","ver","stv","pro")
	shorthand = "FR"