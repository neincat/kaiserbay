var/list/stored_shock_by_ref = list()

/mob/living/proc/apply_stored_shock_to(var/mob/living/target)
	if(stored_shock_by_ref["\ref[src]"])
		target.electrocute_act(stored_shock_by_ref["\ref[src]"]*0.9, src)
		stored_shock_by_ref["\ref[src]"] = 0


/datum/species/proc/check_background(var/datum/job/job, var/datum/preferences/prefs)
	. = TRUE
	if(istype(job) && istype(prefs) && job.required_education > EDUCATION_TIER_NONE)
		var/has_suitable_education = FALSE
		for(var/culturetag in prefs.cultural_info)
			var/decl/cultural_info/culture = SSculture.get_culture(prefs.cultural_info[culturetag])
			if(culture.get_education_tier() >= job.required_education)
				has_suitable_education = TRUE
			if(job.maximum_education)
				if(culture.get_education_tier() > job.maximum_education)
					has_suitable_education = FALSE
					break
		. = has_suitable_education