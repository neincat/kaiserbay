/music_track/fatalist
	artist = "Triarii"
	title = "Fatalist"
	song = 'sound/music/fatalist.ogg'

/music_track/redemption
	artist = "Locrain"
	title = "Redemption (DooM TNT Evilution OST)"
	song = 'sound/music/redemption.ogg'

/music_track/evilincarnate
	artist = "elguitarTom"
	title = "Evil Incarnate (metal remix - DooM II OST)"
	song = 'sound/music/evilincarnate.ogg'

/music_track/dasboot
	artist = "Klaus Doldinger"
	title = "Das Boot (Single Version)"
	song = 'sound/music/dasboot.ogg'

/music_track/systemcontrol
	artist = "Lorcain"
	title = "System Control (DooM TNT Evilution OST)"
	song = 'sound/music/systemcontrol.ogg'

/music_track/containment
	artist = "Bobby Prince"
	title = "The Demons From Adrian's Pen (DooM OST)"
	song = 'sound/music/containment.ogg'
