/music_track/heil_europa
	artist = "Deutsch Marsche"
	title = "Heil Europa"
	song = 'sound/music/jukebox/heil_europa.ogg'

/music_track/automat
	artist = "Eisbrecher"
	title = "Automat"
	song = 'sound/music/jukebox/automat.ogg'

/music_track/grossenwahn
	artist = "Feindflug"
	title = "Grossenwahn"
	song = 'sound/music/jukebox/grossenwahn.ogg'

/music_track/sturmwalzer
	artist = "Feindflug"
	title = "Sturmwalzer"
	song = 'sound/music/jukebox/sturmwalzer.ogg'

/music_track/ich_will
	artist = "Heldsmaschine"
	title = "Ich Will (Rammstein cover)"
	song = 'sound/music/jukebox/ich_will.ogg'

/music_track/rot_und_schwarz
	artist = "Karel Gott"
	title = "Rot und Schwarz (Rolling Stones cover)"
	song = 'sound/music/jukebox/rot_und_schwarz.ogg'

/music_track/roboter
	artist = "Kraftwerk"
	title = "Die Roboter"
	song = 'sound/music/jukebox/roboter.ogg'