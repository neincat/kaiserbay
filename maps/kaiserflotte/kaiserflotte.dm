#if !defined(using_map_DATUM)

	#include "kaiserflotte_announcements.dm"
	#include "kaiserflotte_antagonism.dm"
	#include "kaiserflotte_areas.dm"
	#include "kaiserflotte_elevator.dm"
	#include "kaiserflotte_holodecks.dm"
	#include "kaiserflotte_lobby.dm"
	#include "kaiserflotte_npcs.dm"
	#include "kaiserflotte_overmap.dm"
	#include "kaiserflotte_presets.dm"
	#include "kaiserflotte_ranks.dm"
	#include "kaiserflotte_security_state.dm"
	#include "kaiserflotte_shuttles.dm"
	#include "kaiserflotte_unit_testing.dm"

	#include "datums/uniforms.dm"
	#include "datums/uniforms_expedition.dm"
	#include "datums/uniforms_fleet.dm"
	#include "datums/reports.dm"
	#include "datums/shackle_law_sets.dm"
	#include "datums/supplypacks/security.dm"
	#include "datums/supplypacks/science.dm"

	#include "items/cards_ids.dm"
	#include "items/encryption_keys.dm"
	#include "items/headsets.dm"
	#include "items/items.dm"
	#include "items/machinery.dm"
	#include "items/manuals.dm"
	#include "items/stamps.dm"
	#include "items/uniform_vendor.dm"
	#include "items/rigs.dm"

	#include "items/clothing/clothing.dm"
	#include "items/clothing/reich-accessory.dm"
	#include "items/clothing/reich-armor.dm"
	#include "items/clothing/reich-feet.dm"
	#include "items/clothing/reich-head.dm"
	#include "items/clothing/reich-suit.dm"
	#include "items/clothing/reich-under.dm"

	#include "job/access.dm"
	#include "job/jobs.dm"
	#include "job/outfits.dm"

	#include "structures/closets.dm"
	#include "structures/signs.dm"
	#include "structures/closets/command.dm"
	#include "structures/closets/engineering.dm"
	#include "structures/closets/medical.dm"
	#include "structures/closets/misc.dm"
	#include "structures/closets/research.dm"
	#include "structures/closets/security.dm"
	#include "structures/closets/services.dm"
	#include "structures/closets/supply.dm"
	#include "structures/closets/exploration.dm"

	#include "loadout/_defines.dm"
	#include "loadout/loadout_accessories.dm"
	#include "loadout/loadout_eyes.dm"
	#include "loadout/loadout_gloves.dm"
	#include "loadout/loadout_head.dm"
	#include "loadout/loadout_shoes.dm"
	#include "loadout/loadout_suit.dm"
	#include "loadout/loadout_uniform.dm"
	#include "loadout/loadout_xeno.dm"
	#include "loadout/~defines.dm"

	#include "kaiserflotte-1.dmm"
	#include "kaiserflotte-2.dmm"
	#include "kaiserflotte-3.dmm"
	#include "kaiserflotte-4.dmm"
	#include "kaiserflotte-5.dmm"
	#include "kaiserflotte-6.dmm"
	#include "kaiserflotte-7.dmm"

	#include "../away/empty.dmm"
	#include "../away/mining/mining.dm"
	#include "../away/derelict/derelict.dm"
	#include "../away/bearcat/bearcat.dm"
	#include "../away/lost_supply_base/lost_supply_base.dm"
	#include "../away/marooned/marooned.dm"
	#include "../away/smugglers/smugglers.dm"
	#include "../away/magshield/magshield.dm"
	#include "../away/casino/casino.dm"
	#include "../away/yacht/yacht.dm"
	#include "../away/blueriver/blueriver.dm"
	#include "../away/slavers/slavers_base.dm"
	#include "../away/hydro/hydro.dm"
	#include "../away/mobius_rift/mobius_rift.dm"
	#include "../away/icarus/icarus.dm"
	#include "../away/errant_pisces/errant_pisces.dm"
	#include "../away/lar_maria/lar_maria.dm"


	#define using_map_DATUM /datum/map/kaiserflotte

#elif !defined(MAP_OVERRIDE)

	#warn A map has already been included, ignoring kaiserflotte

#endif

