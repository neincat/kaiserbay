/datum/map/kaiserflotte
	name = "R.K. Adler-Aquilla"
	full_name = "Reichskommissariat Adler-Aquilla"
	path = "kaiserflotte"
	flags = MAP_HAS_BRANCH | MAP_HAS_RANK

	station_levels = list(1,2,3,4,5)
	contact_levels = list(1,2,3,4,5)
	player_levels = list(1,2,3,4,5)
	admin_levels = list(6,7)
	accessible_z_levels = list("1"=1,"2"=1,"3"=1,"4"=1, "5"=1)
	usable_email_tlds = ".de"

	allowed_spawns = list("Cryogenic Storage", "Cyborg Storage")
	default_spawn = "Cryogenic Storage"

	station_name  = "Reichskommissariat Adler-Aquilla"
	station_short = "R.K. Adler-Aquilla"
	dock_name     = "TBD"
	boss_name     = "Das Kaiserliche Ministerium fur Weltraumbesiedlung"
	boss_short    = "Das Kaiserliche Ministerium"
	company_name  = "Das Teutonisches Kaiserreich"
	company_short = "Das Kaiserreich"

	map_admin_faxes = list("Das Kaiserliche Ministerium fur Weltraumbesiedlung", "Interstellare Handelsallianz", "Innenministerium", "Der kaiserliche Hof")

	//These should probably be moved into the evac controller...
	shuttle_docked_message = "Vorsicht! Jump preparation complete. The bluespace drive is now spooling up, secure all stations for departure. Time to jump: approximately %ETD%."
	shuttle_leaving_dock = "Vorsicht! Jump initiated, exiting bluespace in %ETA%."
	shuttle_called_message = "Vorsicht! Jump sequence initiated. Transit procedures are now in effect. Jump in %ETA%."
	shuttle_recall_message = "Vorsicht! Jump sequence aborted, return to normal operating conditions."

	evac_controller_type = /datum/evacuation_controller/shuttle

	default_law_type = /datum/ai_laws/solgov
	away_site_budget = 3

	id_hud_icons = 'maps/kaiserflotte/icons/assignment_hud.dmi'

/datum/map/kaiserflotte/setup_map()
	..()
	system_name = generate_system_name()
	minor_announcement = new(new_sound = sound('sound/AI/torch/commandreport.ogg', volume = 45))

/datum/map/kaiserflotte/map_info(victim)
	to_chat(victim, "<h2>Current map information</h2>")
	to_chat(victim, "Welcome to the <b>[station_name]</b>, an mining-science colony of the Teutonic Empire. The main goal of the colony is research and resource extraction.")
	to_chat(victim, "The inhabitants of the colony are civilians, Reichswehr soldiers, officials and others.")
	to_chat(victim, "This colony is far from other colonies of the empire, so count on yourself. In addition, the territory of the colony is not assigned to the Empire, so expect provocations.")

/datum/map/kaiserflotte/send_welcome()
	var/welcome_text = "<center><img src = kaiserlogo.png /><br /><font size = 3><b>R.K. Adler-Aquilla</b> Sensor messages:</font><hr />"
	welcome_text += "Report generated on [stationdate2text()] at [stationtime2text()]</center><br /><br />"
	welcome_text += "Unidentified objects noticed:<br /><b>[rand(0,10)]</b><br />"
	welcome_text += "Next watch to colony:<br /><b>[rand(10,900)] days</b><br />"
	welcome_text += "Travel time to New-Berlin:<br />95<b> days</b><br />"
	welcome_text += "Time since the last inspection:<br /><b>[rand(10,900)] days</b><br />"
	welcome_text += "Scan results show the following points of interest:<br />"
	welcome_text += "<br>No enemy signals detected.<br />"

	post_comm_message("R.K. Adler-Aquilla", welcome_text)
	minor_announcement.Announce(message = "New [GLOB.using_map.company_name] Update available at all communication consoles.")

/turf/simulated/wall //landlubbers go home
	name = "wall"

/turf/simulated/floor
	name = "plating"

/turf/simulated/floor/tiled
	name = "floor"

/decl/flooring/tiling
	name = "floor"