/decl/hierarchy/mil_uniform/heer/com //Can only be officers
	name = "Heer command"
	min_rank = 11
	departments = COM

	utility_under = /obj/item/clothing/under/reich/utility/heer/officer/
	utility_extra = list(/obj/item/clothing/head/soft/reich/cap, /obj/item/clothing/head/soft/reich/cap/officer, /obj/item/clothing/suit/storage/hooded/wintercoat/reich)

	service_under = /obj/item/clothing/under/reich/utility/heer/officer/
	service_skirt = /obj/item/clothing/under/reich/utility/heer_skirt/officer
	service_over = /obj/item/clothing/suit/storage/reich/service/expeditionary/command
	service_hat = /obj/item/clothing/head/soft/reich/cap/officer

	dress_under = /obj/item/clothing/under/reich/utility/heer/officer/
	dress_skirt = /obj/item/clothing/under/reich/utility/heer_skirt/officer
	dress_over = /obj/item/clothing/suit/dress/reich/expedition/command
	dress_hat = /obj/item/clothing/head/soft/reich/cap/officer

/decl/hierarchy/mil_uniform/heer/eng
	name = "Heer engineering"
	departments = ENG

	utility_under = /obj/item/clothing/under/reich/utility
	utility_extra = list(/obj/item/clothing/head/soft/reich/cap, /obj/item/clothing/head/soft/reich/cap/officer, /obj/item/clothing/suit/storage/hooded/wintercoat/reich)

	service_over = /obj/item/clothing/suit/storage/reich/service/expeditionary/engineering

/decl/hierarchy/mil_uniform/heer/eng/officer
	name = "Heer engineering CO"
	min_rank = 11

	utility_under = /obj/item/clothing/under/reich/utility/heer/officer//engineering
	utility_extra = list(/obj/item/clothing/head/soft/reich/cap, /obj/item/clothing/head/soft/reich/cap/officer, /obj/item/clothing/suit/storage/hooded/wintercoat/reich)

	service_under = /obj/item/clothing/under/reich/utility/heer/officer/
	service_skirt = /obj/item/clothing/under/reich/utility/heer_skirt/officer
	service_over = /obj/item/clothing/suit/storage/reich/service/expeditionary/engineering/command
	service_hat = /obj/item/clothing/head/soft/reich/cap/officer

	dress_under = /obj/item/clothing/under/reich/utility/heer/officer/
	dress_skirt = /obj/item/clothing/under/reich/utility/heer_skirt/officer
	dress_over = /obj/item/clothing/suit/dress/reich/expedition/command
	dress_hat = /obj/item/clothing/head/soft/reich/cap/officer

/decl/hierarchy/mil_uniform/heer/eng/officer/com //Can only be officers
	name = "Heer engineering command"
	departments = ENG|COM

/decl/hierarchy/mil_uniform/heer/sec
	name = "Heer security"
	departments = SEC

	utility_under = /obj/item/clothing/under/reich/utility/heer/officer/security
	utility_extra = list(/obj/item/clothing/head/soft/reich/cap, /obj/item/clothing/head/soft/reich/cap/officer, /obj/item/clothing/suit/storage/hooded/wintercoat/reich)

	service_over = /obj/item/clothing/suit/storage/reich/service/expeditionary/security

/decl/hierarchy/mil_uniform/heer/sec/officer
	name = "Heer security CO"
	min_rank = 11

	utility_under = /obj/item/clothing/under/reich/utility/heer/officer//security
	utility_extra = list(/obj/item/clothing/head/soft/reich/cap, /obj/item/clothing/head/soft/reich/cap/officer, /obj/item/clothing/suit/storage/hooded/wintercoat/reich)

	service_under = /obj/item/clothing/under/reich/utility/heer/officer/command
	service_skirt = /obj/item/clothing/under/reich/utility/heer/officer/
	service_over = /obj/item/clothing/suit/storage/reich/service/expeditionary/security/command
	service_hat = /obj/item/clothing/head/soft/reich/cap/officer

	dress_under = /obj/item/clothing/under/reich/utility/heer/officer/command
	dress_skirt = /obj/item/clothing/under/reich/utility/heer/officer/
	dress_over = /obj/item/clothing/suit/dress/reich/expedition/command
	dress_hat = /obj/item/clothing/head/soft/reich/cap/officer

/decl/hierarchy/mil_uniform/heer/sec/officer/com //Can only be officers
	name = "Heer security command"
	departments = SEC|COM

/decl/hierarchy/mil_uniform/heer/med
	name = "Heer medical"
	departments = MED

	utility_under = /obj/item/clothing/under/reich/utility/heer/medical
	utility_extra = list(/obj/item/clothing/head/soft/reich/cap, /obj/item/clothing/head/soft/reich/cap/officer, /obj/item/clothing/suit/storage/hooded/wintercoat/reich)

	service_over = /obj/item/clothing/suit/storage/reich/service/expeditionary/medical

/decl/hierarchy/mil_uniform/heer/med/officer
	name = "Heer medical CO"
	min_rank = 11

	utility_under = /obj/item/clothing/under/reich/utility/heer/officer/medical
	utility_extra = list(/obj/item/clothing/head/soft/reich/cap, /obj/item/clothing/head/soft/reich/cap/officer, /obj/item/clothing/suit/storage/hooded/wintercoat/reich)

	service_under = /obj/item/clothing/under/reich/utility/heer/officer/command
	service_skirt = /obj/item/clothing/under/reich/utility/heer/officer/
	service_over = /obj/item/clothing/suit/storage/reich/service/expeditionary/medical/command
	service_hat = /obj/item/clothing/head/soft/reich/cap/officer

	dress_under = /obj/item/clothing/under/reich/utility/heer/officer/command
	dress_skirt = /obj/item/clothing/under/reich/utility/heer/officer/
	dress_over = /obj/item/clothing/suit/dress/reich/expedition/command
	dress_hat = /obj/item/clothing/head/soft/reich/cap/officer

/decl/hierarchy/mil_uniform/heer/med/officer/com //Can only be officers
	name = "Heer medical command"
	departments = MED|COM

/decl/hierarchy/mil_uniform/heer/sup
	name = "Heer supply"
	departments = SUP

	utility_extra = list(/obj/item/clothing/head/soft/reich/cap, /obj/item/clothing/head/soft/reich/cap/officer, /obj/item/clothing/suit/storage/hooded/wintercoat/reich)
	utility_under = /obj/item/clothing/under/reich/utility/heer/supply

	service_over = /obj/item/clothing/suit/storage/reich/service/expeditionary/supply

/decl/hierarchy/mil_uniform/heer/sup/officer
	name = "Heer supply CO"
	min_rank = 11

	utility_under = /obj/item/clothing/under/reich/utility/heer/officer/supply
	utility_extra = list(/obj/item/clothing/head/soft/reich/cap, /obj/item/clothing/head/soft/reich/cap/officer, /obj/item/clothing/suit/storage/hooded/wintercoat/reich)

	service_under = /obj/item/clothing/under/reich/utility/heer/officer/command
	service_skirt = /obj/item/clothing/under/reich/utility/heer/officer/
	service_hat = /obj/item/clothing/head/soft/reich/cap/officer

	dress_under = /obj/item/clothing/under/reich/utility/heer/officer/command
	dress_skirt = /obj/item/clothing/under/reich/utility/heer/officer/
	dress_over = /obj/item/clothing/suit/dress/reich/expedition/command
	dress_hat = /obj/item/clothing/head/soft/reich/cap/officer

/decl/hierarchy/mil_uniform/heer/srv
	name = "Heer service"
	departments = SRV

	utility_extra = list(/obj/item/clothing/head/soft/reich/cap, /obj/item/clothing/head/soft/reich/cap/officer, /obj/item/clothing/suit/storage/hooded/wintercoat/reich)
	utility_under = /obj/item/clothing/under/reich/utility/heer/service

	service_over = /obj/item/clothing/suit/storage/reich/service/expeditionary/service

/decl/hierarchy/mil_uniform/heer/srv/officer
	name = "Heer service CO"
	min_rank = 11

	utility_under = /obj/item/clothing/under/reich/utility/heer/officer/service
	utility_extra = list(/obj/item/clothing/head/soft/reich/cap, /obj/item/clothing/head/soft/reich/cap/officer, /obj/item/clothing/suit/storage/hooded/wintercoat/reich)

	service_under = /obj/item/clothing/under/reich/utility/heer/officer/command
	service_skirt = /obj/item/clothing/under/reich/utility/heer/officer/
	service_over = /obj/item/clothing/suit/storage/reich/service/expeditionary/service/command
	service_hat = /obj/item/clothing/head/soft/reich/cap/officer

	dress_under = /obj/item/clothing/under/reich/utility/heer/officer/command
	dress_skirt = /obj/item/clothing/under/reich/utility/heer/officer/
	dress_over = /obj/item/clothing/suit/dress/reich/expedition/command
	dress_hat = /obj/item/clothing/head/soft/reich/cap/officer


/decl/hierarchy/mil_uniform/heer/spt
	name = "Heer command support"
	departments = SPT

	utility_under= /obj/item/clothing/under/reich/utility/heer/command
	utility_extra = list(/obj/item/clothing/head/soft/reich/cap, /obj/item/clothing/head/soft/reich/cap/officer, /obj/item/clothing/suit/storage/hooded/wintercoat/reich)

	service_under= /obj/item/clothing/under/reich/utility/heer/officer/command
	service_over = /obj/item/clothing/suit/storage/reich/service/expeditionary/command

/decl/hierarchy/mil_uniform/heer/spt/officer
	name = "Heer command support CO"
	min_rank = 11

	utility_under= /obj/item/clothing/under/reich/utility/heer/officer/command

	service_under= /obj/item/clothing/under/reich/utility/heer/officer/command
	service_skirt = /obj/item/clothing/under/reich/utility/heer/officer/
	service_hat = /obj/item/clothing/head/soft/reich/cap/officer

	dress_under = /obj/item/clothing/under/reich/utility/heer/officer/command
	dress_skirt = /obj/item/clothing/under/reich/utility/heer/officer/
	dress_over = /obj/item/clothing/suit/dress/reich/expedition/command
	dress_hat = /obj/item/clothing/head/soft/reich/cap/officer
