/decl/hierarchy/mil_uniform
	name = "Master outfit hierarchy"
	hierarchy_type = /decl/hierarchy/mil_uniform
	var/branch = null
	var/departments = 0
	var/min_rank = 0

	var/pt_under = null
	var/pt_shoes = null

	var/utility_under = null
	var/utility_shoes = null
	var/utility_hat = null
	var/utility_extra = null

	var/service_under = null
	var/service_skirt = null
	var/service_over = null
	var/service_shoes = null
	var/service_hat = null
	var/service_gloves = null
	var/service_extra = null

	var/dress_under = null
	var/dress_skirt = null
	var/dress_over = null
	var/dress_shoes = null
	var/dress_hat = null
	var/dress_gloves = null
	var/dress_extra = null

/decl/hierarchy/mil_uniform/mil
	name = "Master Reichsheer outfit"
	hierarchy_type = /decl/hierarchy/mil_uniform/mil
	branch = /datum/mil_branch/heer

	pt_under = /obj/item/clothing/under/reich/pt/heer
	pt_shoes = /obj/item/clothing/shoes/black

	utility_under = /obj/item/clothing/under/reich/utility/heer
	utility_shoes = /obj/item/clothing/shoes/dutyboots
	utility_hat = /obj/item/clothing/head/soft/reich
	utility_extra = list(/obj/item/clothing/head/soft/reich/cap, /obj/item/clothing/head/soft/reich/cap/officer, /obj/item/clothing/suit/storage/hooded/wintercoat/reich, /obj/item/clothing/shoes/jackboots/unathi)

	service_under = /obj/item/clothing/under/reich/utility/heer
	service_skirt = /obj/item/clothing/under/reich/utility/heer_skirt
	service_over = /obj/item/clothing/suit/storage/reich/service/expeditionary
	service_shoes = /obj/item/clothing/shoes/dress
	service_hat = /obj/item/clothing/head/soft/reich/cap

	dress_under = /obj/item/clothing/under/reich/utility/heer
	dress_skirt = /obj/item/clothing/under/reich/utility/heer_skirt
	dress_over = /obj/item/clothing/suit/dress/reich/expedition
	dress_shoes = /obj/item/clothing/shoes/dress
	dress_hat = /obj/item/clothing/head/soft/reich/cap/officer
	dress_gloves = /obj/item/clothing/gloves/white

decl/hierarchy/mil_uniform/civilian
	name = "Master civilian outfit"		//Basically just here for the rent-a-tux, ahem, I mean... dress uniform.
	hierarchy_type = /decl/hierarchy/mil_uniform/civilian
	branch = /datum/mil_branch/civilian

	dress_under = /obj/item/clothing/under/rank/internalaffairs/plain
	dress_over = /obj/item/clothing/suit/storage/toggle/suit/black
	dress_shoes = /obj/item/clothing/shoes/dress
	dress_extra = list(/obj/item/clothing/accessory/wcoat,\
	/obj/item/clothing/under/skirt_c/dress/black, /obj/item/clothing/under/skirt_c/dress/long/black,\
	/obj/item/clothing/under/skirt_c/dress/eggshell, /obj/item/clothing/under/skirt_c/dress/long/eggshell)