
/datum/gear/head/cap
	display_name = "reichsheer cap"
	path = /obj/item/clothing/head/soft/reich
	allowed_roles = SOLGOV_ROLES

/datum/gear/head/surgical
	allowed_roles = STERILE_ROLES

/datum/gear/head/beret
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/mask/bandana
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/head/bandana
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/head/bow
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/head/cap
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/head/hairflower
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/head/hardhat
	allowed_roles = TECHNICAL_ROLES

/datum/gear/head/formalhat
	allowed_roles = FORMAL_ROLES

/datum/gear/head/informalhat
	allowed_roles = SEMIFORMAL_ROLES

/datum/gear/head/welding
	allowed_roles = TECHNICAL_ROLES