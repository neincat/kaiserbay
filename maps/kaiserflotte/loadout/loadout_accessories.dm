/datum/gear/tactical/ubac
	display_name = "ubac selection"
	path = /obj/item/clothing/accessory/ubac
	allowed_roles = MILITARY_ROLES
	flags = GEAR_HAS_TYPE_SELECTION

/datum/gear/accessory/reichawardmajor
	display_name = "reich major award selection"
	description = "A medal or ribbon awarded to Teutonic Empire personnel for significant accomplishments."
	path = /obj/item/clothing/accessory
	cost = 8
	allowed_roles = SOLGOV_ROLES

/datum/gear/accessory/reichawardmajor/New()
	..()
	var/reichmajors = list()
	reichmajors["iron cross"] = /obj/item/clothing/accessory/medal/reich/iron/iron_cross
	reichmajors["silver kaiser medal"] = /obj/item/clothing/accessory/medal/reich/silver/silver_kaiser
	reichmajors["silver sword medal"] = /obj/item/clothing/accessory/medal/reich/silver/silver_sword
	reichmajors["silver cross with oak leaves"] = /obj/item/clothing/accessory/medal/reich/silver/silver_cross
	reichmajors["gold eagle medal"] = /obj/item/clothing/accessory/medal/reich/gold/gold_eagle
	reichmajors["golden cross with swords"] = /obj/item/clothing/accessory/medal/reich/gold/gold_cross
	reichmajors["gold service medal"] = /obj/item/clothing/accessory/medal/reich/gold/gold_star
	reichmajors["peacekeeper ribbon"] = /obj/item/clothing/accessory/ribbon/reich/peace
	reichmajors["marksman ribbon"] = /obj/item/clothing/accessory/ribbon/reich/marksman
	gear_tweaks += new/datum/gear_tweak/path(reichmajors)

/datum/gear/accessory/reichawardminor
	display_name = "reich minor award selection"
	description = "A medal or ribbon awarded to reich personnel for minor accomplishments."
	path = /obj/item/clothing/accessory
	cost = 5
	allowed_roles = SOLGOV_ROLES

/datum/gear/accessory/reichawardminor/New()
	..()
	var/reichminors = list()
	reichminors["oriental cross"] = /obj/item/clothing/accessory/medal/reich/silver/silver_aus_cross
	reichminors["gold oriental cross"] = /obj/item/clothing/accessory/medal/reich/gold/gold_aus_cross
	reichminors["golden order of bavaria"] = /obj/item/clothing/accessory/medal/reich/gold/gold_bav_order
	reichminors["red cross"] = /obj/item/clothing/accessory/medal/reich/red_cross
	reichminors["blue cross"] = /obj/item/clothing/accessory/medal/reich/blue_cross
	reichminors["frontier ribbon"] = /obj/item/clothing/accessory/ribbon/reich/frontier
	reichminors["instructor ribbon"] = /obj/item/clothing/accessory/ribbon/reich/instructor
	gear_tweaks += new/datum/gear_tweak/path(reichminors)

/datum/gear/accessory/tags
	display_name = "dog tags"
	path = /obj/item/clothing/accessory/badge/reich/tags

/datum/gear/accessory/rk_patch
	display_name = "R.K Adler-Aquilla patch"
	path = /obj/item/clothing/accessory/reich/rk_patch

/datum/gear/accessory/army_patch
	display_name = "army patch"
	path = /obj/item/clothing/accessory/reich/army_patch
	flags = GEAR_HAS_TYPE_SELECTION
	allowed_roles = MILITARY_ROLES

/datum/gear/accessory/armband_ma
	display_name = "warden gorget"
	path = /obj/item/clothing/accessory/gorget/reich/warden
	allowed_roles = SECURITY_ROLES

/datum/gear/accessory/armband_security
	display_name = "polizei gorget"
	path = /obj/item/clothing/accessory/gorget/reich/polizei
	allowed_roles = SECURITY_ROLES

/datum/gear/accessory/armband_cargo
	allowed_roles = SUPPLY_ROLES

/datum/gear/accessory/armband_medical
	allowed_roles = MEDICAL_ROLES

/datum/gear/accessory/armband_emt
	allowed_roles = list(/datum/job/doctor)

/datum/gear/accessory/armband_corpsman
	display_name = "medical corps armband"
	path = /obj/item/clothing/accessory/armband/medblue
	allowed_roles = list(/datum/job/cmo, /datum/job/senior_doctor, /datum/job/doctor)

/datum/gear/accessory/armband_engineering
	allowed_roles = ENGINEERING_ROLES

/datum/gear/accessory/armband_hydro
	allowed_roles = list(/datum/job/rd, /datum/job/scientist, /datum/job/scientist_assistant, /datum/job/assistant)

/datum/gear/accessory/tie
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/accessory/tie_color
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/accessory/stethoscope
	allowed_roles = STERILE_ROLES

/datum/gear/storage/brown_vest
	allowed_roles = list(/datum/job/chief_engineer, /datum/job/senior_engineer, /datum/job/engineer, /datum/job/roboticist, /datum/job/qm, /datum/job/cargo_tech,
						/datum/job/mining, /datum/job/janitor, /datum/job/scientist_assistant, /datum/job/merchant)

/datum/gear/storage/black_vest
	allowed_roles = list(/datum/job/hos, /datum/job/warden, /datum/job/detective, /datum/job/officer, /datum/job/merchant)

/datum/gear/storage/white_vest
	allowed_roles = list(/datum/job/cmo, /datum/job/senior_doctor, /datum/job/doctor, /datum/job/roboticist, /datum/job/merchant)

/datum/gear/storage/brown_drop_pouches
	allowed_roles = list(/datum/job/chief_engineer, /datum/job/senior_engineer, /datum/job/engineer, /datum/job/roboticist, /datum/job/qm, /datum/job/cargo_tech,
						/datum/job/mining, /datum/job/janitor, /datum/job/scientist_assistant, /datum/job/merchant)

/datum/gear/storage/black_drop_pouches
	allowed_roles = list(/datum/job/hos, /datum/job/warden, /datum/job/detective, /datum/job/officer, /datum/job/merchant)

/datum/gear/storage/white_drop_pouches
	allowed_roles = list(/datum/job/cmo, /datum/job/senior_doctor, /datum/job/doctor, /datum/job/roboticist, /datum/job/merchant)

/datum/gear/tactical/holster
	allowed_roles = ARMED_ROLES

/datum/gear/tactical/large_pouches
	allowed_roles = ARMORED_ROLES

/datum/gear/tactical/armor_deco
	allowed_roles = ARMORED_ROLES

/datum/gear/tactical/press_tag
	display_name = "Press tag"
	path = /obj/item/clothing/accessory/armor/tag/press
	allowed_roles = list(/datum/job/assistant)

/datum/gear/tactical/helm_covers
	allowed_roles = ARMORED_ROLES

/datum/gear/clothing/hawaii
	allowed_roles = SEMIFORMAL_ROLES

/datum/gear/clothing/scarf
	allowed_roles = SEMIANDFORMAL_ROLES

/datum/gear/clothing/flannel
	allowed_roles = SEMIFORMAL_ROLES

/datum/gear/clothing/vest
	allowed_roles = FORMAL_ROLES

/datum/gear/clothing/suspenders
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/clothing/wcoat
	allowed_roles = FORMAL_ROLES

/datum/gear/clothing/zhongshan
	allowed_roles = FORMAL_ROLES

/datum/gear/clothing/dashiki
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/clothing/thawb
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/clothing/sherwani
	allowed_roles = FORMAL_ROLES

/datum/gear/clothing/qipao
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/clothing/sweater
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/clothing/tangzhuang
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/accessory/bowtie
	allowed_roles = NON_MILITARY_ROLES

/datum/gear/accessory/ftupin
	allowed_roles = list(/datum/job/scientist, /datum/job/mining, /datum/job/scientist_assistant,
						/datum/job/scientist_assistant, /datum/job/roboticist,
						/datum/job/psychiatrist, /datum/job/bartender, /datum/job/merchant, /datum/job/assistant)
