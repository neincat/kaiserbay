/datum/map/kaiserflotte
	branch_types = list(
		/datum/mil_branch/heer,
		/datum/mil_branch/civilian
	)

	spawn_branch_types = list(
		/datum/mil_branch/heer,
		/datum/mil_branch/civilian
	)


/*
 *  Branches
 *  ========
 */

/datum/mil_branch/heer
	name = "Reichsheer"
	name_short = "RH"
	email_domain = "vonbayern.reichsheer.de"

	rank_types = list(
		/datum/mil_rank/heer/e3,
		/datum/mil_rank/heer/e4,
		/datum/mil_rank/heer/e5,
		/datum/mil_rank/heer/e6,
		/datum/mil_rank/heer/e7,
		/datum/mil_rank/heer/e8,
		/datum/mil_rank/heer/e9,
		/datum/mil_rank/heer/w1,
		/datum/mil_rank/heer/w2,
		/datum/mil_rank/heer/w3,
		/datum/mil_rank/heer/w4,
		/datum/mil_rank/heer/w5,
		/datum/mil_rank/heer/o1,
		/datum/mil_rank/heer/o2,
		/datum/mil_rank/heer/o3,
		/datum/mil_rank/heer/o4,
		/datum/mil_rank/heer/o5,
		/datum/mil_rank/heer/o6,
		/datum/mil_rank/heer/o7,
		/datum/mil_rank/heer/o8,
		/datum/mil_rank/heer/o9,
		/datum/mil_rank/heer/o10
	)

	spawn_rank_types = list(
		/datum/mil_rank/heer/e3,
		/datum/mil_rank/heer/e4,
		/datum/mil_rank/heer/e5,
		/datum/mil_rank/heer/e6,
		/datum/mil_rank/heer/e7,
		/datum/mil_rank/heer/e8,
		/datum/mil_rank/heer/e9,
		/datum/mil_rank/heer/o1,
		/datum/mil_rank/heer/o2,
		/datum/mil_rank/heer/o3,
		/datum/mil_rank/heer/o4,
		/datum/mil_rank/heer/o5,
		/datum/mil_rank/heer/o6,
		/datum/mil_rank/heer/o7
	)

	assistant_job = "Colonist"
	min_skill = list(	SKILL_HAULING = SKILL_BASIC,
						SKILL_WEAPONS = SKILL_BASIC,
						SKILL_EVA     = SKILL_BASIC)

/datum/mil_branch/civilian
	name = "Civilian"
	name_short = "civ"
	email_domain = "postamt.de"

	rank_types = list(
		/datum/mil_rank/civ/civ,
		/datum/mil_rank/civ/official,
		/datum/mil_rank/civ/contractor,
		/datum/mil_rank/civ/offduty,
		/datum/mil_rank/civ/verdeckte,
		/datum/mil_rank/civ/synthetic
	)

	spawn_rank_types = list(
		/datum/mil_rank/civ/civ,
		/datum/mil_rank/civ/official,
		/datum/mil_rank/civ/contractor,
		/datum/mil_rank/civ/offduty,
		/datum/mil_rank/civ/verdeckte,
		/datum/mil_rank/civ/synthetic
	)

	assistant_job = "Colonist"

/datum/mil_rank/grade()
	. = ..()
	if(!sort_order)
		return ""
	if(sort_order <= 10)
		return "E[sort_order]"
	return "O[sort_order - 10]"


/*
 *  REICHSHEER
 *  =====
 */

/datum/mil_rank/heer/e3
	name = "Schutze"
	name_short = "S"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/enlisted, /obj/item/clothing/accessory/reich/specialty/enlisted)
	sort_order = 3

/datum/mil_rank/heer/e4
	name = "Oberschutze"
	name_short = "OS"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/enlisted/e4, /obj/item/clothing/accessory/reich/specialty/enlisted)
	sort_order = 4

/datum/mil_rank/heer/e5
	name = "Gefreiter"
	name_short = "G"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/enlisted/e5, /obj/item/clothing/accessory/reich/specialty/enlisted)
	sort_order = 5

/datum/mil_rank/heer/e6
	name = "Obergefreiter"
	name_short = "OG"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/enlisted/e6, /obj/item/clothing/accessory/reich/specialty/enlisted)
	sort_order = 6

/datum/mil_rank/heer/e7
	name = "Hauptgefreiter"
	name_short = "HG"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/enlisted/e7, /obj/item/clothing/accessory/reich/specialty/enlisted)
	sort_order = 7

/datum/mil_rank/heer/e8
	name = "Stabsgefreiter"
	name_short = "SG"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/enlisted/e8, /obj/item/clothing/accessory/reich/specialty/enlisted)
	sort_order = 8

/datum/mil_rank/heer/e9
	name = "Unteroffizier"
	name_short = "UO"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/enlisted/e9, /obj/item/clothing/accessory/reich/specialty/enlisted)
	sort_order = 9

/datum/mil_rank/heer/w1
	name = "Fahnenjunker"
	name_short = "FJ"
	sort_order = -1

/datum/mil_rank/heer/w2
	name = "Fahnenjunkerunteroffizier"
	name_short = "FJUO"
	sort_order = -2

/datum/mil_rank/heer/w3
	name = "Fahnrich"
	name_short = "OFR"
	sort_order = -3

/datum/mil_rank/heer/w4
	name = "Oberfahnrich"
	name_short = "HFR"
	sort_order = -4

/datum/mil_rank/heer/w5
	name = "Feldwebel"
	name_short = "FW"
	sort_order = -5

/datum/mil_rank/heer/o1
	name = "Leutnant"
	name_short = "OFW"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/officer, /obj/item/clothing/accessory/reich/specialty/officer)
	sort_order = 11

/datum/mil_rank/heer/o2
	name = "Oberleutnant"
	name_short = "OLT"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/officer/o2, /obj/item/clothing/accessory/reich/specialty/officer)
	sort_order = 12

/datum/mil_rank/heer/o3
	name = "Hauptmann"
	name_short = "HM"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/officer/o3, /obj/item/clothing/accessory/reich/specialty/officer)
	sort_order = 13

/datum/mil_rank/heer/o4
	name = "Major"
	name_short = "M"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/officer/o4, /obj/item/clothing/accessory/reich/specialty/officer)
	sort_order = 14

/datum/mil_rank/heer/o5
	name = "Oberst"
	name_short = "O"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/officer/o5, /obj/item/clothing/accessory/reich/specialty/officer)
	sort_order = 15

/datum/mil_rank/heer/o6
	name = "Generalmajor"
	name_short = "GMA"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/officer/o6, /obj/item/clothing/accessory/reich/specialty/officer)
	sort_order = 16

/datum/mil_rank/heer/o7
	name = "Generalleutnant"
	name_short = "GLT"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/flag, /obj/item/clothing/accessory/reich/specialty/officer)
	sort_order = 17

/datum/mil_rank/heer/o8
	name = "General der Infanterie"
	name_short = "GDI"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/flag/o8, /obj/item/clothing/accessory/reich/specialty/officer)
	sort_order = 18

/datum/mil_rank/heer/o9
	name = "Generaloberst"
	name_short = "GOBT"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/flag/o9, /obj/item/clothing/accessory/reich/specialty/officer)
	sort_order = 19

/datum/mil_rank/heer/o10
	name = "Generalfeldmarschall"
	name_short = "GFM"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/flag/o10, /obj/item/clothing/accessory/reich/specialty/officer)
	sort_order = 20

/datum/mil_rank/heer/o11
	name = "Reichsmarschall"
	name_short = "RM"
	accessory = list(/obj/item/clothing/accessory/reich/rank/heer/flag/o10_alt, /obj/item/clothing/accessory/reich/specialty/officer)
	sort_order = 20



/*
 *  Civilians
 *  =========
 */

/datum/mil_rank/civ/civ
	name = "Civilian"
	name_short = null

/datum/mil_rank/civ/official
	name = "Official"

/datum/mil_rank/civ/contractor
	name = "Contractor"

/datum/mil_rank/civ/offduty
	name = "Off-Duty Personnel"

/datum/mil_rank/civ/verdeckte
	name = "Verdecktepolizei"
	name_short = "VPol"
	accessory = list(/obj/item/clothing/accessory/badge/marshal)

/datum/mil_rank/civ/synthetic
	name = "Synthetic"
