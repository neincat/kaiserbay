/obj/structure/sign/dedicationplaque
	name = "\improper SEV kaiserflotte dedication plaque"
	desc = "S.E.V. kaiserflotte - Mako Class - Sol Expeditionary Corps Registry 95519 - Shiva Fleet Yards, Mars - First Vessel To Bear The Name - Launched 2560 - Sol Central Government - 'Never was anything great achieved without danger.'"
	icon = 'maps/kaiserflotte/icons/obj/reich-decals.dmi'
	icon_state = "lightplaque"

/obj/structure/sign/ecplaque
	name = "Kaiserreich Hymn"
	desc = "A plaque with Teutonic flag etched in it."
	icon = 'maps/kaiserflotte/icons/obj/reich-decals.dmi'
	icon_state = "reichplaque"
	var/hymn = {"<hr><center>
		<b>Es braust ein Ruf wie Donnerhall,</b><br>

		<b>wie Schwertgeklirr und Wogenprall:</b><br>

		<b>Zum Raum, zum Raum, zum teutonische Raum</b><br>

		<b>wer will des Stromes Huter sein?</b><br>

		<b>Lieb' Vaterland, magst ruhig sein,</b><br>

		<b>lieb' Vaterland, magst ruhig sein,</b><br>

		<b>fest steht und treu die Wacht, die Wacht am Raum!</b><br>

		<b>Fest steht und treu die Wacht, die Wacht am Raum!</b><br>

		<b>Durch Hunderttausend zuckt es schnell,</b><br>

		<b>und aller Augen blitzen hell;</b><br>

		<b>der Teutonische bieder, fromm und stark,</b><br>

		<b>besch�tzt die heil�ge Landesmark.</b><br>

		<b>Er blickt hinauf in Himmelsau�n,</b><br>

		<b>wo Heldenv�ter niederschau�n,</b><br>

		<b>und schw�rt mit stolzer Kampfeslust:</b><br>

		<b>Du Raum bleibst teutonische wie meine Brust!</b><br>

		<b>Solang ein Tropfen Blut noch gluht,</b><br>

		<b>noch eine Faust den Degen zieht,</b><br>

		<b>und noch ein Arm die Buchse spannt,</b><br>

		<b>betritt kein Feind hier deinen Strand!</b><br>

		<b>So fuhre uns, du bist bew�hrt;</b><br>

		<b>In Gottvertrau�n greif' zu dem Schwert!</b><br>

		<b>Hoch Wilhelm! Nieder mit der Brut!</b><br>

		<b>Und tilg' die Schmach mit Feindesblut!</b><br>

		</center><hr>"}

/obj/structure/sign/ecplaque/examine()
	..()
	to_chat(usr, "It's a hymn of Teutonic Empire: <A href='?src=\ref[src];show_info=1'>Teutonic Empire Hymn</A>")

/obj/structure/sign/ecplaque/CanUseTopic()
	return STATUS_INTERACTIVE

/obj/structure/sign/ecplaque/OnTopic(href, href_list)
	if(href_list["show_info"])
		to_chat(usr, hymn)
		return TOPIC_HANDLED

/obj/structure/sign/ecplaque/attackby(var/obj/I, var/mob/user)
	if(istype(I, /obj/item/grab))
		var/obj/item/grab/G = I
		if(!ishuman(G.affecting))
			return
		G.affecting.apply_damage(5, BRUTE, BP_HEAD, used_weapon="Metal Plaque")
		visible_message("<span class='warning'>[G.assailant] smashes [G.assailant] into \the [src] face-first!</span>")
		playsound(get_turf(src), 'sound/weapons/tablehit1.ogg', 50)
		to_chat(G.affecting, "<span class='danger'>[hymn]</span>")
		admin_attack_log(user, G.affecting, "educated victim on \the [src].", "Was educated on \the [src].", "used \a [src] to educate")
		G.force_drop()
	else
		..()

/obj/effect/floor_decal/scglogo
	alpha = 230
	icon = 'maps/kaiserflotte/icons/obj/reich_floor.dmi'
	icon_state = "1,1"

/obj/structure/sign/reichcross
	name = "\improper Teutonic Empire Seal"
	desc = "A sign which signifies who this vessel belongs to."
	icon = 'maps/kaiserflotte/icons/obj/reich-decals.dmi'
	icon_state = "reichseal"

/obj/structure/sign/double/teutonicflag
	name = "Teutonic Empire Flag"
	desc = "The flag of the Teutonic Empire, a symbol of many things to many people."
	icon = 'maps/kaiserflotte/icons/obj/reich-decals.dmi'

/obj/structure/sign/double/teutonicflag/left
	icon_state = "kaiserflag-left"

/obj/structure/sign/double/teutonicflag/right
	icon_state = "kaiserflag-right"

/obj/structure/sign/double/rkflag
	name = "Reichkomissariat Adler-Aquilla Flag"
	desc = "The flag of the R.K. Adler-Aquilla."
	icon = 'maps/kaiserflotte/icons/obj/reich-decals.dmi'

/obj/structure/sign/double/rkflag/left
	icon_state = "rkflag-left"

/obj/structure/sign/double/rkflag/right
	icon_state = "rkflag-right"

/obj/structure/sign/shieldreich
	name = "\improper Kaiser Shield"
	desc = "Shield with coat of arms of the Teutonic Empire."
	icon = 'maps/kaiserflotte/icons/obj/reich-decals.dmi'
	icon_state = "shieldreich"

/obj/structure/sign/shieldrk
	name = "\improper R.K. Shield"
	desc = "Shield with coat of arms of the R.K. Adler-Aquilla."
	icon = 'maps/kaiserflotte/icons/obj/reich-decals.dmi'
	icon_state = "shieldrk"
