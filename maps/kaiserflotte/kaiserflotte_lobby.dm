/datum/map/kaiserflotte
	lobby_icon = 'maps/kaiserflotte/icons/lobby.dmi'
	lobby_screens = list("title")
	lobby_tracks = list(
		/music_track/fatalist,
		/music_track/redemption,
		/music_track/evilincarnate,
		/music_track/dasboot,
		/music_track/systemcontrol,
		/music_track/containment
		)
