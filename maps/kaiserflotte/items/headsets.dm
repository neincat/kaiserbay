/obj/item/device/radio/headset/kaiserflotte/nanotrasen
	name = "nanotrasen headset"
	desc = "A headset for corporate drones."
	icon_state = "nt_headset"
	item_state = "headset"
	ks1type = /obj/item/device/encryptionkey/headset_kaiserflotte/nt

/obj/item/device/radio/headset/heads/kaiserflotte/captain
	name = "commanding officer's headset"
	desc = "The skipper's headset."
	icon_state = "com_headset"
	item_state = "headset"
	ks1type = /obj/item/device/encryptionkey/heads/kaiserflotte/captain

/obj/item/device/radio/headset/heads/kaiserflotte/xo
	name = "executive officer's headset"
	desc = "The headset of the guy who will one day be CO."
	icon_state = "com_headset"
	item_state = "headset"
	ks1type = /obj/item/device/encryptionkey/heads/kaiserflotte/xo

/obj/item/device/radio/headset/heads/kaiserflotte/ntcommand
	name = "nanotrasen command headset"
	desc = "Headset of the corporate overlords."
	icon_state = "nt_headset"
	item_state = "headset"
	ks1type = /obj/item/device/encryptionkey/headset_kaiserflotte/rd

/obj/item/device/radio/headset/heads/cos
	name = "chief of security's headset"
	desc = "The headset of the man who protects your worthless lives."
	icon_state = "com_headset"
	item_state = "headset"
	ks1type = /obj/item/device/encryptionkey/heads/hos

/obj/item/device/radio/headset/headset_deckofficer
	name = "deck officer's radio headset"
	desc = "The headset of the chief box pusher."
	icon_state = "cargo_headset"
	item_state = "headset"
	ks1type = /obj/item/device/encryptionkey/headset_deckofficer

/obj/item/weapon/storage/box/headset/kaiserflotte/xo
	name = "box of spare executive officer headsets"
	desc = "A box full of executive officer headsets."
	startswith = list(/obj/item/device/radio/headset/heads/kaiserflotte/xo = 7)

/obj/item/device/radio/headset/bridgeofficer
	name = "bridge officer's headset"
	desc = "A headset with access to the command, engineering and exploration channels."
	icon_state = "com_headset"
	item_state = "headset"
	ks1type = /obj/item/device/encryptionkey/bridgeofficer

/obj/item/device/radio/headset/exploration
	name = "exploration headset"
	desc = "A headset for real tools, with access to the exploration channel."
	icon_state = "srv_headset"
	item_state = "headset"
	ks1type = /obj/item/device/encryptionkey/exploration

/obj/item/device/radio/headset/pathfinder
	name = "pathfinder's headset"
	desc = "A headset with access to the command and exploration channels."
	icon_state = "com_headset"
	item_state = "headset"
	ks1type = /obj/item/device/encryptionkey/pathfinder

/obj/item/device/radio/headset/kaiserflotte/roboticist
	name = "roboticist's headset"
	desc = "A headset with access to the engineering and medical channels."
	icon_state = "eng_headset"
	item_state = "headset"
	ks1type = /obj/item/device/encryptionkey/headset_kaiserflotte/roboticist
