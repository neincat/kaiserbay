/obj/item/clothing/under/reich
	name = "master reich uniform"
	desc = "You shouldn't be seeing this."
	icon = 'maps/kaiserflotte/icons/obj/reich-under.dmi'
	item_icons = list(slot_w_uniform_str = 'maps/kaiserflotte/icons/mob/reich-under.dmi')
	armor = list(melee = 5, bullet = 0, laser = 5, energy = 5, bomb = 0, bio = 5, rad = 5)
	siemens_coefficient = 0.8

//PT
/obj/item/clothing/under/reich/pt
	name = "pt uniform"
	desc = "Shorts! Shirt! Miami! Sexy!"
	icon_state = "miami"
	worn_state = "miami"
	body_parts_covered = UPPER_TORSO|LOWER_TORSO

/obj/item/clothing/under/reich/pt/heer
	name = "reich training uniform"
	desc = "Does NOT leave much to the imagination."
	icon_state = "reichpt"
	worn_state = "reichpt"


//Utility

/obj/item/clothing/under/reich/utility
	name = "reichsheer civilian uniform"
	desc = "Comfortable uniforms of the Teutonic army without soldiers' buckles."
	icon_state = "reichutility"
	item_state = "reichutility"
	worn_state = "reichutility"

/obj/item/clothing/under/reich/utility/heer
	name = "reichsheer uniform"
	desc = "Uniform Teutonic Army made of durable materials. With silver buckle."
	icon_state = "reichutility_crew"
	worn_state = "reichutility_crew"

/obj/item/clothing/under/reich/utility/heer_skirt
	name = "heer skirt"
	desc = "Uniform Teutonic Army made of durable materials. For military ladies."
	icon_state = "reichservicef"
	worn_state = "reichservicef"

/obj/item/clothing/under/reich/utility/heer_skirt/officer
	name = "heer officer skirt"
	desc = "Gray officer uniform of the Teutonic Empire. Especially for female officers."
	icon_state = "reichservicef_com"
	worn_state = "reichservicef_com"

/obj/item/clothing/under/reich/utility/heer/command
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/command)

/obj/item/clothing/under/reich/utility/heer/engineering
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/engineering)

/obj/item/clothing/under/reich/utility/heer/security
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/security, /obj/item/clothing/accessory/gorget/reich/polizei)

/obj/item/clothing/under/reich/utility/heer/medical
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/medical)

/obj/item/clothing/under/reich/utility/heer/supply
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/supply)

/obj/item/clothing/under/reich/utility/heer/service
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/service)

/obj/item/clothing/under/reich/utility/heer/exploration
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/exploration)

/obj/item/clothing/under/reich/utility/heer/officer
	name = "heer officer's uniform"
	desc = "Gray officer uniform of the Teutonic Empire. On the buckle it says ''Gott mit Uns''."
	icon_state = "reichutility_com"
	worn_state = "reichutility_com"

/obj/item/clothing/under/reich/utility/heer/officer/command
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/command)

/obj/item/clothing/under/reich/utility/heer/officer/engineering
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/engineering)

/obj/item/clothing/under/reich/utility/heer/officer/security
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/security)

/obj/item/clothing/under/reich/utility/heer/officer/medical
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/medical)

/obj/item/clothing/under/reich/utility/heer/officer/supply
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/supply)

/obj/item/clothing/under/reich/utility/heer/officer/service
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/service)

/obj/item/clothing/under/reich/utility/heer/officer/exploration
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/exploration)


/obj/item/clothing/under/reich/utility/heer/obercommando
	name = "heer black uniform"
	desc = "Black officer uniform of the Teutonic Empire. On the buckle it says ''Gott mit Uns''."
	icon_state = "reichuniform_ober"
	worn_state = "reichuniform_ober"


/obj/item/clothing/under/reich/utility/heer/obercommando/command
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/command)

/obj/item/clothing/under/reich/utility/heer/obercommando/engineering
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/engineering)

/obj/item/clothing/under/reich/utility/heer/obercommando/security
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/security)

/obj/item/clothing/under/reich/utility/heer/obercommando/medical
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/medical)

/obj/item/clothing/under/reich/utility/heer/obercommando/supply
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/supply)

/obj/item/clothing/under/reich/utility/heer/obercommando/service
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/service)

/obj/item/clothing/under/reich/utility/heer/obercommando/exploration
	starting_accessories = list(/obj/item/clothing/accessory/reich/department/exploration)

/obj/item/clothing/under/reich/utility/heer/reichkomissar
	name = "reichskomissar uniform"
	desc = "Beautiful uniform made to order for Reichskommissar Adler-Aquila."
	icon_state = "rkuniform"
	worn_state = "rkuniform"

/obj/item/clothing/under/reich/utility/heer/comissar
	name = "heer comissar uniform"
	desc = "Feldgrau officer uniform of the Teutonic Empire. On the buckle it says ''Gott mit Uns''."
	icon_state = "reichutility_com"
	worn_state = "reichutility_com"

//Service

/obj/item/clothing/under/reich/utility/heer/monkey
	name = "adjusted heer uniform"
	desc = "The utility uniform of the SCG heer Corps, made from biohazard resistant material. This one has silver trim. It was also mangled to fit a monkey. This better be worth the NJP you'll get for making it."
	species_restricted = list("Monkey")
	sprite_sheets = list("Monkey" = 'icons/mob/species/monkey/uniform.dmi')
	starting_accessories = list(/obj/item/clothing/accessory/reich/rank/fleet/officer/wo1_monkey)
