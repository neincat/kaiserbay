/obj/item/clothing/accessory/reich
	name = "master kaiserreich accessory"
	icon = 'maps/kaiserflotte/icons/obj/reich-accessory.dmi'
	accessory_icons = list(slot_w_uniform_str = 'maps/kaiserflotte/icons/mob/reich-accessory.dmi', slot_wear_suit_str = 'maps/kaiserflotte/icons/mob/reich-accessory.dmi')

/*****
medals
*****/
/obj/item/clothing/accessory/medal/reich
	name = "master kaiserreich medal"
	desc = "You shouldn't be seeing this."
	icon = 'maps/kaiserflotte/icons/obj/reich-accessory.dmi'
	accessory_icons = list(slot_w_uniform_str = 'maps/kaiserflotte/icons/mob/reich-accessory.dmi', slot_wear_suit_str = 'maps/kaiserflotte/icons/mob/reich-accessory.dmi')

/obj/item/clothing/accessory/medal/reich/iron/iron_cross
	name = "iron cross"
	desc = "The Iron Cross is awarded to the citizens of the Teutonic Empire for bravery on the battlefield."
	icon_state = "iron_cross"

/obj/item/clothing/accessory/medal/reich/silver/silver_kaiser
	name = "silver kaiser medal"
	desc = "Medal with the image of the Kaiser Teutonic Empire. Awarded for hard work for the good of the Empire."
	icon_state = "silver_kaiser"

/obj/item/clothing/accessory/medal/reich/silver/silver_sword
	name = "silver sword medal"
	desc = "Silver sword awarded officers and soldiers of the Teutonic Empire for good service on the borders of the Empire."
	icon_state = "silver_sword"

/obj/item/clothing/accessory/medal/reich/silver/silver_cross
	name = "silver cross with oak leaves"
	desc = "Silver cross with oak leaves awarded for performing particularly difficult tasks and showing courage in battle."
	icon_state = "silver_cross"

/obj/item/clothing/accessory/medal/reich/gold/gold_eagle
	name = "gold eagle medal"
	desc = "Gold Eagle Medal awarded for special services to the Teutonic Empire."
	icon_state = "gold_eagle"

/obj/item/clothing/accessory/medal/reich/silver/silver_aus_cross
	name = "oriental cross"
	desc = "The Oriental Cross is awarded to non-Teutons who served the Teutonic Empire."
	icon_state = "silver_aus_cross"

/obj/item/clothing/accessory/medal/reich/gold/gold_cross
	name = "golden cross with swords"
	desc = "A gold cross awarded to officers and soldiers of the Teutonic Empire for acts of heroism in a combat zone."
	icon_state = "gold_cross"

/obj/item/clothing/accessory/medal/reich/gold/gold_star
	name = "gold service medal"
	desc = "A gold medal awarded to citizens of the Teutonic Empire by the Kaiser for significant contributions to the Teutonic Empire."
	icon_state = "gold_star"

/obj/item/clothing/accessory/medal/reich/gold/gold_aus_cross
	name = "gold oriental cross"
	desc = "The Golden East Cross is awarded to foreigners who have shown courage and heroism for the Teutonic Empire."
	icon_state = "gold_aus_cross"

/obj/item/clothing/accessory/medal/reich/gold/gold_bav_order
	name = "golden order of bavaria"
	desc = "The Golden Bavarian Order is awarded for many years of hard work in the colonies of the Teutonic Empire."
	icon_state = "gold_bav_order"

/obj/item/clothing/accessory/medal/reich/red_cross
	name = "red cross"
	desc = "A red cross awarded to citizens of the Teutonic Empire for service as a medical professional in a combat zone."
	icon_state = "red_cross"

/obj/item/clothing/accessory/medal/reich/blue_cross
	name = "blue cross"
	desc = "A blue cross awarded to citizens of the Teutonic Empire for service as a engineer professional in a combat zone."
	icon_state = "blue_cross"

/obj/item/clothing/accessory/reich/rk_patch
	name = "r.k. adler-aquila colonial patch"
	desc = "A fire resistant shoulder patch, worn by the personnel involved in the R.K. Adler-Aquilla."
	icon_state = "rkpatch"
	slot = ACCESSORY_SLOT_INSIGNIA

/obj/item/clothing/accessory/reich/army_patch
	name = "first field army patch"
	desc = "A fancy shoulder patch carrying insignia of First Field Army, the Neu-Berlin Guard, stationed in Neu-Berlin."
	icon_state = "divisionpatch1"
	slot = ACCESSORY_SLOT_INSIGNIA

/obj/item/clothing/accessory/reich/army_patch/second
	name = "second field army patch"
	desc = "A well-worn shoulder patch carrying insignia of Second Field Army, defending the core worlds of the Teutonic Empire."
	icon_state = "divisionpatch2"

/obj/item/clothing/accessory/reich/army_patch/third
	name = "third field army patch"
	desc = "A scuffed shoulder patch carrying insignia of Third Field Army, the Border Guard, guarding borders of the Teutonic Empire territory against enemies, pirates, smugglers."
	icon_state = "divisionpatch3"

/obj/item/clothing/accessory/reich/army_patch/fourth
	name = "fourth field army patch"
	desc = "A pristine shoulder patch carrying insignia of Fourth Field Army, colonial guard of the empire."
	icon_state = "divisionpatch4"

/obj/item/clothing/accessory/reich/army_patch/fifth
	name = "fifth field army patch"
	desc = "A tactical shoulder patch carrying insignia of Fifth Field Army, the special forces of the Teutonic Empire, recently formed and outfited with last tech."
	icon_state = "divisionpatch5"

/******
ribbons
******/
/obj/item/clothing/accessory/ribbon/reich
	name = "ribbon"
	desc = "A simple military decoration."
	icon_state = "ribbon_marksman"
	slot = ACCESSORY_SLOT_MEDAL
	icon = 'maps/kaiserflotte/icons/obj/reich-accessory.dmi'
	accessory_icons = list(slot_w_uniform_str = 'maps/kaiserflotte/icons/mob/reich-accessory.dmi', slot_wear_suit_str = 'maps/kaiserflotte/icons/mob/reich-accessory.dmi')

/obj/item/clothing/accessory/ribbon/reich/marksman
	name = "marksmanship ribbon"
	desc = "A military decoration awarded to members of the Teutonic Empire  for good marksmanship scores in training. Common in the days of energy or firearms weapon."
	icon_state = "ribbon_marksman"

/obj/item/clothing/accessory/ribbon/reich/peace
	name = "peacekeeping ribbon"
	desc = "A military decoration awarded to members of the Teutonic Empire for service during a suppression of uprisings and peacekeeping operations."
	icon_state = "ribbon_peace"

/obj/item/clothing/accessory/ribbon/reich/frontier
	name = "frontier ribbon"
	desc = "A military decoration awarded to members of the Teutonic Empire for service along the frontier."
	icon_state = "ribbon_frontier"

/obj/item/clothing/accessory/ribbon/reich/instructor
	name = "instructor ribbon"
	desc = "A military decoration awarded to members of the Teutonic Empire for service as an instructor."
	icon_state = "ribbon_instructor"

/*************
specialty pins
*************/
/obj/item/clothing/accessory/reich/specialty
	name = "speciality blaze"
	desc = "A color blaze denoting fleet personnel in some special role. This one is silver."
	icon_state = "heerrank_command"
	slot = ACCESSORY_SLOT_INSIGNIA

/obj/item/clothing/accessory/reich/specialty/get_fibers()
	return null

/obj/item/clothing/accessory/reich/specialty/janitor
	name = "custodial blazes"
	desc = "Purple blazes denoting a custodial technician."
	icon_state = "fleetspec_janitor"

/obj/item/clothing/accessory/reich/specialty/brig
	name = "brig blazes"
	desc = "Red blazes denoting a brig officer."
	icon_state = "fleetspec_brig"

/obj/item/clothing/accessory/reich/specialty/forensic
	name = "forensics blazes"
	desc = "Steel blazes denoting a forensic technician."
	icon_state = "fleetspec_forensic"

/obj/item/clothing/accessory/reich/specialty/atmos
	name = "atmospherics blazes"
	desc = "Turquoise blazes denoting an atmospheric technician."
	icon_state = "fleetspec_atmos"

/obj/item/clothing/accessory/reich/specialty/counselor
	name = "counselor blazes"
	desc = "Blue blazes denoting a counselor."
	icon_state = "fleetspec_counselor"

/obj/item/clothing/accessory/reich/specialty/chemist
	name = "chemistry blazes"
	desc = "Orange blazes denoting a chemist."
	icon_state = "fleetspec_chemist"

/obj/item/clothing/accessory/reich/specialty/enlisted
	name = "enlisted qualification pin"
	desc = "An iron pin denoting some special qualification."
	icon_state = "fleetpin_enlisted"

/obj/item/clothing/accessory/reich/specialty/officer
	name = "officer's qualification pin"
	desc = "A golden pin denoting some special qualification."
	icon_state = "fleetpin_officer"

/obj/item/clothing/accessory/reich/speciality/pilot
	name = "pilot's qualification pin"
	desc = "An iron pin denoting the qualification to fly in the SGDF."
	icon_state = "pin_pilot"

/*****
badges
*****/
/obj/item/clothing/accessory/badge/reich
	name = "master reich badge"
	icon = 'maps/kaiserflotte/icons/obj/reich-accessory.dmi'
	accessory_icons = list(slot_w_uniform_str = 'maps/kaiserflotte/icons/mob/reich-accessory.dmi', slot_wear_suit_str = 'maps/kaiserflotte/icons/mob/reich-accessory.dmi')

/obj/item/clothing/accessory/badge/reich/security
	name = "security forces badge"
	desc = "A silver law enforcement badge. Stamped with the words 'Polizie'."
	icon_state = "silverbadge"
	slot_flags = SLOT_TIE
	badge_string = "Teutonic Empire"

/obj/item/clothing/accessory/badge/reich/tags
	name = "dog tags"
	desc = "Plain identification tags made from a durable metal. They are stamped with a variety of informational details."
	gender = PLURAL
	icon_state = "tags"
	badge_string = "Teutonic Empire"
	slot_flags = SLOT_MASK | SLOT_TIE

/obj/item/clothing/accessory/badge/reich/tags/Initialize()
	. = ..()
	var/mob/living/carbon/human/H
	H = get_holder_of_type(src, /mob/living/carbon/human)
	if(H)
		set_name(H.real_name)
		set_desc(H)

/obj/item/clothing/accessory/badge/solgov/tags/set_desc(var/mob/living/carbon/human/H)
	if(!istype(H))
		return
	var/decl/cultural_info/culture = H.get_cultural_value(TAG_RELIGION)
	var/religion = culture ? culture.name : "Unset"
	desc = "[initial(desc)]\nName: [H.real_name] ([H.get_species()])[H.char_branch ? "\nBranch: [H.char_branch.name]" : ""]\nReligion: [religion]\nBlood type: [H.b_type]"

/obj/item/clothing/accessory/badge/reich/representative
	name = "representative's badge"
	desc = "A leather-backed plastic badge with a variety of information printed on it. Belongs to a Verdeckte Polizei Officer."
	icon_state = "reichbadge"
	slot_flags = SLOT_TIE
	badge_string = "Teutonic Empire"

/*******
armbands
*******/
/obj/item/clothing/accessory/gorget/reich
	name = "master reich gorget"
	icon = 'maps/kaiserflotte/icons/obj/reich-accessory.dmi'
	slot_flags = SLOT_TIE
	accessory_icons = list(slot_w_uniform_str = 'maps/kaiserflotte/icons/mob/reich-accessory.dmi', slot_wear_suit_str = 'maps/kaiserflotte/icons/mob/reich-accessory.dmi')

/obj/item/clothing/accessory/gorget/reich/polizei
	name = "feldgendarmerie gorget"
	desc = "An gorget to show whose chain dog is this man. This steel gorget depicts an eagle and is worn by representatives of the Military Police."
	icon_state = "mpband"

/obj/item/clothing/accessory/gorget/reich/warden
	name = "warden gorget"
	desc = "An gorget to show whose chain dog is this man. This bronze gorget depicts an eagle and is worn by the Warden."
	icon_state = "maband"

/obj/item/weapon/storage/box/gorget
	name = "box of spare feldgendarmerie gorgets"
	desc = "A box full of security armbands. For use in emergencies when provisional security personnel are needed."
	startswith = list(/obj/item/clothing/accessory/gorget/reich/polizei = 5)

/*****************
armour attachments
*****************/
/obj/item/clothing/accessory/armor/tag/reich
	name = "teutonic empire flag"
	desc = "An emblem depicting the Teutonic Empire flag."
	icon_override = 'maps/kaiserflotte/icons/obj/reich-accessory.dmi'
	icon = 'maps/kaiserflotte/icons/obj/reich-accessory.dmi'
	accessory_icons = list(slot_tie_str = 'maps/kaiserflotte/icons/mob/reich-accessory.dmi', slot_w_uniform_str = 'maps/kaiserflotte/icons/mob/reich-accessory.dmi', slot_wear_suit_str = 'maps/kaiserflotte/icons/mob/reich-accessory.dmi')
	icon_state = "solflag"
	slot = ACCESSORY_SLOT_ARMOR_M

/obj/item/clothing/accessory/armor/tag/reich/ec
	name = "rk adler-aquilla flag"
	desc = "An emblem depicting the R.K. Adler-Aquilla Flag."
	icon_state = "ecflag"

/obj/item/clothing/accessory/armor/tag/reich/sec
	name = "polizei tag"
	desc = "An armor tag with the word POLIZEI printed in silver lettering on it."
	icon_state = "sectag"

/obj/item/clothing/accessory/armor/tag/reich/com
	name = "teutonic empire tag"
	desc = "An armor tag with the words TEUTONIC EMPIRE printed in gold lettering on it."
	icon_state = "comtag"

/obj/item/clothing/accessory/armor/tag/reich/com/sec
	name = "\improper POLICE tag"
	desc = "An armor tag with the words POLICE printed in gold lettering on it."

/**************
department tags
**************/
/obj/item/clothing/accessory/reich/department
	name = "department insignia"
	desc = "Insignia denoting assignment to a department. These appear blank."
	icon_state = "dept_exped"
	on_rolled = list("down" = "none", "rolled" = "dept_exped_sleeves")
	slot = ACCESSORY_SLOT_DEPT
	removable = FALSE

/obj/item/clothing/accessory/reich/department/command
	name = "command insignia"
	desc = "Insignia denoting assignment to the command department. These fit Expeditionary Corps uniforms."
	color = "#e5ea4f"

/obj/item/clothing/accessory/reich/department/command/service
	icon_state = "dept_exped_service"

/obj/item/clothing/accessory/reich/department/command/fleet
	icon_state = "dept_fleet"
	desc = "Insignia denoting assignment to the command department. These fit Fleet uniforms."
	on_rolled = list("rolled" = "dept_fleet_sleeves", "down" = "none")

/obj/item/clothing/accessory/reich/department/command/marine
	icon_state = "dept_marine"
	desc = "Insignia denoting assignment to the command department. These fit Marine Corps uniforms."
	on_rolled = list("down" = "none")

/obj/item/clothing/accessory/reich/department/engineering
	name = "engineering insignia"
	desc = "Insignia denoting assignment to the engineering department. These fit Expeditionary Corps uniforms."
	color = "#ff7f00"

/obj/item/clothing/accessory/reich/department/engineering/service
	icon_state = "dept_exped_service"

/obj/item/clothing/accessory/reich/department/engineering/fleet
	icon_state = "dept_fleet"
	desc = "Insignia denoting assignment to the engineering department. These fit Fleet uniforms."
	on_rolled = list("rolled" = "dept_fleet_sleeves", "down" = "none")

/obj/item/clothing/accessory/reich/department/engineering/marine
	icon_state = "dept_marine"
	desc = "Insignia denoting assignment to the engineering department. These fit Marine Corps uniforms."
	on_rolled = list("down" = "none")

/obj/item/clothing/accessory/reich/department/security
	name = "security insignia"
	desc = "Insignia denoting assignment to the security department. These fit Expeditionary Corps uniforms."
	color = "#bf0000"

/obj/item/clothing/accessory/reich/department/security/service
	icon_state = "dept_exped_service"

/obj/item/clothing/accessory/reich/department/security/fleet
	icon_state = "dept_fleet"
	desc = "Insignia denoting assignment to the security department. These fit Fleet uniforms."
	on_rolled = list("rolled" = "dept_fleet_sleeves", "down" = "none")

/obj/item/clothing/accessory/reich/department/security/marine
	icon_state = "dept_marine"
	desc = "Insignia denoting assignment to the security department. These fit Marine Corps uniforms."
	on_rolled = list("down" = "none")

/obj/item/clothing/accessory/reich/department/medical
	name = "medical insignia"
	desc = "Insignia denoting assignment to the medical department. These fit Expeditionary Corps uniforms."
	color = "#4c9ce4"

/obj/item/clothing/accessory/reich/department/medical/service
	icon_state = "dept_exped_service"

/obj/item/clothing/accessory/reich/department/medical/fleet
	icon_state = "dept_fleet"
	desc = "Insignia denoting assignment to the medical department. These fit Fleet uniforms."
	on_rolled = list("rolled" = "dept_fleet_sleeves", "down" = "none")

/obj/item/clothing/accessory/reich/department/medical/marine
	icon_state = "dept_marine"
	desc = "Insignia denoting assignment to the medical department. These fit Marine Corps uniforms."
	on_rolled = list("down" = "none")

/obj/item/clothing/accessory/reich/department/supply
	name = "supply insignia"
	desc = "Insignia denoting assignment to the supply department. These fit Expeditionary Corps uniforms."
	color = "#bb9042"

/obj/item/clothing/accessory/reich/department/supply/service
	icon_state = "dept_exped_service"

/obj/item/clothing/accessory/reich/department/supply/fleet
	icon_state = "dept_fleet"
	desc = "Insignia denoting assignment to the supply department. These fit Fleet uniforms."
	on_rolled = list("rolled" = "dept_fleet_sleeves", "down" = "none")

/obj/item/clothing/accessory/reich/department/supply/marine
	icon_state = "dept_marine"
	desc = "Insignia denoting assignment to the supply department. These fit Marine Corps uniforms."
	on_rolled = list("down" = "none")

/obj/item/clothing/accessory/reich/department/service
	name = "service insignia"
	desc = "Insignia denoting assignment to the service department. These fit Expeditionary Corps uniforms."
	color = "#6eaa2c"

/obj/item/clothing/accessory/reich/department/service/service
	icon_state = "dept_exped_service"

/obj/item/clothing/accessory/reich/department/service/fleet
	icon_state = "dept_fleet"
	desc = "Insignia denoting assignment to the service department. These fit Fleet uniforms."
	on_rolled = list("rolled" = "dept_fleet_sleeves", "down" = "none")

/obj/item/clothing/accessory/reich/department/service/marine
	icon_state = "dept_marine"
	desc = "Insignia denoting assignment to the service department. These fit Marine Corps uniforms."
	on_rolled = list("down" = "none")

/obj/item/clothing/accessory/reich/department/exploration
	name = "exploration insignia"
	desc = "Insignia denoting assignment to the exploration department. These fit Expeditionary Corps uniforms."
	color = "#68099e"

/obj/item/clothing/accessory/reich/department/exploration/service
	icon_state = "dept_exped_service"

/obj/item/clothing/accessory/reich/department/exploration/fleet
	icon_state = "dept_fleet"
	desc = "Insignia denoting assignment to the exploration department. These fit Fleet uniforms."
	on_rolled = list("rolled" = "dept_fleet_sleeves", "down" = "none")

/obj/item/clothing/accessory/reich/department/exploration/marine
	icon_state = "dept_marine"
	desc = "Insignia denoting assignment to the exploration department. These fit Marine Corps uniforms."
	on_rolled = list("down" = "none")

/*********
ranks - ec
*********/

/obj/item/clothing/accessory/reich/rank
	name = "ranks"
	desc = "Insignia denoting rank of some kind. These appear blank."
	icon_state = "fleetrank"
	on_rolled = list("down" = "none")
	slot = ACCESSORY_SLOT_RANK
	gender = PLURAL
	high_visibility = 1

/obj/item/clothing/accessory/reich/rank/get_fibers()
	return null
/************
ranks - fleet
************/
/obj/item/clothing/accessory/reich/rank/fleet
	name = "naval ranks"
	desc = "Insignia denoting naval rank of some kind. These appear blank."
	icon_state = "fleetrank"
	on_rolled = list("down" = "none")

/obj/item/clothing/accessory/reich/rank/fleet/enlisted
	name = "ranks (E-1 crewman recruit)"
	desc = "Insignia denoting the rank of Crewman Recruit."
	icon_state = "fleetrank_enlisted"

/obj/item/clothing/accessory/reich/rank/fleet/enlisted/e2
	name = "ranks (E-2 crewman apprentice)"
	desc = "Insignia denoting the rank of Crewman Apprentice."

/obj/item/clothing/accessory/reich/rank/fleet/enlisted/e3
	name = "ranks (E-3 crewman)"
	desc = "Insignia denoting the rank of Crewman."

/obj/item/clothing/accessory/reich/rank/fleet/enlisted/e4
	name = "ranks (E-4 petty officer third class)"
	desc = "Insignia denoting the rank of Petty Officer Third Class."

/obj/item/clothing/accessory/reich/rank/fleet/enlisted/e5
	name = "ranks (E-5 petty officer second class)"
	desc = "Insignia denoting the rank of Petty Officer Second Class."

/obj/item/clothing/accessory/reich/rank/fleet/enlisted/e6
	name = "ranks (E-6 petty officer first class)"
	desc = "Insignia denoting the rank of Petty Officer First Class."

/obj/item/clothing/accessory/reich/rank/fleet/enlisted/e7
	name = "ranks (E-7 chief petty officer)"
	desc = "Insignia denoting the rank of Chief Petty Officer."

/obj/item/clothing/accessory/reich/rank/fleet/enlisted/e8
	name = "ranks (E-8 senior chief petty officer)"
	desc = "Insignia denoting the rank of Senior Chief Petty Officer."

/obj/item/clothing/accessory/reich/rank/fleet/enlisted/e9
	name = "ranks (E-9 master chief petty officer)"
	desc = "Insignia denoting the rank of Master Chief Petty Officer."

/obj/item/clothing/accessory/reich/rank/fleet/enlisted/e9_alt1
	name = "ranks (E-9 command master chief petty officer)"
	desc = "Insignia denoting the rank of Command Master Chief Petty Officer."

/obj/item/clothing/accessory/reich/rank/fleet/enlisted/e9_alt2
	name = "ranks (E-9 fleet master chief petty officer)"
	desc = "Insignia denoting the rank of Fleet Master Chief Petty Officer."

/obj/item/clothing/accessory/reich/rank/fleet/enlisted/e9_alt3
	name = "ranks (E-9 force master chief petty officer)"
	desc = "Insignia denoting the rank of Force Master Chief Petty Officer."

/obj/item/clothing/accessory/reich/rank/fleet/enlisted/e9_alt4
	name = "ranks (E-9 master chief petty officer of the Fleet)"
	desc = "Insignia denoting the rank of Master Chief Petty Officer of the Fleet."

/obj/item/clothing/accessory/reich/rank/fleet/officer
	name = "ranks (O-1 ensign)"
	desc = "Insignia denoting the rank of Ensign."
	icon_state = "fleetrank_officer"

/obj/item/clothing/accessory/reich/rank/fleet/officer/wo1_monkey
	name = "makeshift ranks (WO-1 warrant officer 1)"
	desc = "Insignia denoting the elusive rank of Warrant Officer. Too bad it's obviously fake."

/obj/item/clothing/accessory/reich/rank/fleet/officer/o2
	name = "ranks (O-2 sub-lieutenant)"
	desc = "Insignia denoting the rank of Sub-lieutenant."

/obj/item/clothing/accessory/reich/rank/fleet/officer/o3
	name = "ranks (O-3 lieutenant)"
	desc = "Insignia denoting the rank of Lieutenant."

/obj/item/clothing/accessory/reich/rank/fleet/officer/o4
	name = "ranks (O-4 lieutenant commander)"
	desc = "Insignia denoting the rank of Lieutenant Commander."

/obj/item/clothing/accessory/reich/rank/fleet/officer/o5
	name = "ranks (O-5 commander)"
	desc = "Insignia denoting the rank of Commander."

/obj/item/clothing/accessory/reich/rank/fleet/officer/o6
	name = "ranks (O-6 captain)"
	desc = "Insignia denoting the rank of Captain."
	icon_state = "fleetrank_command"

/obj/item/clothing/accessory/reich/rank/fleet/flag
	name = "ranks (O-7 commodore)"
	desc = "Insignia denoting the rank of Commodore."
	icon_state = "fleetrank_command"

/obj/item/clothing/accessory/reich/rank/fleet/flag/o8
	name = "ranks (O-8 rear admiral)"
	desc = "Insignia denoting the rank of Rear Admiral."

/obj/item/clothing/accessory/reich/rank/fleet/flag/o9
	name = "ranks (O-9 vice admiral)"
	desc = "Insignia denoting the rank of Vice Admiral."

/obj/item/clothing/accessory/reich/rank/fleet/flag/o10
	name = "ranks (O-10 admiral)"
	desc = "Insignia denoting the rank of Admiral."

/obj/item/clothing/accessory/reich/rank/fleet/flag/o10_alt
	name = "ranks (O-10 fleet admiral)"
	desc = "Insignia denoting the rank of Fleet Admiral."

/**************
ranks - heer
**************/
/obj/item/clothing/accessory/reich/rank/heer
	name = "heer ranks"
	desc = "Insignia denoting heer rank of some kind. These appear blank."
	icon_state = "heerrank_enlisted"
	on_rolled = list("down" = "none")

/obj/item/clothing/accessory/reich/rank/heer/enlisted
	name = "ranks (S - Schutze)"
	desc = "Insignia denoting the rank of Schutze."
	icon_state = "heerrank_enlisted"

/obj/item/clothing/accessory/reich/rank/heer/enlisted/e4
	name = "ranks (OS - Oberschutze)"
	desc = "Insignia denoting the rank of Oberschutze."

/obj/item/clothing/accessory/reich/rank/heer/enlisted/e5
	name = "ranks (G - Gefreiter)"
	desc = "Insignia denoting the rank of Gefreiter."

/obj/item/clothing/accessory/reich/rank/heer/enlisted/e6
	name = "ranks (OG - Obergefreiter)"
	desc = "Insignia denoting the rank of Obergefreiter."

/obj/item/clothing/accessory/reich/rank/heer/enlisted/e7
	name = "ranks (HG - Hauptgefreiter)"
	desc = "Insignia denoting the rank of Hauptgefreiter."

/obj/item/clothing/accessory/reich/rank/heer/enlisted/e8
	name = "ranks (SG - Stabsgefreiter)"
	desc = "Insignia denoting the rank of Stabsgefreiter."

/obj/item/clothing/accessory/reich/rank/heer/enlisted/e9
	name = "ranks (UO - Unteroffizier)"
	desc = "Insignia denoting the rank of Unteroffizier."


/obj/item/clothing/accessory/reich/rank/heer/officer
	name = "ranks (L - Leutnant)"
	desc = "Insignia denoting the rank of Leutnant."
	icon_state = "heerrank_officer"

/obj/item/clothing/accessory/reich/rank/heer/officer/o2
	name = "ranks (OLT - Oberleutnant)"
	desc = "Insignia denoting the rank of Oberleutnant."

/obj/item/clothing/accessory/reich/rank/heer/officer/o3
	name = "ranks (HM - Hauptmann)"
	desc = "Insignia denoting the rank of Hauptmann."

/obj/item/clothing/accessory/reich/rank/heer/officer/o4
	name = "ranks (M - Major)"
	desc = "Insignia denoting the rank of Major."

/obj/item/clothing/accessory/reich/rank/heer/officer/o5
	name = "ranks (O - Oberst)"
	desc = "Insignia denoting the rank of Oberst."

/obj/item/clothing/accessory/reich/rank/heer/officer/o6
	name = "ranks (GM - Generalmajor)"
	desc = "Insignia denoting the rank of Generalmajor."
	icon_state = "heerrank_command"

/obj/item/clothing/accessory/reich/rank/heer/flag
	name = "ranks (GLT - Generalleutnant)"
	desc = "Insignia denoting the rank of Generalleutnant."
	icon_state = "heerrank_command"

/obj/item/clothing/accessory/reich/rank/heer/flag/o8
	name = "ranks (GDI - General der Infanterie)"
	desc = "Insignia denoting the rank of General der Infanterie."

/obj/item/clothing/accessory/reich/rank/heer/flag/o9
	name = "ranks (GOBT - Generaloberst)"
	desc = "Insignia denoting the rank of Generaloberst."

/obj/item/clothing/accessory/reich/rank/heer/flag/o10
	name = "ranks (GFM - Generalfeldmarschall)"
	desc = "Insignia denoting the rank of Generalfeldmarschall."

/obj/item/clothing/accessory/reich/rank/heer/flag/o10_alt
	name = "ranks (RM - Reichsmarschall)"
	desc = "Insignia denoting the rank of Reichsmarschall."
