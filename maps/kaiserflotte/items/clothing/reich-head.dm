/obj/item/clothing/head/reich
	name = "master reich hat"
	icon = 'maps/kaiserflotte/icons/obj/reich-head.dmi'
	item_icons = list(slot_head_str = 'maps/kaiserflotte/icons/mob/reich-head.dmi')
	armor = list(melee = 0, bullet = 0, laser = 0, energy = 0, bomb = 0, bio = 0, rad = 0)
	siemens_coefficient = 0.9

//Utility
/obj/item/clothing/head/soft/reich
	name = "reichsheer cap"
	desc = "This is the standard army cap of the Teutonic Empire infantry."
	icon_state = "reichcap"
	icon = 'maps/kaiserflotte/icons/obj/reich-head.dmi'
	item_icons = list(slot_head_str = 'maps/kaiserflotte/icons/mob/reich-head.dmi')

/obj/item/clothing/head/soft/reich/cap
	name = "forage-cap"
	desc = "This is the forage-cap in the Teutonic army."
	icon_state = "reichpilotka"

/obj/item/clothing/head/soft/reich/cap/officer
	name = "officer cap"
	desc = "This is an officer's cap used by the command of the Teutonic Empire."
	icon_state = "reichhat"

/obj/item/clothing/head/soft/reich/cap/comissar
	name = "comissar cap"
	desc = "This is an officer's cap used by the Komissars."
	icon_state = "comhat"

/obj/item/clothing/head/soft/reich/cap/oberofficer
	name = "black officer cap"
	desc = "This is an black officer's cap used by the command of the Teutonic Empire."
	icon_state = "oberreichhat"

/obj/item/clothing/head/soft/reich/cap/old/officer
	name = "old fleet cap"
	desc = "Old-style cap for Teutonic Empire fleet."
	icon_state = "oldofficerhat"

/obj/item/clothing/head/soft/reich/cap/old/capitan
	name = "old capitan cap"
	desc = "Old-style officer cap for Teutonic Empire fleet."
	icon_state = "oldcapofficerhat"

//helmets and other such trash

/obj/item/clothing/head/helmet/reich

/obj/item/clothing/head/helmet/reich/m35helmet
	name = "feldgrau stahlhelm"
	desc = "A military helmet used by the army of the Teutonic Empire."
	icon_state = "mhelmet"
	icon = 'maps/kaiserflotte/icons/obj/reich-head.dmi'
	item_icons = list(slot_head_str = 'maps/kaiserflotte/icons/mob/reich-head.dmi')
	starting_accessories = null

/obj/item/clothing/head/helmet/reich/m35darkhelmet
	name = "black stahlhelm"
	desc = "A military helmet used by the elite forces of the Teutonic Empire."
	icon_state = "mblackhelmet"
	icon = 'maps/kaiserflotte/icons/obj/reich-head.dmi'
	item_icons = list(slot_head_str = 'maps/kaiserflotte/icons/mob/reich-head.dmi')
	starting_accessories = null

/obj/item/clothing/head/helmet/reich/pickelhelm
	name = "pickelhelm"
	desc = "The steel helmet with the image of an eagle is used by the higher generals and the elite of the Teutonic society."
	icon_state = "pickelhelm"
	icon = 'maps/kaiserflotte/icons/obj/reich-head.dmi'
	item_icons = list(slot_head_str = 'maps/kaiserflotte/icons/mob/reich-head.dmi')
	starting_accessories = null
