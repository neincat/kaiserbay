/datum/map/kaiserflotte
	species_to_job_whitelist = list(
		/datum/species/nabber = list(/datum/job/ai, /datum/job/cyborg, /datum/job/janitor, /datum/job/scientist_assistant, /datum/job/chemist,
		/datum/job/roboticist, /datum/job/chef, /datum/job/bartender),
		/datum/species/vox = list(/datum/job/ai, /datum/job/cyborg, /datum/job/merchant, /datum/job/stowaway)
	)

#define HUMAN_ONLY_JOBS /datum/job/captain, /datum/job/hop, /datum/job/cmo, /datum/job/chief_engineer, /datum/job/hos, /datum/job/representative, /datum/job/sea, /datum/job/warden
	species_to_job_blacklist = list(
		/datum/species/unathi  = list(HUMAN_ONLY_JOBS), //Other jobs unavailable via branch restrictions,
		/datum/species/skrell  = list(HUMAN_ONLY_JOBS),
		/datum/species/tajaran = list(HUMAN_ONLY_JOBS),
		/datum/species/machine = list(HUMAN_ONLY_JOBS),
		/datum/species/diona   = list(HUMAN_ONLY_JOBS),	//Other jobs unavailable via branch restrictions,
	)
#undef HUMAN_ONLY_JOBS

	allowed_jobs = list(/datum/job/captain, /datum/job/hop, /datum/job/rd, /datum/job/cmo, /datum/job/chief_engineer, /datum/job/hos,
						/datum/job/representative, /datum/job/sea, /datum/job/maid,
						/datum/job/mining, /datum/job/senior_engineer, /datum/job/engineer, /datum/job/roboticist,
						/datum/job/officer, /datum/job/warden, /datum/job/detective,
						/datum/job/senior_doctor, /datum/job/doctor, /datum/job/chemist,
						/datum/job/psychiatrist,
						/datum/job/qm, /datum/job/cargo_tech,
						/datum/job/janitor, /datum/job/chef, /datum/job/bartender,
						/datum/job/senior_scientist, /datum/job/scientist, /datum/job/scientist_assistant,
						/datum/job/crew, /datum/job/assistant,
						/datum/job/merchant, /datum/job/stowaway
						)


/datum/map/kaiserflotte/setup_map()
	..()
	for(var/job_type in GLOB.using_map.allowed_jobs)
		var/datum/job/job = decls_repository.get_decl(job_type)
		// Most species are restricted from SCG security and command roles
		if((job.department_flag & (COM)) && job.allowed_branches.len && !(/datum/mil_branch/civilian in job.allowed_branches))
			for(var/species_name in list(SPECIES_IPC, SPECIES_TAJARA, SPECIES_SKRELL, SPECIES_UNATHI))
				var/datum/species/S = all_species[species_name]
				var/species_blacklist = species_to_job_blacklist[S.type]
				if(!species_blacklist)
					species_blacklist = list()
					species_to_job_blacklist[S.type] = species_blacklist
				species_blacklist |= job.type

/datum/job/captain
	title = "Reichskommissar"
	supervisors = "the Teutonic Empire"
	minimal_player_age = 21
	economic_power = 15
	ideal_character_age = 50
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/command/CO
	allowed_branches = list(
		/datum/mil_branch/heer
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/o7
	)
	alt_titles = list(
		"Statthalter",
		"Generalgouverneur",
		"Burgermeister"
		)
	min_skill = list(	SKILL_MANAGEMENT  = SKILL_ADEPT,
						SKILL_BUREAUCRACY = SKILL_BASIC,
						SKILL_SCIENCE     = SKILL_ADEPT,
						SKILL_PILOT       = SKILL_ADEPT)
	skill_points = 40

	software_on_spawn = list(/datum/computer_file/program/comm,
							 /datum/computer_file/program/card_mod,
							 /datum/computer_file/program/camera_monitor)

/datum/job/captain/get_description_blurb()
	return "You are the Reichskommissar. You are the head of this colony. You are a top dog and are responsible for the efficiency of the colony. Your job is to make sure [GLOB.using_map.full_name] fulfils its mission. Delegate to your Executive Officer, your department heads, and your Senior Enlisted Advisor to effectively manage the colony, and listen to and trust their expertise."

/datum/job/hop
	title = "Kolonialminister"
	supervisors = "the Reichskommissar"
	department = "Command"
	department_flag = COM
	minimal_player_age = 21
	economic_power = 10
	ideal_character_age = 45
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/command/XO
	allowed_branches = list(
		/datum/mil_branch/heer,
		/datum/mil_branch/civilian
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/o6,
		/datum/mil_rank/heer/o5,
		/datum/mil_rank/heer/o4
	)
	min_skill = list(	SKILL_MANAGEMENT  = SKILL_ADEPT,
						SKILL_BUREAUCRACY = SKILL_ADEPT,
						SKILL_COMPUTER    = SKILL_BASIC,
						SKILL_PILOT       = SKILL_BASIC)
	skill_points = 40

	access = list(access_security, access_brig, access_armory, access_forensics_lockers,
			            access_medical, access_morgue, access_engine, access_engine_equip, access_maint_tunnels, access_external_airlocks, access_emergency_storage,
			            access_change_ids, access_ai_upload, access_teleporter, access_eva, access_heads,
			            access_all_personal_lockers, access_chapel_office, access_tech_storage, access_atmospherics, access_bar, access_janitor, access_crematorium, access_robotics,
			            access_kitchen, access_cargo, access_construction, access_chemistry, access_cargo_bot, access_hydroponics, access_library, access_virology,
			            access_cmo, access_qm, access_network, access_surgery, access_mailsorting, access_heads_vault, access_ce,
			            access_hop, access_hos, access_RC_announce, access_keycard_auth, access_tcomsat, access_gateway, access_sec_doors, access_psychiatrist,
			            access_medical_equip, access_solgov_crew, access_robotics_engineering, access_emergency_armory, access_gun, access_expedition_shuttle, access_guppy,
			            access_seneng, access_senmed, access_senadv, access_hangar, access_guppy_helm, access_expedition_shuttle_helm, access_aquila, access_aquila_helm, access_explorer, access_pathfinder)
	minimal_access = list(access_security, access_brig, access_armory, access_forensics_lockers,
			            access_medical, access_morgue, access_engine, access_engine_equip, access_maint_tunnels, access_external_airlocks, access_emergency_storage,
			            access_change_ids, access_ai_upload, access_teleporter, access_eva, access_heads,
			            access_all_personal_lockers, access_chapel_office, access_tech_storage, access_atmospherics, access_bar, access_janitor, access_crematorium,
			            access_kitchen, access_cargo, access_construction, access_chemistry, access_cargo_bot, access_hydroponics, access_library, access_virology,
			            access_cmo, access_qm, access_network, access_surgery, access_mailsorting, access_heads_vault, access_ce,
			            access_hop, access_hos, access_RC_announce, access_keycard_auth, access_tcomsat, access_gateway, access_sec_doors, access_psychiatrist,
			            access_medical_equip, access_solgov_crew, access_robotics_engineering, access_emergency_armory, access_gun, access_expedition_shuttle, access_guppy,
			            access_seneng, access_senmed, access_senadv, access_hangar, access_guppy_helm, access_aquila, access_aquila_helm, access_explorer, access_pathfinder,
			            access_expedition_shuttle_helm)

	software_on_spawn = list(/datum/computer_file/program/comm,
							 /datum/computer_file/program/card_mod,
							 /datum/computer_file/program/camera_monitor)

/datum/job/hop/get_description_blurb()
	return "You are the Kolonialminister. You are an experienced official, the second man of the colony, and are responsible for the smooth operation of the colony under your Reichskomissar. In his absence, you are expected to take his place. Your primary duty is directly managing department heads and all those outside a department heading. You are also responsible for the contractors and civilians on the colony."

/datum/job/maid
	title = "Maid"
	department = "Command"
	department_flag = COM
	total_positions = 1
	spawn_positions = 1
	selection_color = "#2f2f7f"
	supervisors = "your master"
	economic_power = 5
	announced = FALSE
	//outfit_type = /decl/hierarchy/outfit/job/maid
	allowed_branches = list(/datum/mil_branch/civilian)
	allowed_ranks = list(
		/datum/mil_rank/civ/civ,
	)

/datum/job/rd
	title = "Research Director"
	supervisors = "the Reichskommissar and the Kolonialminister"
	economic_power = 20
	minimal_player_age = 14
	ideal_character_age = 60
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/passenger/research/rd
	allowed_branches = list(/datum/mil_branch/civilian)
	allowed_ranks = list(
		/datum/mil_rank/civ/official
	)
	min_skill = list(	SKILL_MANAGEMENT  = SKILL_ADEPT,
						SKILL_BUREAUCRACY = SKILL_ADEPT,
						SKILL_COMPUTER    = SKILL_BASIC,
						SKILL_FINANCE     = SKILL_ADEPT,
						SKILL_BOTANY      = SKILL_BASIC,
						SKILL_ANATOMY     = SKILL_BASIC,
						SKILL_DEVICES     = SKILL_BASIC,
						SKILL_SCIENCE     = SKILL_ADEPT)
	skill_points = 40

	access = list(access_tox, access_tox_storage, access_emergency_storage, access_teleporter, access_heads, access_rd,
						access_research, access_mining, access_mining_office, access_mining_station, access_xenobiology,
						access_RC_announce, access_keycard_auth, access_xenoarch, access_nanotrasen, access_sec_guard,
						access_expedition_shuttle, access_guppy, access_hangar, access_petrov, access_petrov_helm, access_guppy_helm)
	minimal_access = list()

	software_on_spawn = list(/datum/computer_file/program/comm,
							 /datum/computer_file/program/aidiag,
							 /datum/computer_file/program/camera_monitor)

/datum/job/rd/get_description_blurb()
	return "You are the Research Director. You are responsible for the research department. You handle both the science part of the mission."

/datum/job/cmo
	title = "Chief Medical Officer"
	supervisors = "the Reichskommissar and the Kolonialminister"
	economic_power = 10
	minimal_player_age = 21
	ideal_character_age = 48
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/command/cmo
	allowed_branches = list(
		/datum/mil_branch/heer,
		/datum/mil_branch/civilian = /decl/hierarchy/outfit/job/kaiserflotte/crew/command/cmo/civilian
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/o1,
		/datum/mil_rank/heer/o3,
		/datum/mil_rank/heer/o2,
		/datum/mil_rank/civ/official
	)
	min_skill = list(	SKILL_MANAGEMENT  = SKILL_ADEPT,
						SKILL_BUREAUCRACY = SKILL_BASIC,
						SKILL_MEDICAL     = SKILL_ADEPT,
						SKILL_ANATOMY     = SKILL_EXPERT,
						SKILL_CHEMISTRY   = SKILL_BASIC,
						SKILL_VIROLOGY    = SKILL_BASIC)
	skill_points = 40

	access = list(access_medical, access_morgue, access_maint_tunnels, access_external_airlocks, access_emergency_storage,
			            access_teleporter, access_eva, access_heads,
			            access_chapel_office, access_crematorium, access_chemistry, access_virology,
			            access_cmo, access_surgery, access_RC_announce, access_keycard_auth, access_psychiatrist,
			            access_medical_equip, access_solgov_crew, access_senmed, access_hangar)
	minimal_access = list()

	software_on_spawn = list(/datum/computer_file/program/comm,
							 /datum/computer_file/program/suit_sensors,
							 /datum/computer_file/program/camera_monitor)

/datum/job/cmo/get_description_blurb()
	return "You are the Chief Medical Officer. You manage the medical department. You ensure all members of medical are skilled, tasked and handling their duties. Ensure your doctors are staffing your infirmary and your corpsman/paramedics are ready for response. Act as a second surgeon or backup chemist in the absence of either. You are expected to know medical very well, along with general regulations."

/datum/job/chief_engineer
	title = "Chief Engineer"
	supervisors = "the Reichskommissar and the Kolonialminister"
	economic_power = 9
	ideal_character_age = 40
	minimal_player_age = 21
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/command/chief_engineer
	allowed_branches = list(
		/datum/mil_branch/heer,
		/datum/mil_branch/civilian = /decl/hierarchy/outfit/job/kaiserflotte/crew/command/chief_engineer/civilian
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/o1,
		/datum/mil_rank/heer/o3,
		/datum/mil_rank/heer/o2,
		/datum/mil_rank/civ/official
	)
	min_skill = list(	SKILL_MANAGEMENT   = SKILL_ADEPT,
						SKILL_BUREAUCRACY  = SKILL_BASIC,
						SKILL_COMPUTER     = SKILL_ADEPT,
						SKILL_EVA          = SKILL_ADEPT,
						SKILL_CONSTRUCTION = SKILL_ADEPT,
						SKILL_ELECTRICAL   = SKILL_ADEPT,
						SKILL_ATMOS        = SKILL_ADEPT,
						SKILL_ENGINES      = SKILL_EXPERT)
	skill_points = 32

	access = list(access_engine, access_engine_equip, access_maint_tunnels, access_external_airlocks, access_emergency_storage,
			            access_ai_upload, access_teleporter, access_eva, access_heads,
			            access_tech_storage, access_robotics, access_atmospherics, access_janitor, access_construction,
			            access_network, access_ce, access_RC_announce, access_keycard_auth, access_tcomsat,
			            access_solgov_crew, access_robotics_engineering, access_seneng, access_hangar, access_robotics)
	minimal_access = list(access_engine, access_engine_equip, access_maint_tunnels, access_external_airlocks, access_emergency_storage,
			            access_ai_upload, access_teleporter, access_eva, access_heads,
			            access_tech_storage, access_atmospherics, access_janitor, access_construction,
			            access_network, access_ce, access_RC_announce, access_keycard_auth, access_tcomsat,
			            access_solgov_crew, access_robotics_engineering, access_seneng, access_hangar, access_robotics)

	software_on_spawn = list(/datum/computer_file/program/comm,
							 /datum/computer_file/program/ntnetmonitor,
							 /datum/computer_file/program/power_monitor,
							 /datum/computer_file/program/supermatter_monitor,
							 /datum/computer_file/program/alarm_monitor,
							 /datum/computer_file/program/atmos_control,
							 /datum/computer_file/program/rcon_console,
							 /datum/computer_file/program/camera_monitor,
							 /datum/computer_file/program/shields_monitor)

/datum/job/chief_engineer/get_description_blurb()
	return "You are the Chief Engineer. You manage the Engineering Department. You are responsible for the Senior engineer, who is your right hand and (should be) an experienced, skilled engineer. Delegate to and listen to them. Manage your engineers, ensure vessel power stays on, breaches are patched and problems are fixed. Advise the CO on engineering matters. You are also responsible for the maintenance and control of any vessel synthetics. You are an experienced engineer with a wealth of theoretical knowledge. You should also know vessel regulations to a reasonable degree."

/datum/job/hos
	title = "Polizeidirektor"
	supervisors = "the Reichskommissar and the Kolonialminister"
	economic_power = 8
	minimal_player_age = 21
	ideal_character_age = 35
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/command/cos
	allowed_branches = list(
		/datum/mil_branch/heer
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/o5
	)
	min_skill = list(	SKILL_MANAGEMENT  = SKILL_ADEPT,
						SKILL_BUREAUCRACY = SKILL_ADEPT,
						SKILL_EVA         = SKILL_BASIC,
						SKILL_COMBAT      = SKILL_BASIC,
						SKILL_WEAPONS     = SKILL_ADEPT,
						SKILL_FORENSICS   = SKILL_BASIC)
	skill_points = 30

	access = list(access_security, access_brig, access_armory, access_forensics_lockers,
			            access_maint_tunnels, access_external_airlocks, access_emergency_storage,
			            access_teleporter, access_eva, access_heads,
			            access_hos, access_RC_announce, access_keycard_auth, access_sec_doors,
			            access_solgov_crew, access_gun, access_emergency_armory)
	minimal_access = list()

	software_on_spawn = list(/datum/computer_file/program/comm,
							 /datum/computer_file/program/digitalwarrant,
							 /datum/computer_file/program/camera_monitor)

/datum/job/hos/get_description_blurb()
	return "You are the Chief of Security. You manage ship security. The Masters at Arms and the Military Police, as well as the Brig Officer and the Forensic Technician. You keep the vessel safe. You handle both internal and external security matters. You are the law. You are subordinate to the CO and the XO. You are expected to know the SCMJ and Sol law and Alert Procedure to a very high degree along with general regulations."


/datum/job/representative
	title = "Offizier der Verdecktepolizei"
	department = "Support"
	department_flag = SPT

	total_positions = 1
	spawn_positions = 1
	supervisors = "the Reichcomissar"
	selection_color = "#2f2f7f"
	economic_power = 15
	minimal_player_age = 10

	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/representative
	allowed_branches = list(/datum/mil_branch/civilian)
	allowed_ranks = list(/datum/mil_rank/civ/civ)
	min_skill = list(	SKILL_BUREAUCRACY = SKILL_EXPERT,
						SKILL_FINANCE     = SKILL_BASIC)
	skill_points = 20

	access = list(access_representative, access_security,access_medical, access_engine,
			            access_heads, access_cargo, access_solgov_crew, access_hangar)

/datum/job/representative/get_description_blurb()
	return "You are the Sol Gov Representative. You are a civilian assigned as both a diplomatic liaison for first contact and foreign affair situations on board. You are also responsible for monitoring for any serious missteps of justice, sol law or other ethical or legal issues aboard and informing and advising the Commanding Officer of them. You are a mid-level bureaucrat. You liaise between the crew and Nanotrasen interests on board. Send faxes back to Sol on mission progress and important events."

/datum/job/sea
	title = "Kommissar"
	department = "Support"
	department_flag = SPT

	total_positions = 1
	spawn_positions = 1
	supervisors = "the Reichskommissar and the Kolonialminister"
	selection_color = "#2f2f7f"
	minimal_player_age = 21
	economic_power = 8
	ideal_character_age = 45
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/command/sea
	allowed_branches = list(
		/datum/mil_branch/heer
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/o4,
		/datum/mil_rank/heer/o3
	)
	min_skill = list(	SKILL_MANAGEMENT = SKILL_ADEPT,
						SKILL_EVA        = SKILL_BASIC,
						SKILL_COMBAT     = SKILL_BASIC,
						SKILL_WEAPONS    = SKILL_ADEPT)
	skill_points = 24


	access = list(access_security, access_medical, access_engine, access_maint_tunnels, access_external_airlocks, access_emergency_storage,
			            access_teleporter, access_eva, access_heads, access_all_personal_lockers, access_janitor,
			            access_kitchen, access_cargo, access_RC_announce, access_keycard_auth, access_guppy_helm,
			            access_solgov_crew, access_gun, access_expedition_shuttle, access_guppy, access_senadv, access_hangar, access_emergency_armory)

	software_on_spawn = list(/datum/computer_file/program/camera_monitor)

/datum/job/sea/get_description_blurb()
	return "You are the Senior Enlisted Advisor. You are the highest enlisted person on the ship. You are directly subordinate to the CO. You advise them on enlisted concerns and provide expertise and advice to officers. You are responsible for ensuring discipline and good conduct among enlisted, as well as notifying officers of any issues and “advising” them on mistakes they make. You also handle various duties on behalf of the CO and XO. You are an experienced enlisted person, very likely equal only in experience to the CO and XO. You know the regulations better than anyone."



/datum/job/senior_engineer
	title = "Senior Engineer"
	department = "Engineering"
	department_flag = ENG

	total_positions = 1
	spawn_positions = 1
	supervisors = "the Chief Engineer"
	selection_color = "#5b4d20"
	economic_power = 6
	minimal_player_age = 14
	ideal_character_age = 40
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/engineering/senior_engineer
	allowed_branches = list(
		/datum/mil_branch/heer,
		/datum/mil_branch/civilian
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/e8,
		/datum/mil_rank/heer/e6,
		/datum/mil_rank/heer/e7,
		/datum/mil_rank/heer/e5,
		/datum/mil_rank/civ/contractor
	)
	min_skill = list(	SKILL_MANAGEMENT   = SKILL_BASIC,
						SKILL_COMPUTER     = SKILL_BASIC,
						SKILL_EVA          = SKILL_ADEPT,
						SKILL_CONSTRUCTION = SKILL_ADEPT,
						SKILL_ELECTRICAL   = SKILL_ADEPT,
						SKILL_ATMOS        = SKILL_BASIC,
						SKILL_ENGINES      = SKILL_ADEPT)
	skill_points = 24

	access = list(access_engine, access_engine_equip, access_maint_tunnels, access_external_airlocks, access_emergency_storage,
			            access_teleporter, access_eva, access_tech_storage, access_atmospherics, access_janitor, access_construction,
			            access_tcomsat, access_solgov_crew, access_seneng, access_hangar)

	software_on_spawn = list(/datum/computer_file/program/power_monitor,
							 /datum/computer_file/program/supermatter_monitor,
							 /datum/computer_file/program/alarm_monitor,
							 /datum/computer_file/program/atmos_control,
							 /datum/computer_file/program/rcon_console,
							 /datum/computer_file/program/camera_monitor,
							 /datum/computer_file/program/shields_monitor)

/datum/job/senior_engineer/get_description_blurb()
	return "You are the Senior Engineer. You are a veteran SNCO. You are subordinate to the Chief Engineer though you may have many years more experience than them and your subordinates are the rest of engineering. You should be an expert in practically every engineering area and familiar and possess leadership skills. Coordinate the team and ensure the smooth running of the department along with the Chief Engineer."

/datum/job/engineer
	title = "Engineer"
	total_positions = 4
	spawn_positions = 4
	supervisors = "the Chief Engineer"
	economic_power = 5
	minimal_player_age = 7
	ideal_character_age = 30
	alt_titles = list(
		"Maintenance Technician",
		"Engine Technician",
		"Damage Control Technician",
		"EVA Technician",
		"Electrician",
		"Atmospheric Technician",
		)
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/engineering/engineer
	allowed_branches = list(
		/datum/mil_branch/heer,
		/datum/mil_branch/civilian = /decl/hierarchy/outfit/job/kaiserflotte/crew/engineering/engineer/civilian
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/e5,
		/datum/mil_rank/heer/e4,
		/datum/mil_rank/heer/e3,
		/datum/mil_rank/civ/contractor
	)
	min_skill = list(	SKILL_COMPUTER     = SKILL_BASIC,
						SKILL_EVA          = SKILL_BASIC,
						SKILL_CONSTRUCTION = SKILL_ADEPT,
						SKILL_ELECTRICAL   = SKILL_BASIC,
						SKILL_ATMOS        = SKILL_BASIC,
						SKILL_ENGINES      = SKILL_BASIC)

	access = list(access_engine, access_engine_equip, access_maint_tunnels, access_external_airlocks, access_emergency_storage,
			            access_teleporter, access_eva, access_tech_storage, access_atmospherics, access_janitor, access_construction,
			            access_solgov_crew, access_hangar)
	minimal_access = list()

	software_on_spawn = list(/datum/computer_file/program/power_monitor,
							 /datum/computer_file/program/supermatter_monitor,
							 /datum/computer_file/program/alarm_monitor,
							 /datum/computer_file/program/atmos_control,
							 /datum/computer_file/program/rcon_console,
							 /datum/computer_file/program/camera_monitor,
							 /datum/computer_file/program/shields_monitor)

/datum/job/engineer/get_description_blurb()
	return "You are an Engineer. You operate under one of many titles and may be highly specialised in a specific area of engineering. You probably have at least a general familiarity with most other areas though this is not expected. You are subordinate to the Senior Engineer and the Chief Engineer and are expected to follow them."


/datum/job/roboticist
	title = "Mechsuit Technician"
	department = "Engineering"
	department_flag = ENG|MED

	total_positions = 1
	spawn_positions = 1
	supervisors = "the Chief Engineer and the Chief Medical Officer"
	selection_color = "#5b4d20"
	economic_power = 6
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/engineering/roboticist
	allowed_branches = list(
			/datum/mil_branch/heer,
			/datum/mil_branch/civilian = /decl/hierarchy/outfit/job/kaiserflotte/crew/engineering/roboticist/civ
			)

	allowed_ranks = list(
		/datum/mil_rank/heer/e5,
		/datum/mil_rank/heer/e4,
		/datum/mil_rank/heer/e3,
		/datum/mil_rank/civ/contractor
	)
	min_skill = list(	SKILL_COMPUTER		= SKILL_ADEPT,
						SKILL_MECH = SKILL_ADEPT,
						SKILL_DEVICES		= SKILL_ADEPT)

	access = list(access_robotics, access_robotics_engineering, access_tech_storage, access_morgue, access_medical, access_robotics_engineering, access_solgov_crew)
	minimal_access = list()

/datum/job/roboticist/get_description_blurb()
	return "You are the Roboticist. You are responsible for repairing, upgrading and handling ship synthetics as well as the repair of all synthetic crew on board. You are also responsible for placing brains into MMI’s and giving them bodies and the production of exosuits(mechs) for various departments. You answer to the Chief Engineer."

/datum/job/warden
	title = "Polizeimeister"
	total_positions = 1
	spawn_positions = 1
	supervisors = "the Polizeidirector"
	economic_power = 5
	minimal_player_age = 14
	ideal_character_age = 35
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/security/brig_officer
	allowed_branches = list(
		/datum/mil_branch/heer,
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/e8,
		/datum/mil_rank/heer/e7,
		/datum/mil_rank/heer/e6,
		/datum/mil_rank/heer/e5
	)
	min_skill = list(	SKILL_MANAGEMENT  = SKILL_BASIC,
						SKILL_BUREAUCRACY = SKILL_ADEPT,
						SKILL_EVA         = SKILL_BASIC,
						SKILL_COMBAT      = SKILL_BASIC,
						SKILL_WEAPONS     = SKILL_BASIC,
						SKILL_FORENSICS   = SKILL_BASIC)
	skill_points = 20

	access = list(access_security, access_brig, access_armory, access_forensics_lockers,
			            access_maint_tunnels, access_external_airlocks, access_emergency_storage,
			            access_eva, access_sec_doors, access_solgov_crew, access_gun)
	minimal_access = list()

	software_on_spawn = list(/datum/computer_file/program/digitalwarrant,
							 /datum/computer_file/program/camera_monitor)

/datum/job/detective
	title = "Investigator"
	total_positions = 2
	spawn_positions = 2
	supervisors = "the Polizeidirector"
	alt_titles = list(
		"Criminalist"
		)
	economic_power = 5
	minimal_player_age = 7
	ideal_character_age = 35
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/security/forensic_tech
	allowed_branches = list(
		/datum/mil_branch/heer,
		/datum/mil_branch/civilian = /decl/hierarchy/outfit/job/kaiserflotte/crew/security/forensic_tech/contractor
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/e8,
		/datum/mil_rank/heer/e7,
		/datum/mil_rank/heer/e6,
		/datum/mil_rank/heer/e5,
		/datum/mil_rank/civ/contractor
	)
	min_skill = list(	SKILL_BUREAUCRACY = SKILL_BASIC,
						SKILL_COMPUTER    = SKILL_BASIC,
						SKILL_EVA         = SKILL_BASIC,
						SKILL_COMBAT      = SKILL_BASIC,
						SKILL_WEAPONS     = SKILL_BASIC,
						SKILL_FORENSICS   = SKILL_ADEPT)
	skill_points = 20

	access = list(access_security, access_brig, access_forensics_lockers,
			            access_maint_tunnels, access_emergency_storage,
			            access_sec_doors, access_solgov_crew, access_morgue)
	minimal_access = list()

	software_on_spawn = list(/datum/computer_file/program/digitalwarrant,
							 /datum/computer_file/program/camera_monitor)

/datum/job/officer
	title = "Polizist"
	total_positions = 4
	spawn_positions = 4
	supervisors = "the Chief of Security"
	economic_power = 4
	minimal_player_age = 10
	ideal_character_age = 25
	alt_titles = list()
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/security/maa
	allowed_branches = list(
		/datum/mil_branch/heer
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/e3,
		/datum/mil_rank/heer/e4
	)
	min_skill = list(	SKILL_BUREAUCRACY = SKILL_BASIC,
						SKILL_EVA         = SKILL_BASIC,
						SKILL_COMBAT      = SKILL_BASIC,
						SKILL_WEAPONS     = SKILL_BASIC,
						SKILL_FORENSICS   = SKILL_BASIC)

	access = list(access_security, access_brig, access_maint_tunnels,
						access_external_airlocks, access_emergency_storage,
			            access_eva, access_sec_doors, access_solgov_crew)
	minimal_access = list()

	software_on_spawn = list(/datum/computer_file/program/digitalwarrant,
							 /datum/computer_file/program/camera_monitor)

/datum/job/senior_doctor
	title = "Physician"
	department = "Medical"
	department_flag = MED

	minimal_player_age = 14
	ideal_character_age = 45
	total_positions = 2
	spawn_positions = 2
	supervisors = "the Chief Medical Officer"
	selection_color = "#013d3b"
	economic_power = 8
	alt_titles = list(
		"Surgeon",
		"Trauma Surgeon")
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical/senior
	allowed_branches = list(
		/datum/mil_branch/heer,
		/datum/mil_branch/civilian = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical/senior/civ
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/o1,
		/datum/mil_rank/heer/o2,
		/datum/mil_rank/civ/contractor
	)
	min_skill = list(	SKILL_BUREAUCRACY = SKILL_BASIC,
						SKILL_MEDICAL     = SKILL_ADEPT,
						SKILL_ANATOMY     = SKILL_EXPERT,
						SKILL_CHEMISTRY   = SKILL_BASIC,
						SKILL_VIROLOGY    = SKILL_BASIC)
	skill_points = 32

	access = list(access_medical, access_morgue, access_virology, access_maint_tunnels, access_emergency_storage,
			            access_crematorium, access_chemistry, access_surgery,
			            access_medical_equip, access_solgov_crew, access_senmed)

	software_on_spawn = list(/datum/computer_file/program/suit_sensors,
							 /datum/computer_file/program/camera_monitor)

/datum/job/doctor
	title = "Medic"
	minimal_player_age = 7
	total_positions = 5
	spawn_positions = 5
	supervisors = "the Chief Medical Officer"
	economic_power = 7
	ideal_character_age = 40
	alt_titles = list(
		"Medical Technician",
		"Nurse",
		"Orderly" = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/orderly,
		"Virologist" = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/virologist,
		"Xenosurgeon" = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/xenosurgeon,
		"Paramedic" = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/paramedic
		)
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical/doctor
	allowed_branches = list(
		/datum/mil_branch/civilian = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical/doctor/civ,
		/datum/mil_branch/heer
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/e3,
		/datum/mil_rank/heer/e4,
		/datum/mil_rank/heer/e5,
		/datum/mil_rank/heer/e6,
		/datum/mil_rank/civ/contractor
	)
	min_skill = list(	SKILL_EVA     = SKILL_BASIC,
						SKILL_MEDICAL = SKILL_BASIC,
						SKILL_ANATOMY = SKILL_BASIC)
	access = list(access_medical, access_morgue, access_maint_tunnels, access_external_airlocks, access_emergency_storage,
			            access_eva, access_surgery, access_medical_equip, access_solgov_crew, access_hangar)
	minimal_access = list()

	software_on_spawn = list(/datum/computer_file/program/suit_sensors,
							 /datum/computer_file/program/camera_monitor)
	skill_points = 26


/datum/job/chemist
	title = "Chemist"
	department = "Medical"
	department_flag = MED

	total_positions = 1
	spawn_positions = 1
	supervisors = "the Chief Medical Officer and Medical Personnel"
	selection_color = "#013d3b"
	economic_power = 4
	ideal_character_age = 30
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/chemist
	allowed_branches = list(/datum/mil_branch/civilian)
	allowed_ranks = list(/datum/mil_rank/civ/contractor)
	min_skill = list(	SKILL_MEDICAL   = SKILL_BASIC,
						SKILL_CHEMISTRY = SKILL_ADEPT)
	skill_points = 26

	access = list(access_medical, access_maint_tunnels, access_emergency_storage, access_medical_equip, access_solgov_crew, access_chemistry)
	minimal_access = list()

/datum/job/psychiatrist
	title = "Counselor"
	total_positions = 1
	spawn_positions = 1
	ideal_character_age = 40
	economic_power = 5
	supervisors = "the Chief Medical Officer"
	alt_titles = list(
		"Psychiatrist" = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical/counselor/psychiatrist,
		"Chaplain" = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical/counselor/chaplain,
	)
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical/counselor
	allowed_branches = list(
		/datum/mil_branch/civilian,
		/datum/mil_branch/heer = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical/counselor/heer)
	allowed_ranks = list(
		/datum/mil_rank/civ/contractor,
		/datum/mil_rank/heer/o1,
		/datum/mil_rank/heer/o2
		)
	min_skill = list(	SKILL_BUREAUCRACY = SKILL_BASIC,
						SKILL_MEDICAL     = SKILL_BASIC)

	access = list(access_medical, access_morgue, access_chapel_office, access_crematorium, access_psychiatrist, access_solgov_crew)
	minimal_access = list()

	software_on_spawn = list(/datum/computer_file/program/suit_sensors,
							 /datum/computer_file/program/camera_monitor)

/datum/job/qm
	title = "Ladenbesitzer"
	department = "Supply"
	department_flag = SUP
	total_positions = 1
	spawn_positions = 1
	supervisors = "the Kolonial Minister and Zweite Hanse"
	economic_power = 5
	minimal_player_age = 7
	ideal_character_age = 35
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/supply/deckofficer/civ
	allowed_branches = list(
		/datum/mil_branch/civilian
	)
	allowed_ranks = list(
		/datum/mil_rank/civ/contractor
	)
	min_skill = list(	SKILL_MANAGEMENT  = SKILL_BASIC,
						SKILL_BUREAUCRACY = SKILL_ADEPT,
						SKILL_FINANCE     = SKILL_BASIC,
						SKILL_HAULING     = SKILL_BASIC,
						SKILL_EVA         = SKILL_BASIC,
						SKILL_PILOT       = SKILL_BASIC)
	skill_points = 20

	access = list(access_maint_tunnels, access_heads, access_emergency_storage, access_tech_storage,  access_cargo, access_guppy_helm,
						access_cargo_bot, access_qm, access_mailsorting, access_solgov_crew, access_expedition_shuttle, access_guppy, access_hangar)
	minimal_access = list()

	software_on_spawn = list(/datum/computer_file/program/supply)

/datum/job/cargo_tech
	title = "Kurier"
	department = "Supply"
	department_flag = SUP
	total_positions = 2
	spawn_positions = 2
	supervisors = "the Quartiermeister and Kolonial Minister"
	minimal_player_age = 3
	ideal_character_age = 24
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/supply/tech/civ
	allowed_branches = list(
		/datum/mil_branch/civilian
	)
	allowed_ranks = list(
		/datum/mil_rank/civ/contractor
	)
	min_skill = list(	SKILL_BUREAUCRACY = SKILL_BASIC,
						SKILL_FINANCE     = SKILL_BASIC,
						SKILL_HAULING     = SKILL_BASIC)

	access = list(access_maint_tunnels, access_emergency_storage, access_cargo, access_guppy_helm,
						access_cargo_bot, access_mailsorting, access_solgov_crew, access_expedition_shuttle, access_guppy, access_hangar)
	minimal_access = list()

	software_on_spawn = list(/datum/computer_file/program/supply)

/datum/job/janitor
	title = "Janitor"
	department = "Service"
	department_flag = SRV

	total_positions = 2
	spawn_positions = 2
	supervisors = "the Kolonial Ministr"
	ideal_character_age = 20
	alt_titles = list(
		"Janitor")
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/service/janitor
	allowed_branches = list(
		/datum/mil_branch/civilian,
		/datum/mil_branch/heer = /decl/hierarchy/outfit/job/kaiserflotte/crew/service/janitor/mil
	)
	allowed_ranks = list(
		/datum/mil_rank/civ/contractor,
		/datum/mil_rank/heer/e3,
		/datum/mil_rank/heer/e4
	)
	min_skill = list(	SKILL_HAULING = SKILL_BASIC)

	access = list(access_maint_tunnels, access_emergency_storage, access_janitor, access_solgov_crew)
	minimal_access = list()

/datum/job/chef
	title = "Chef"
	department = "Service"
	department_flag = SRV
	total_positions = 1
	spawn_positions = 1
	supervisors = "the Executive Officer"
	alt_titles = list(
		"Chef",
		"Culinary Specialist"
		)
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/service/cook
	allowed_branches = list(
		/datum/mil_branch/civilian,
		/datum/mil_branch/heer = /decl/hierarchy/outfit/job/kaiserflotte/crew/service/cook/mil
	)
	allowed_ranks = list(
		/datum/mil_rank/civ/contractor,
		/datum/mil_rank/heer/e3,
		/datum/mil_rank/heer/e4
	)
	min_skill = list(	SKILL_COOKING   = SKILL_ADEPT,
						SKILL_BOTANY    = SKILL_BASIC,
						SKILL_CHEMISTRY = SKILL_BASIC)

	access = list(access_maint_tunnels, access_hydroponics, access_kitchen, access_solgov_crew, access_bar)
	minimal_access = list()

/datum/job/bartender
	title = "Bartender"
	department = "Service"
	department_flag = SRV
	supervisors = "the Kolonial Ministr"
	ideal_character_age = 30
	selection_color = "#515151"
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/service/bartender
	allowed_branches = list(/datum/mil_branch/civilian)
	allowed_ranks = list(/datum/mil_rank/civ/contractor)

	access = list(access_hydroponics, access_bar, access_solgov_crew, access_kitchen)
	minimal_access = list()
	min_skill = list(	SKILL_COOKING   = SKILL_BASIC,
						SKILL_BOTANY    = SKILL_BASIC,
						SKILL_CHEMISTRY = SKILL_BASIC)

/datum/job/crew
	title = "Garnison"
	department = "Service"
	department_flag = SRV

	total_positions = 5
	spawn_positions = 5
	supervisors = "the Kolonial Minister and Reichswehr Personnel"
	selection_color = "#515151"
	ideal_character_age = 20
	alt_titles = list(
		"Engineer Trainee",
		"Corpsman Trainee"
		)
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/service/crewman
	allowed_branches = list(
		/datum/mil_branch/heer
	)
	allowed_ranks = list(
		/datum/mil_rank/heer/e3,
		/datum/mil_rank/heer/e4
	)


	access = list(access_maint_tunnels, access_emergency_storage, access_solgov_crew)

/datum/job/senior_scientist
	title = "Senior Researcher"
	department = "Science"
	department_flag = SCI

	total_positions = 1
	spawn_positions = 1
	supervisors = "the Research Director"
	selection_color = "#633d63"
	economic_power = 12
	minimal_player_age = 10
	ideal_character_age = 50
	alt_titles = list(
		"Research Supervisor")
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/passenger/research/senior_scientist
	allowed_branches = list(/datum/mil_branch/civilian)
	allowed_ranks = list(/datum/mil_rank/civ/contractor)

	access = list(access_tox, access_tox_storage, access_research, access_mining, access_mining_office,
						access_mining_station, access_xenobiology, access_xenoarch, access_nanotrasen,
						access_expedition_shuttle, access_guppy, access_hangar, access_petrov, access_petrov_helm, access_guppy_helm)
	min_skill = list(	SKILL_BUREAUCRACY = SKILL_BASIC,
						SKILL_COMPUTER    = SKILL_BASIC,
						SKILL_FINANCE     = SKILL_BASIC,
						SKILL_BOTANY      = SKILL_BASIC,
						SKILL_ANATOMY     = SKILL_BASIC,
						SKILL_DEVICES     = SKILL_ADEPT,
						SKILL_SCIENCE     = SKILL_ADEPT)
	skill_points = 20

/datum/job/scientist
	title = "Scientist"
	total_positions = 6
	spawn_positions = 6
	supervisors = "the Research Director"
	economic_power = 10
	minimal_player_age = 7
	ideal_character_age = 45
	alt_titles = list(
		"Xenoarcheologist",
		"Anomalist",
		"Researcher",
		"Xenobiologist",
		"Xenobotanist",
		"Psychologist" = /decl/hierarchy/outfit/job/kaiserflotte/passenger/research/scientist/psych)
	min_skill = list(	SKILL_BUREAUCRACY = SKILL_BASIC,
						SKILL_COMPUTER    = SKILL_BASIC,
						SKILL_DEVICES     = SKILL_BASIC,
						SKILL_SCIENCE     = SKILL_ADEPT)

	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/passenger/research/scientist
	allowed_branches = list(/datum/mil_branch/civilian)
	allowed_ranks = list(/datum/mil_rank/civ/contractor)

	access = list(access_tox, access_tox_storage, access_research, access_petrov, access_petrov_helm,
						access_mining_office, access_mining_station, access_xenobiology, access_guppy_helm,
						access_xenoarch, access_nanotrasen, access_expedition_shuttle, access_guppy, access_hangar)
	minimal_access = list()
	skill_points = 20



/datum/job/scientist_assistant
	title = "Research Assistant"
	department = "Science"
	department_flag = SCI

	total_positions = 4
	spawn_positions = 4
	supervisors = "the Research Director and NanoTrasen Personnel"
	selection_color = "#633d63"
	economic_power = 3
	ideal_character_age = 30
	alt_titles = list(
		"Custodian" = /decl/hierarchy/outfit/job/kaiserflotte/passenger/research/assist/janitor,
		"Testing Assistant" = /decl/hierarchy/outfit/job/kaiserflotte/passenger/research/assist/testsubject,
		"Laboratory Technician",
		"Intern",
		"Clerk",
		"Field Assistant")

	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/passenger/research/assist
	allowed_branches = list(/datum/mil_branch/civilian)
	allowed_ranks = list(/datum/mil_rank/civ/contractor)

	access = list(access_research, access_mining_office, access_nanotrasen, access_petrov, access_expedition_shuttle, access_guppy, access_hangar)


/datum/job/assistant
	title = "Colonist"
	total_positions = 12
	spawn_positions = 12
	supervisors = "Kolonial Ministr"
	selection_color = "#515151"
	economic_power = 6
	announced = FALSE
	alt_titles = list(
		"Journalist" = /decl/hierarchy/outfit/job/kaiserflotte/passenger/passenger/journalist,
		"Historian",
		"Botanist",
		"Investor" = /decl/hierarchy/outfit/job/kaiserflotte/passenger/passenger/investor,
		"Naturalist",
		"Ecologist",
		"Entertainer",
		"Independent Observer",
		"Sociologist",
		"Off-Duty",
		"Trainer")
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/passenger/passenger
	allowed_branches = list(/datum/mil_branch/civilian)
	allowed_ranks = list(
		/datum/mil_rank/civ/civ,
		/datum/mil_rank/civ/offduty,
		/datum/mil_rank/civ/official
	)


/datum/job/merchant
	title = "Merchant"
	department = "Civilian"
	department_flag = CIV

	total_positions = 2
	spawn_positions = 2
	availablity_chance = 70
	supervisors = "the invisible hand of the market"
	selection_color = "#515151"
	ideal_character_age = 30
	minimal_player_age = 7
	create_record = 0
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/merchant
	allowed_branches = list(/datum/mil_branch/civilian)
	allowed_ranks = list(/datum/mil_rank/civ/civ)
	latejoin_at_spawnpoints = 1
	access = list(access_merchant)
	announced = FALSE
	min_skill = list(	SKILL_FINANCE = SKILL_ADEPT,
						SKILL_PILOT	  = SKILL_BASIC)
	skill_points = 24

/datum/job/stowaway
	title = "Hobo"
	department = "Civilian"
	department_flag = CIV

	total_positions = 1
	spawn_positions = 1
	availablity_chance = 20
	supervisors = "yourself"
	selection_color = "#515151"
	ideal_character_age = 30
	minimal_player_age = 7
	create_record = 0
	account_allowed = 0
	outfit_type = /decl/hierarchy/outfit/job/kaiserflotte/stowaway
	allowed_branches = list(/datum/mil_branch/civilian)
	allowed_ranks = list(/datum/mil_rank/civ/civ)
	latejoin_at_spawnpoints = 1
	announced = FALSE
