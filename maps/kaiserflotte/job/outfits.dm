//Job Outfits

/*
kaiserflotte OUTFITS
Keeping them simple for now, just spawning with basic EC uniforms, and pretty much no gear. Gear instead goes in lockers. Keep this in mind if editing.
*/

/decl/hierarchy/outfit/job/kaiserflotte
	name = OUTFIT_JOB_NAME("R.K. Outfit")
	hierarchy_type = /decl/hierarchy/outfit/job/kaiserflotte
	uniform = /obj/item/clothing/under/color/grey
	l_ear = /obj/item/device/radio/headset
	shoes = /obj/item/clothing/shoes/black
	pda_type = /obj/item/modular_computer/pda
	pda_slot = slot_l_store

/decl/hierarchy/outfit/job/kaiserflotte/crew
	name = OUTFIT_JOB_NAME("R.K. Crew Outfit")
	hierarchy_type = /decl/hierarchy/outfit/job/kaiserflotte/crew
	uniform = /obj/item/clothing/under/reich/utility/heer
	shoes = /obj/item/clothing/shoes/dutyboots

/decl/hierarchy/outfit/job/kaiserflotte/passenger
	name = OUTFIT_JOB_NAME("R.K. Passenger")
	hierarchy_type = /decl/hierarchy/outfit/job/kaiserflotte/passenger
	uniform = /obj/item/clothing/under/reich/utility/heer

//Command Outfits
/decl/hierarchy/outfit/job/kaiserflotte/crew/command
	name = OUTFIT_JOB_NAME("R.K. Command Outfit")
	hierarchy_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/command
	l_ear = /obj/item/device/radio/headset/headset_com

/decl/hierarchy/outfit/job/kaiserflotte/crew/command/CO
	name = OUTFIT_JOB_NAME("Reichscomissar")
	glasses = /obj/item/clothing/glasses/sunglasses
	uniform = /obj/item/clothing/under/reich/utility/heer/reichkomissar
	l_ear = /obj/item/device/radio/headset/heads/kaiserflotte/captain
	shoes = /obj/item/clothing/shoes/dutyboots
	head = /obj/item/clothing/head/helmet/reich/pickelhelm
	id_type = /obj/item/weapon/card/id/kaiserflotte/gold
	pda_type = /obj/item/modular_computer/pda/captain

/decl/hierarchy/outfit/job/kaiserflotte/crew/command/CO/New()
	..()
	backpack_overrides[/decl/backpack_outfit/backpack] = /obj/item/weapon/storage/backpack/captain
	backpack_overrides[/decl/backpack_outfit/satchel] = /obj/item/weapon/storage/backpack/satchel_cap
	backpack_overrides[/decl/backpack_outfit/messenger_bag] = /obj/item/weapon/storage/backpack/messenger/com

/decl/hierarchy/outfit/job/kaiserflotte/crew/command/XO
	name = OUTFIT_JOB_NAME("Kolonialminister")
	uniform = /obj/item/clothing/under/reich/utility/heer/officer
	l_ear = /obj/item/device/radio/headset/heads/kaiserflotte/xo
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/silver
	pda_type = /obj/item/modular_computer/pda/heads/hop

/decl/hierarchy/outfit/job/kaiserflotte/passenger/research/rd
	name = OUTFIT_JOB_NAME("Research Director")
	l_ear = /obj/item/device/radio/headset/heads/kaiserflotte/ntcommand
	uniform = /obj/item/clothing/under/rank/research_director
	suit = /obj/item/clothing/suit/storage/toggle/labcoat/science
	shoes = /obj/item/clothing/shoes/brown
	id_type = /obj/item/weapon/card/id/kaiserflotte/silver/research
	pda_type = /obj/item/modular_computer/pda/heads/rd

/decl/hierarchy/outfit/job/kaiserflotte/crew/command/cmo
	name = OUTFIT_JOB_NAME("Chief Medical Officer - HEER")
	l_ear  =/obj/item/device/radio/headset/heads/cmo
	uniform = /obj/item/clothing/under/reich/utility/heer/officer/medical
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/silver/medical
	pda_type = /obj/item/modular_computer/pda/heads/cmo
	pda_slot = slot_l_store

/decl/hierarchy/outfit/job/kaiserflotte/crew/command/cmo/civilian
	name = OUTFIT_JOB_NAME("Chief Medical Officer - CIV")
	l_ear  =/obj/item/device/radio/headset/heads/cmo
	uniform = /obj/item/clothing/under/rank/chief_medical_officer
	suit = /obj/item/clothing/suit/storage/toggle/labcoat/cmo
	shoes = /obj/item/clothing/shoes/brown
	l_hand = /obj/item/weapon/storage/firstaid/adv
	r_pocket = /obj/item/device/flashlight/pen
	id_type = /obj/item/weapon/card/id/medical/head
	pda_type = /obj/item/modular_computer/pda/heads/cmo


/decl/hierarchy/outfit/job/kaiserflotte/crew/command/cmo/New()
	..()
	BACKPACK_OVERRIDE_MEDICAL

/decl/hierarchy/outfit/job/kaiserflotte/crew/command/chief_engineer
	name = OUTFIT_JOB_NAME("Chief Engineer - R.K.")
	uniform = /obj/item/clothing/under/reich/utility/heer/officer/engineering
	shoes = /obj/item/clothing/shoes/dutyboots
	l_ear = /obj/item/device/radio/headset/heads/ce
	id_type = /obj/item/weapon/card/id/kaiserflotte/silver/engineering
	pda_type = /obj/item/modular_computer/pda/heads/ce
	pda_slot = slot_l_store
	flags = OUTFIT_HAS_BACKPACK|OUTFIT_EXTENDED_SURVIVAL

/decl/hierarchy/outfit/job/kaiserflotte/crew/command/cos/New()
	..()
	BACKPACK_OVERRIDE_ENGINEERING

/decl/hierarchy/outfit/job/kaiserflotte/crew/command/chief_engineer/civilian
	name = OUTFIT_JOB_NAME("Chief Engineer - CIV")
	head = /obj/item/clothing/head/hardhat/white
	uniform = /obj/item/clothing/under/rank/chief_engineer
	l_ear = /obj/item/device/radio/headset/heads/ce
	gloves = /obj/item/clothing/gloves/thick
	id_type = /obj/item/weapon/card/id/engineering/head
	pda_type = /obj/item/modular_computer/pda/heads/ce


/decl/hierarchy/outfit/job/kaiserflotte/crew/command/cos
	name = OUTFIT_JOB_NAME("Polizeidirektor")
	l_ear = /obj/item/device/radio/headset/heads/cos
	uniform = /obj/item/clothing/under/reich/utility/heer/officer/security
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/silver/security
	pda_type = /obj/item/modular_computer/pda/heads/hos
	head = /obj/item/clothing/head/soft/reich/cap/officer

/decl/hierarchy/outfit/job/kaiserflotte/crew/command/cos/New()
	..()
	BACKPACK_OVERRIDE_SECURITY


/decl/hierarchy/outfit/job/kaiserflotte/crew/representative
	name = OUTFIT_JOB_NAME("Officer der VerdecktePolizei")
	l_ear = /obj/item/device/radio/headset/headset_com
	uniform = /obj/item/clothing/under/reich/utility/heer/obercommando
	shoes = /obj/item/clothing/shoes/laceup
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/representative
	head = /obj/item/clothing/head/soft/reich/cap/oberofficer
	pda_type = /obj/item/modular_computer/pda/heads/paperpusher
	backpack_contents = list(/obj/item/clothing/accessory/badge/reich/representative = 1, /obj/item/clothing/head/helmet/reich/m35darkhelmet = 1)

/decl/hierarchy/outfit/job/kaiserflotte/crew/command/sea
	name = OUTFIT_JOB_NAME("Kommissar")
	uniform = /obj/item/clothing/under/reich/utility/heer/comissar
	shoes = /obj/item/clothing/shoes/dutyboots
	l_ear = /obj/item/device/radio/headset/heads/kaiserflotte/xo
	head = /obj/item/clothing/head/soft/reich/cap/comissar
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/sea
	pda_type = /obj/item/modular_computer/pda/heads


//Engineering Outfits

/decl/hierarchy/outfit/job/kaiserflotte/crew/engineering
	hierarchy_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/engineering
	l_ear = /obj/item/device/radio/headset/headset_eng
	pda_slot = slot_l_store
	flags = OUTFIT_HAS_BACKPACK|OUTFIT_EXTENDED_SURVIVAL

/decl/hierarchy/outfit/job/kaiserflotte/crew/engineering/New()
	..()
	BACKPACK_OVERRIDE_ENGINEERING

/decl/hierarchy/outfit/job/kaiserflotte/crew/engineering/senior_engineer
	name = OUTFIT_JOB_NAME("Senior Engineer")
	uniform = /obj/item/clothing/under/reich/utility/heer/officer/engineering
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/engineering/senior
	pda_type = /obj/item/modular_computer/pda/heads/ce

/decl/hierarchy/outfit/job/kaiserflotte/crew/engineering/senior_engineer/civilian
	name = OUTFIT_JOB_NAME("Senior Engineer - CIV")
	head = /obj/item/clothing/head/hardhat
	uniform = /obj/item/clothing/under/rank/engineer
	r_pocket = /obj/item/device/t_scanner
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/engineering/senior
	pda_type = /obj/item/modular_computer/pda/heads/ce

/decl/hierarchy/outfit/job/kaiserflotte/crew/engineering/engineer
	name = OUTFIT_JOB_NAME("Engineer - HEER")
	uniform = /obj/item/clothing/under/reich/utility/heer/engineering
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/engineering
	pda_type = /obj/item/modular_computer/pda/engineering

/decl/hierarchy/outfit/job/kaiserflotte/crew/engineering/engineer/civilian
	name = OUTFIT_JOB_NAME("Engineer - Civilian")
	uniform = /obj/item/clothing/under/rank/engineer
	shoes = /obj/item/clothing/shoes/workboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/engineering
	pda_type = /obj/item/modular_computer/pda/engineering

/decl/hierarchy/outfit/job/kaiserflotte/crew/engineering/roboticist
	name = OUTFIT_JOB_NAME("Mechsuit Technician - HEER")
	uniform = /obj/item/clothing/under/reich/utility/heer
	shoes = /obj/item/clothing/shoes/black
	l_ear = /obj/item/device/radio/headset/kaiserflotte/roboticist
	id_type = /obj/item/weapon/card/id/kaiserflotte/contractor/engineering/roboticist
	pda_type = /obj/item/modular_computer/pda/roboticist

/decl/hierarchy/outfit/job/kaiserflotte/crew/engineering/roboticist/civ
	name = OUTFIT_JOB_NAME("Mechsuit Technician - CIV")
	uniform = /obj/item/clothing/under/rank/roboticist
	shoes = /obj/item/clothing/shoes/black
	l_ear = /obj/item/device/radio/headset/kaiserflotte/roboticist
	id_type = /obj/item/weapon/card/id/kaiserflotte/contractor/engineering/roboticist
	pda_type = /obj/item/modular_computer/pda/roboticist

//Security Outfits

/decl/hierarchy/outfit/job/kaiserflotte/crew/security
	hierarchy_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/security
	l_ear = /obj/item/device/radio/headset/headset_sec
	pda_slot = slot_l_store

/decl/hierarchy/outfit/job/kaiserflotte/crew/security/New()
	..()
	BACKPACK_OVERRIDE_SECURITY

/decl/hierarchy/outfit/job/kaiserflotte/crew/security/brig_officer
	name = OUTFIT_JOB_NAME("Polizeimeister")
	uniform = /obj/item/clothing/under/reich/utility/heer/officer/security
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/security/brigofficer
	pda_type = /obj/item/modular_computer/pda/security
	backpack_contents = list(/obj/item/clothing/accessory/gorget/reich/warden = 1)

/decl/hierarchy/outfit/job/kaiserflotte/crew/security/forensic_tech
	name = OUTFIT_JOB_NAME("Investigator")
	uniform = /obj/item/clothing/under/reich/utility/heer/security
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/security/forensic
	pda_type = /obj/item/modular_computer/pda/forensics

/decl/hierarchy/outfit/job/kaiserflotte/crew/security/forensic_tech/contractor
	name = OUTFIT_JOB_NAME("Investigator - Contractor")
	head = /obj/item/clothing/head/det
	uniform = /obj/item/clothing/under/det
	suit = /obj/item/clothing/suit/storage/det_trench/ft
	shoes = /obj/item/clothing/shoes/laceup
	backpack_contents = list(/obj/item/clothing/accessory/badge/PI = 1)

/decl/hierarchy/outfit/job/kaiserflotte/crew/security/maa
	name = OUTFIT_JOB_NAME("Polizist")
	uniform = /obj/item/clothing/under/reich/utility/heer/security
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/security
	pda_type = /obj/item/modular_computer/pda/security

//Medical Outfits

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical
	hierarchy_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/medical
	l_ear = /obj/item/device/radio/headset/headset_med
	pda_type = /obj/item/modular_computer/pda/medical
	pda_slot = slot_l_store

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/New()
	..()
	BACKPACK_OVERRIDE_MEDICAL

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/senior
	name = OUTFIT_JOB_NAME("Physician")
	uniform = /obj/item/clothing/under/reich/utility/heer/officer/medical
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/medical/senior

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/senior/civ
	name = OUTFIT_JOB_NAME("Physician - CIV")
	uniform = /obj/item/clothing/under/rank/medical
	suit = /obj/item/clothing/suit/storage/toggle/labcoat
	l_hand = /obj/item/weapon/storage/firstaid/adv
	r_pocket = /obj/item/device/flashlight/pen
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/medical/senior

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/doctor
	name = OUTFIT_JOB_NAME("Medic")
	uniform = /obj/item/clothing/under/reich/utility/heer/medical
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/medical

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/doctor/civ
	name = OUTFIT_JOB_NAME("Medic - CIV")
	uniform = /obj/item/clothing/under/rank/medical
	suit = /obj/item/clothing/suit/storage/toggle/labcoat
	l_hand = /obj/item/weapon/storage/firstaid/adv
	r_pocket = /obj/item/device/flashlight/pen
	id_type = /obj/item/weapon/card/id/medical

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/orderly
	name = OUTFIT_JOB_NAME("Orderly")
	uniform = /obj/item/clothing/under/rank/orderly

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/resident
	name = OUTFIT_JOB_NAME("Medical Resident")
	uniform = /obj/item/clothing/under/color/white

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/xenosurgeon
	name = OUTFIT_JOB_NAME("Xenosurgeon")
	uniform = /obj/item/clothing/under/rank/medical/scrubs/purple

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/mortus
	name = OUTFIT_JOB_NAME("Mortician")
	uniform = /obj/item/clothing/under/rank/medical/scrubs/black

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/virologist
	name = OUTFIT_JOB_NAME("Virologist - R.K.")
	uniform = /obj/item/clothing/under/rank/virologist

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/virologist/New()
	..()
	BACKPACK_OVERRIDE_VIROLOGY

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/paramedic
	name = OUTFIT_JOB_NAME("Paramedic - R.K.")
	uniform = /obj/item/clothing/under/rank/medical/paramedic
	suit = /obj/item/clothing/suit/storage/toggle/fr_jacket
	shoes = /obj/item/clothing/shoes/jackboots
	l_hand = /obj/item/weapon/storage/firstaid/adv
	belt = /obj/item/weapon/storage/belt/medical/emt
	flags = OUTFIT_HAS_BACKPACK|OUTFIT_EXTENDED_SURVIVAL

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/chemist
	name = OUTFIT_JOB_NAME("Chemist - R.K.")
	uniform = /obj/item/clothing/under/rank/chemist
	shoes = /obj/item/clothing/shoes/white
	pda_type = /obj/item/modular_computer/pda/chemistry
	id_type = /obj/item/weapon/card/id/kaiserflotte/contractor/chemist

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/contractor/chemist/New()
	..()
	BACKPACK_OVERRIDE_CHEMISTRY

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/counselor
	name = OUTFIT_JOB_NAME("Counselor")
	uniform = /obj/item/clothing/under/rank/psych/turtleneck
	shoes = /obj/item/clothing/shoes/white
	id_type = /obj/item/weapon/card/id/kaiserflotte/contractor/medical/counselor

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/counselor/psychiatrist
	name = OUTFIT_JOB_NAME("Psychiatrist - R.K.")
	uniform = /obj/item/clothing/under/rank/psych

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/counselor/chaplain
	name = OUTFIT_JOB_NAME("Chaplain - R.K.")
	uniform = /obj/item/clothing/under/rank/chaplain

/decl/hierarchy/outfit/job/kaiserflotte/crew/medical/counselor/heer
	name = OUTFIT_JOB_NAME("Counselor - Heer")
	uniform = /obj/item/clothing/under/reich/utility/heer/officer/medical
	shoes = /obj/item/clothing/shoes/dutyboots

//Supply Outfits

/decl/hierarchy/outfit/job/kaiserflotte/crew/supply
	l_ear = /obj/item/device/radio/headset/headset_cargo
	hierarchy_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/supply

/decl/hierarchy/outfit/job/kaiserflotte/crew/supply/New()
	..()
	BACKPACK_OVERRIDE_ENGINEERING

/decl/hierarchy/outfit/job/kaiserflotte/crew/supply/deckofficer
	name = OUTFIT_JOB_NAME("Quartiermeister")
	l_ear = /obj/item/device/radio/headset/headset_deckofficer
	uniform = /obj/item/clothing/under/reich/utility/heer/supply
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/supply/deckofficer
	pda_type = /obj/item/modular_computer/pda/cargo

/decl/hierarchy/outfit/job/kaiserflotte/crew/supply/deckofficer/civ
	name = OUTFIT_JOB_NAME("Quartiermeister - CIV")
	uniform = /obj/item/clothing/under/rank/cargo
	shoes = /obj/item/clothing/shoes/brown
	glasses = /obj/item/clothing/glasses/sunglasses
	l_hand = /obj/item/weapon/clipboard

/decl/hierarchy/outfit/job/kaiserflotte/crew/supply/tech
	name = OUTFIT_JOB_NAME("Frachttechniker")
	uniform = /obj/item/clothing/under/reich/utility/heer/supply
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/supply
	pda_type = /obj/item/modular_computer/pda/cargo

/decl/hierarchy/outfit/job/kaiserflotte/crew/supply/tech/civ
	name = OUTFIT_JOB_NAME("Frachttechniker - CIV")
	uniform = /obj/item/clothing/under/rank/cargotech


//Service Outfits

/decl/hierarchy/outfit/job/kaiserflotte/crew/service
	l_ear = /obj/item/device/radio/headset/headset_service
	hierarchy_type = /decl/hierarchy/outfit/job/kaiserflotte/crew/service

/decl/hierarchy/outfit/job/kaiserflotte/crew/service/janitor
	name = OUTFIT_JOB_NAME("Janitor - CIV")
	uniform = /obj/item/clothing/under/rank/janitor
	shoes = /obj/item/clothing/shoes/black
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/service/janitor
	pda_type = /obj/item/modular_computer/pda

/decl/hierarchy/outfit/job/kaiserflotte/crew/service/janitor/mil
	name = OUTFIT_JOB_NAME("Janitor - Militar")
	uniform = /obj/item/clothing/under/reich/utility/heer/service
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/service/janitor
	pda_type = /obj/item/modular_computer/pda

/decl/hierarchy/outfit/job/kaiserflotte/crew/service/cook
	name = OUTFIT_JOB_NAME("Cook - CIV")
	uniform = /obj/item/clothing/under/rank/chef
	shoes = /obj/item/clothing/shoes/black
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/service/chef
	pda_type = /obj/item/modular_computer/pda

/decl/hierarchy/outfit/job/kaiserflotte/crew/service/cook/mil
	name = OUTFIT_JOB_NAME("Chef - Militar")
	uniform = /obj/item/clothing/under/reich/utility/heer/service
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew/service/chef
	pda_type = /obj/item/modular_computer/pda


/decl/hierarchy/outfit/job/kaiserflotte/crew/service/bartender
	name = OUTFIT_JOB_NAME("Bartender")
	uniform = /obj/item/clothing/under/rank/bartender
	shoes = /obj/item/clothing/shoes/laceup
	id_type = /obj/item/weapon/card/id/kaiserflotte/contractor/service/bartender
	pda_type = /obj/item/modular_computer/pda

/decl/hierarchy/outfit/job/kaiserflotte/crew/service/crewman
	name = OUTFIT_JOB_NAME("Garnison")
	uniform =  /obj/item/clothing/under/reich/utility/heer
	shoes = /obj/item/clothing/shoes/dutyboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/crew
	pda_type = /obj/item/modular_computer/pda

/
//Passenger Outfits

/decl/hierarchy/outfit/job/kaiserflotte/passenger/research
	hierarchy_type = /decl/hierarchy/outfit/job/kaiserflotte/passenger/research
	l_ear = /obj/item/device/radio/headset/kaiserflotte/nanotrasen

/decl/hierarchy/outfit/job/kaiserflotte/passenger/research/senior_scientist
	name = OUTFIT_JOB_NAME("Senior Researcher")
	uniform = /obj/item/clothing/under/rank/scientist/executive
	suit = /obj/item/clothing/suit/storage/toggle/labcoat/science
	shoes = /obj/item/clothing/shoes/white
	pda_type = /obj/item/modular_computer/pda/heads/rd
	id_type = /obj/item/weapon/card/id/kaiserflotte/passenger/research/senior_scientist

/decl/hierarchy/outfit/job/kaiserflotte/passenger/research/senior_scientist/New()
	..()
	BACKPACK_OVERRIDE_RESEARCH

/decl/hierarchy/outfit/job/kaiserflotte/passenger/research/scientist
	name = OUTFIT_JOB_NAME("Scientist - R.K.")
	uniform = /obj/item/clothing/under/rank/scientist
	shoes = /obj/item/clothing/shoes/white
	pda_type = /obj/item/modular_computer/pda/science
	id_type = /obj/item/weapon/card/id/kaiserflotte/passenger/research/scientist

/decl/hierarchy/outfit/job/kaiserflotte/passenger/research/scientist/New()
	..()
	BACKPACK_OVERRIDE_RESEARCH

/decl/hierarchy/outfit/job/kaiserflotte/passenger/research/scientist/psych
	name = OUTFIT_JOB_NAME("Psychologist - R.K.")
	uniform = /obj/item/clothing/under/rank/psych

/decl/hierarchy/outfit/job/kaiserflotte/passenger/research/prospector
	name = OUTFIT_JOB_NAME("Prospector")
	uniform = /obj/item/clothing/under/rank/ntwork
	shoes = /obj/item/clothing/shoes/workboots
	id_type = /obj/item/weapon/card/id/kaiserflotte/passenger/research/mining
	pda_type = /obj/item/modular_computer/pda/science
	flags = OUTFIT_HAS_BACKPACK|OUTFIT_EXTENDED_SURVIVAL

/decl/hierarchy/outfit/job/kaiserflotte/passenger/research/prospector/New()
	..()
	BACKPACK_OVERRIDE_ENGINEERING

/decl/hierarchy/outfit/job/kaiserflotte/passenger/research/assist
	name = OUTFIT_JOB_NAME("Research Assistant - R.K.")
	uniform = /obj/item/clothing/under/rank/scientist
	shoes = /obj/item/clothing/shoes/white
	pda_type = /obj/item/modular_computer/pda/science
	id_type = /obj/item/weapon/card/id/kaiserflotte/passenger/research

/decl/hierarchy/outfit/job/kaiserflotte/passenger/research/assist/janitor
	name = OUTFIT_JOB_NAME("Custodian - R.K.")
	uniform = /obj/item/clothing/under/rank/janitor

/decl/hierarchy/outfit/job/kaiserflotte/passenger/research/assist/testsubject
	name = OUTFIT_JOB_NAME("Testing Assistant")
	uniform = /obj/item/clothing/under/rank/ntwork

/decl/hierarchy/outfit/job/kaiserflotte/passenger/passenger
	name = OUTFIT_JOB_NAME("Passenger - R.K.")
	uniform = /obj/item/clothing/under/color/grey
	l_ear = /obj/item/device/radio/headset
	shoes = /obj/item/clothing/shoes/black
	pda_type = /obj/item/modular_computer/pda
	id_type = /obj/item/weapon/card/id/kaiserflotte/passenger

/decl/hierarchy/outfit/job/kaiserflotte/passenger/passenger/journalist
	name = OUTFIT_JOB_NAME("Journalist - R.K.")
	backpack_contents = list(/obj/item/device/tvcamera = 1,
	/obj/item/clothing/accessory/badge/press = 1)

/decl/hierarchy/outfit/job/kaiserflotte/passenger/passenger/investor
	name = OUTFIT_JOB_NAME("Investor - R.K.")

/decl/hierarchy/outfit/job/kaiserflotte/passenger/passenger/investor/post_equip(var/mob/living/carbon/human/H)
	..()
	var/obj/item/weapon/storage/secure/briefcase/money/case = new(H.loc)
	H.put_in_hands(case)

/decl/hierarchy/outfit/job/kaiserflotte/merchant
	name = OUTFIT_JOB_NAME("Merchant - R.K.")
	uniform = /obj/item/clothing/under/color/black
	l_ear = null
	shoes = /obj/item/clothing/shoes/black
	pda_type = /obj/item/modular_computer/pda
	id_type = /obj/item/weapon/card/id/kaiserflotte/merchant

/decl/hierarchy/outfit/job/kaiserflotte/stowaway
	name = OUTFIT_JOB_NAME("Stowaway - R.K.")
	id_type = null
	pda_type = null
	l_ear = null
	l_pocket = /obj/item/weapon/wrench
	r_pocket = /obj/item/weapon/crowbar

/decl/hierarchy/outfit/job/kaiserflotte/stowaway/post_equip(var/mob/living/carbon/human/H)
	..()
	var/obj/item/weapon/card/id/kaiserflotte/stowaway/ID = new(H.loc)
	H.put_in_hands(ID)
