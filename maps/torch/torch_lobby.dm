/datum/map/torch
	lobby_icon = 'maps/torch/icons/lobby.dmi'
	lobby_screens = list("title","title2")
	lobby_tracks = list(
		/music_track/total_war,
		/music_track/totalitarianism,
		/music_track/emptyautumn,
		/music_track/fatalist,
		/music_track/victorymarch,
		/music_track/redemption)
